<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->char('active',1)->nullable()->default(\DefConst::NOTACTIVE);
            $table->unsignedInteger('classification_id')->nullable();
            $table->string('name')->unique();
            $table->string('slug_name')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type')->nullable();
            $table->string('addr1')->nullable();
            $table->string('addr2')->nullable();
            $table->string('zip')->nullable();
           	$table->string('city')->nullable();
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->string('website')->nullable();
            $table->string('description')->nullable();
            $table->decimal('rating', 2)->default(0);
            $table->string('location')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('classification_id')->references('id')->on('classifications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
