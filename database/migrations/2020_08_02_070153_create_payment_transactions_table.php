<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('item_sale_id');
            $table->string('xref_id')->nullable();
            $table->string('transaction_type')->nullable();
            $table->string('transction_status')->nullable();
            $table->string('transction_number')->nullable();
            $table->string('transction_token')->nullable();
            $table->string('account_name')->nullable();
            $table->string('purshase_amount')->nullable();
            $table->string('refunded_amount')->nullable();
            $table->timestamps();

            $table->foreign('item_sale_id')->references('id')->on('item_sales');		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transactions');
    }
}
