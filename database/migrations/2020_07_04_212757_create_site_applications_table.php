<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('recordno')->nullable();
            $table->string('status')->nullable()->default(\DefConst::PENDING);
            $table->string('company_name')->unique();
            $table->string('email')->unique();
            $table->string('classification_id')->nullable();
            $table->string('addr')->nullable();
            $table->string('zip')->nullable();
           	$table->string('city')->nullable();
           	$table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('website')->nullable();
            $table->string('description', 1000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_applications');
    }
}
