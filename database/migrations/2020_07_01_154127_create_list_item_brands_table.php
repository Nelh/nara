<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListItemBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_item_brands', function (Blueprint $table) {
            $table->increments('id');
            $table->char('active',1)->default(\DefConst::NOTACTIVE);
            $table->char('type',1)->nullable();
            $table->string('name')->nullable();
            $table->string('logo')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_item_brands');
    }
}
