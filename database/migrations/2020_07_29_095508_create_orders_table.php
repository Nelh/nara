<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('item_sales_id')->nullable();
            $table->string('invoiceno')->nullable()->index();
            $table->char('active',1)->nullable()->default(\DefConst::ACTIVE);
            $table->string('status')->nullable()->index();
            $table->string('status_track_id')->nullable();
            $table->string('paid_at')->nullable();
            $table->string('priority')->nullable();
            $table->timestamp('duedate')->nullable();
            $table->timestamp('latestupdate')->nullable();
            $table->string('receivername')->nullable();
            $table->string('receivertel')->nullable();
            $table->string('deliveryaddress')->nullable();
            $table->string('deliverysuburb')->nullable();
            $table->string('deliverycity')->nullable();
            $table->string('addresstype')->nullable();
            $table->string('comments')->nullable();
			$table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
