<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_cart_items', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('shopping_cart_id')->index();
            $table->string('invoiceno')->nullable()->index();
			$table->unsignedInteger('site_id')->nullable()->index();
			$table->unsignedInteger('item_id')->index();
			$table->string('recordno')->nullable();
			$table->integer('qty');
			$table->decimal('price',14,2)->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->timestamps();

            $table->foreign('shopping_cart_id')->references('id')->on('shopping_cart');			
            $table->foreign('site_id')->references('id')->on('sites');
			$table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_cart_items');
    }
}
