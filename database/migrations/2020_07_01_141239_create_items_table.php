<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('site_id')->nullable();
			$table->unsignedInteger('list_item_category_id')->nullable();
			$table->unsignedInteger('list_item_brand_id')->nullable();
            $table->unsignedInteger('item_groups_id')->nullable();
            $table->unsignedInteger('sub_category_id')->nullable();
			$table->string('recordno')->nullable();
            $table->char('active',1)->nullable();
			$table->string('name')->nullable();
			$table->string('description',1000)->nullable();
			$table->string('specification',1000)->nullable();
            $table->double('price',13,2)->nullable();
			$table->decimal('size',13,2)->nullable();
			$table->decimal('deduction',13,2)->nullable();
            $table->string('warranty')->nullable();
            $table->string('location')->nullable();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `items` ADD FULLTEXT INDEX `items_name_description_ft` (`name`,`description`)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
