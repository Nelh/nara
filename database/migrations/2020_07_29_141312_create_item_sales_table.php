<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_sales', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('user_id')->nullable()->index();
			$table->unsignedInteger('shopping_cart_id')->index();
            $table->string('invoiceno')->nullable()->index();
            $table->string('items')->nullable();
            $table->decimal('qty', 13, 2)->nullable();
			$table->decimal('subtotal',14,2)->nullable();
            $table->decimal('total', 13, 2)->nullable();
            $table->string('paid_at')->nullable()->default(null);
            $table->timestamp('saledate')->nullable();
			$table->timestamp('deleted_at')->nullable();
            $table->timestamps();
            
            $table->foreign('shopping_cart_id')->references('id')->on('shopping_cart');		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_sales');
    }
}
