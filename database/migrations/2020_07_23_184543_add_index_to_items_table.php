<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->index('site_id');
			$table->index('list_item_category_id');
			$table->index('list_item_brand_id');
			$table->index('sub_category_id');
			$table->index('item_groups_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropIndex('items_site_id_index');
            $table->dropIndex('items_list_item_category_id_index');
            $table->dropIndex('items_list_item_brand_id_index');
            $table->dropIndex('items_sub_category_id_index');
            $table->dropIndex('items_item_groups_id_index');
        });
    }
}
