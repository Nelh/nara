<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_cart', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('user_id')->nullable()->index();
            $table->string('invoiceno')->nullable()->index();
			$table->decimal('subtotal',14,2)->nullable();
            $table->string('paid_at')->nullable()->default(null);
			$table->timestamp('deleted_at')->nullable();
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_cart');
    }
}
