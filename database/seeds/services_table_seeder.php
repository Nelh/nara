<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class services_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('services')->delete();

		DB::table('services')->insert([
			[
				'active' => 'Y',
				'fname' => 'Nelh',
				'lname' => 'Demo',
                'email' => 'demo@gmail.com',
                'address' => '13 prestwich Street',
				'password' => '$2y$10$8xXTv22c89NFmo46pfCtq.O1Rz558YQc4PUguzD7e1SCrufg.jsUC'
			],
			[
				'active' => 'Y',
				'fname' => 'Nouni',
				'lname' => 'Dinnhel',
                'email' => 'nelhamstrong9@gmail.com',
                'address' => '13 prestwich Street',
				'password' => '$2y$10$8xXTv22c89NFmo46pfCtq.O1Rz558YQc4PUguzD7e1SCrufg.jsUC'
			]
		]);


	}
}
