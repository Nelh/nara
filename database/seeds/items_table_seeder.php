<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\List_item_brands;
use App\Models\List_item_category;
use App\Models\SubCategory;
use App\Site;
use Faker\Factory as Faker;


class items_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('items')->delete();

		DB::table('items')->insert([
			[
				'site_id' => 1,
				'list_item_brand_id' => null,
				'list_item_category_id' => null,
				'sub_category_id' => null,
                'name' => 'Banana',
                'recordno' => 'ITEMCODE00' . 1,
				'price' => '3',
				'description' => 'sweet banana ready to eat',
				'specification' => null,  
				'location' => 'AB'
			],
			[
				'site_id' => 2,
				'list_item_brand_id' => null,
				'list_item_category_id' => null,
				'sub_category_id' => null,
                'name' => 'Fresh baguette',
                'recordno' => 'ITEMCODE00' . 2,
				'price' => '3',
				'description' => 'Our bakery is named after its signature product, the traditional baguette. Our master baker leaves nothing to chance and uses the very best French techniques: a cold dough process that develops unique flavors, homemade “levain” (leaven), and an oven imported from France with a stone based sole for outstanding baking. The result is a baguette that is light, airy, with just the right crunch and doesn’t require a trip to France.  And we bake baguettes throughout the day, so you can always get a fresh one.',
				'specification' => null,  
				'location' => 'AB'
			],
			[
				'site_id' => 1,
				'list_item_brand_id' => null,
				'list_item_category_id' => null,
				'sub_category_id' => null,
                'name' => 'Tea & Infusions',
                'recordno' => 'ITEMCODE00' . 3,
				'price' => '3',
				'description' => 'This attractive tea is packaged in a sachet made of eco-friendly film. It is sealed to keep fragrance and flavour in and moisture out, so the tea can retain the fine natural nuances.',
				'specification' => null,  
				'location' => 'AB'
			]
		]);

	}
}
