<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class cities_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('cities')->delete();

		DB::table('cities')->insert([
			[
				'city_code' => 'AB',
				'city_name' => 'Alberta',
				'country' => 'Canada',
            ],
            [
				'city_code' => 'BC',
				'city_name' => 'British columbia',
				'country' => 'Canada',
            ],
            [
				'city_code' => 'BC',
				'city_name' => 'Quebec',
				'country' => 'Canada',
			],
            [
				'city_code' => 'YT',
				'city_name' => 'Yukon',
				'country' => 'Canada',
			]
		]);


	}
}
