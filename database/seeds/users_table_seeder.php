<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class users_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('users')->delete();

		DB::table('users')->insert([
			[
				'active' => 'Y',
				'name' => 'nelh',
				'email' => 'nelh@esponline.co.za',
				'password' => '$2y$10$8xXTv22c89NFmo46pfCtq.O1Rz558YQc4PUguzD7e1SCrufg.jsUC'
			]
		]);


	}
}
