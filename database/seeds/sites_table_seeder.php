<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;


class sites_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('sites')->delete();

		$faker = Faker::create();
		DB::table('sites')->insert([
			[
				'active' => 'Y',
                'name' => 'Casino Park and Shop',
                'slug_name' => 'casino-park-and-shop',
				'email' => 'info@casino.com',
				'password' => '$2y$10$8xXTv22c89NFmo46pfCtq.O1Rz558YQc4PUguzD7e1SCrufg.jsUC',
				'location' => 'AB'
			],
			[
				'active' => 'Y',
                'name' => 'La baguette',
                'slug_name' => 'la-baguette',
				'email' => 'info@la-baguette.com',
				'password' => '$2y$10$8xXTv22c89NFmo46pfCtq.O1Rz558YQc4PUguzD7e1SCrufg.jsUC',
				'location' => 'AB'
			],
		]);


	}
}
