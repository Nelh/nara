<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class brands_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('list_item_brands')->delete();

		DB::table('list_item_brands')->insert([
			[
				'active' => 'Y',
				'name' => 'samsung',
				'logo' => '',
            ],
            [
				'active' => 'Y',
				'name' => 'Apple',
				'logo' => '',
			],
            [
				'active' => 'Y',
				'name' => 'HP',
				'logo' => '',
            ],
		]);


	}
}
