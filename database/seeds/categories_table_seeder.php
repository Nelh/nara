<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class categories_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('list_item_categories')->delete();

		DB::table('list_item_categories')->insertOrIgnore([
            [
                'id' => 2,
				'active' => 'Y',
                'name' => 'grosseries',
                'type' => 'R',
			],
            [
                'id' => 3,
				'active' => 'Y',
                'name' => 'food',
                'type' => 'R',
			],
            [
                'id' => 5,
				'active' => 'Y',
                'name' => 'misc',
                'type' => 'R',
			],
            [
                'id' => 6,
				'active' => 'Y',
                'name' => 'drinks',
                'type' => 'R',
			],
            [
                'id' => 7,
				'active' => 'Y',
                'name' => 'beauty',
                'type' => 'R',
			],
            [
                'id' => 8,
				'active' => 'Y',
                'name' => 'baby',
                'type' => 'R',
            ],
            [
                'id' => 9,
				'active' => 'Y',
                'name' => 'pets',
                'type' => 'R',
            ],
            [
                'id' => 10,
				'active' => 'Y',
                'name' => 'appliance',
                'type' => 'R',
            ],
            [
                'id' => 11,
				'active' => 'Y',
                'name' => 'electronic',
                'type' => 'R',
            ],
            [
                'id' => 12,
				'active' => 'Y',
                'name' => 'auto',
                'type' => 'R',
            ],
            [
                'id' => 14,
				'active' => 'Y',
                'name' => 'pharmacy',
                'type' => 'R',
            ],
		]);


	}
}
