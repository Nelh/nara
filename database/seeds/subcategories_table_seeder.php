<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class subcategories_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('sub_categories')->delete();

		DB::table('sub_categories')->insertOrIgnore([
			[
                'id' => 1,
				'list_item_category_id' => 5,
				'name' => 'Papeteries',
            ],
            [
                'id' => 2,
				'list_item_category_id' => 5,
				'name' => 'Fournitures',
            ],
            [
                'id' => 3,
				'list_item_category_id' => 5,
				'name' => 'Calculatrice',
            ],
            [
                'id' => 4,
				'list_item_category_id' => 5,
				'name' => 'Livres scolaires',
            ],
            [
                'id' => 5,
				'list_item_category_id' => 5,
				'name' => 'Produits Eco',
            ],
            [
                'id' => 6,
				'list_item_category_id' => 5,
				'name' => 'Ecriture et Correction',
            ],
            [
                'id' => 7,
				'list_item_category_id' => 5,
				'name' => 'Classements',
            ],
            [
                'id' => 8,
				'list_item_category_id' => 2,
				'name' => 'Fruits',
            ],
            [
                'id' => 9,
				'list_item_category_id' => 2,
				'name' => 'Legumes',
            ],
            [
                'id' => 10,
				'list_item_category_id' => 2,
				'name' => 'Fruits & Legumes Bio',
            ],
            [
                'id' => 11,
				'list_item_category_id' => 2,
				'name' => 'Jus de fruits',
            ],
            [
                'id' => 12,
				'list_item_category_id' => 2,
				'name' => 'Legumes Frais',
            ],
            [
                'id' => 13,
				'list_item_category_id' => 2,
				'name' => 'Plantes & Fleurs',
            ],
            [
                'id' => 14,
				'list_item_category_id' => 3,
				'name' => 'Boucherie',
            ],
            [
                'id' => 15,
				'list_item_category_id' => 3,
				'name' => 'Poissonerie',
            ],
            [
                'id' => 16,
				'list_item_category_id' => 3,
				'name' => 'Rotisseries',
            ],
            [
                'id' => 17,
				'list_item_category_id' => 3,
				'name' => 'Boulangerie',
            ],
            [
                'id' => 18,
				'list_item_category_id' => 3,
				'name' => 'Patissierie',
            ],
            [
                'id' => 19,
				'list_item_category_id' => 3,
				'name' => 'Brioche',
            ],
            [
                'id' => 20,
				'list_item_category_id' => 3,
				'name' => 'Hamburger',
            ],
            [
                'id' => 21,
				'list_item_category_id' => 3,
				'name' => 'Viennoiserie',
            ],
            [
                'id' => 23,
				'list_item_category_id' => 5,
				'name' => 'Ustensils de Cuisine',
            ],
            [
                'id' => 24,
				'list_item_category_id' => 6,
				'name' => 'Jus de Fruits',
            ],
            [
                'id' => 25,
				'list_item_category_id' => 6,
				'name' => 'Eau minerals',
            ],
            [
                'id' => 26,
				'list_item_category_id' => 6,
				'name' => 'Colas, Thes glaces',
            ],
            [
                'id' => 27,
				'list_item_category_id' => 6,
				'name' => 'soft drink',
            ],
            [
                'id' => 28,
				'list_item_category_id' => 6,
				'name' => 'Dosettes & Capsules',
            ],
            [
                'id' => 29,
				'list_item_category_id' => 6,
				'name' => 'Laits, Boissons Lactees & vegetales',
            ],
            [
                'id' => 30,
				'list_item_category_id' => 7,
				'name' => 'Papier toilettes',
            ],
            [
                'id' => 31,
				'list_item_category_id' => 7,
				'name' => 'Mouchoirs, cotons',
            ],
            [
                'id' => 32,
				'list_item_category_id' => 7,
				'name' => 'Premiers soins',
            ],
            [
                'id' => 33,
				'list_item_category_id' => 7,
				'name' => 'Le corps',
            ],
            [
                'id' => 34,
				'list_item_category_id' => 7,
				'name' => 'Les cheveux',
            ],
            [
                'id' => 35,
				'list_item_category_id' => 7,
				'name' => 'Le visage',
            ],
            [
                'id' => 36,
				'list_item_category_id' => 7,
				'name' => 'Hygiene dentaire',
            ],
            [
                'id' => 37,
				'list_item_category_id' => 7,
				'name' => 'Hygiene feminine, incontinente',
            ],
            [
                'id' => 38,
				'list_item_category_id' => 7,
				'name' => 'Enfants',
            ],
            [
                'id' => 39,
				'list_item_category_id' => 7,
				'name' => 'Hommes',
            ],
            [
                'id' => 40,
				'list_item_category_id' => 7,
				'name' => 'Huiles essentielles',
            ],
            [
                'id' => 41,
				'list_item_category_id' => 8,
				'name' => 'Laits infantiles',
            ],
            [
                'id' => 42,
				'list_item_category_id' => 8,
				'name' => 'Cereales pour bebe',
            ],
            [
                'id' => 43,
				'list_item_category_id' => 8,
				'name' => 'Cereales pour bebe',
            ],
            [
                'id' => 44,
				'list_item_category_id' => 8,
				'name' => 'Repas pour bebe',
            ],
            [
                'id' => 45,
				'list_item_category_id' => 8,
				'name' => 'Dessert, Gouter, Jus',
            ],
            [
                'id' => 46,
				'list_item_category_id' => 8,
				'name' => 'Toilette pour bebe',
            ],
            [
                'id' => 47,
				'list_item_category_id' => 8,
				'name' => 'Eveil, Babde, Sommeil',
            ],
            [
                'id' => 48,
				'list_item_category_id' => 8,
				'name' => 'Pour maman',
            ],
            [
                'id' => 49,
				'list_item_category_id' => 9,
				'name' => 'croquettes',
            ],
            [
                'id' => 50,
				'list_item_category_id' => 9,
				'name' => 'Sachets repas',
            ],
            [
                'id' => 51,
				'list_item_category_id' => 9,
				'name' => 'Friandises',
            ],
            [
                'id' => 52,
				'list_item_category_id' => 9,
				'name' => 'Hygiene soins',
            ],
            [
                'id' => 53,
				'list_item_category_id' => 9,
				'name' => 'Accessoires',
            ],
            [
                'id' => 54,
				'list_item_category_id' => 10,
				'name' => 'Climatisations',
            ],
            [
                'id' => 55,
				'list_item_category_id' => 10,
				'name' => 'Traitement d\'air',
            ],
            [
                'id' => 56,
				'list_item_category_id' => 10,
				'name' => 'Cuissons',
            ],
            [
                'id' => 57,
				'list_item_category_id' => 10,
				'name' => 'Salles a manger',
            ],
            [
                'id' => 58,
				'list_item_category_id' => 10,
				'name' => 'Salons',
            ],
            [
                'id' => 59,
				'list_item_category_id' => 10,
				'name' => 'Lavage & Sechage',
            ],
            [
                'id' => 60,
				'list_item_category_id' => 10,
				'name' => 'Lavage & Sechage',
            ],
            [
                'id' => 61,
				'list_item_category_id' => 10,
				'name' => 'Soin du linge',
            ],
            [
                'id' => 62,
				'list_item_category_id' => 10,
				'name' => 'Chauffage',
            ],
            [
                'id' => 63,
				'list_item_category_id' => 10,
				'name' => 'Froid',
            ],
            [
                'id' => 64,
				'list_item_category_id' => 11,
				'name' => 'Telephonie et Object connecte',
            ],
            [
                'id' => 65,
				'list_item_category_id' => 11,
				'name' => 'Hifi & Enceintes',
            ],
            [
                'id' => 66,
				'list_item_category_id' => 11,
				'name' => 'Photo & Video Numerique',
            ],
            [
                'id' => 67,
				'list_item_category_id' => 11,
				'name' => 'Tv & Lecteurs DVD',
            ],
            [
                'id' => 68,
				'list_item_category_id' => 11,
				'name' => 'Ordinateurs & Tablettes',
            ],
            [
                'id' => 69,
				'list_item_category_id' => 11,
				'name' => 'Peripherique, Reseau & WIfi',
            ],
            [
                'id' => 70,
				'list_item_category_id' => 11,
				'name' => 'Composants et Stockage',
            ],
            [
                'id' => 71,
				'list_item_category_id' => 11,
				'name' => 'DVD - Bly Ray',
            ],
            [
                'id' => 72,
				'list_item_category_id' => 11,
				'name' => 'Jeux et Consoles',
            ],
            [
                'id' => 73,
				'list_item_category_id' => 12,
				'name' => 'Quicaillerie',
            ],
            [
                'id' => 74,
				'list_item_category_id' => 12,
				'name' => 'Plomberie et Sanitaire',
            ],
            [
                'id' => 75,
				'list_item_category_id' => 12,
				'name' => 'Outillages',
            ],
            [
                'id' => 76,
				'list_item_category_id' => 12,
				'name' => 'Electricites',
            ],
            [
                'id' => 77,
				'list_item_category_id' => 12,
				'name' => 'Auto',
            ],
            [
                'id' => 78,
				'list_item_category_id' => 12,
				'name' => 'Revetement Sols & Murs',
            ],
            [
                'id' => 79,
				'list_item_category_id' => 3,
				'name' => 'Volaille',
            ],
            [
                'id' => 80,
				'list_item_category_id' => 8,
				'name' => 'Couches et apprentissage propiete',
            ],
            [
                'id' => 81,
				'list_item_category_id' => 11,
				'name' => 'Tv & Lecteurs DVD',
            ],
            [
                'id' => 82,
				'list_item_category_id' => 11,
				'name' => 'Tv & Lecteurs DVD',
            ],
		]);


	}
}
