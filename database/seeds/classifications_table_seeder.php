<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class classifications_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('classifications')->delete();

		DB::table('classifications')->insert([
			[
				'name' => 'Entreprise',
				'type' => 'R'
            ],
            [
				'name' => 'Pharmacie',
				'type' => 'D'
            ],
            [
				'name' => 'Boutique',
				'type' => 'R'
            ],
            [
				'name' => 'Supermarcher',
				'type' => 'R'
            ],
		]);


	}
}
