<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class orders_status_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('orders_status')->delete();

		DB::table('orders_status')->insert([
			[
				'name' => \DefConst::PLACED,
				'track_id' => 1,
            ],
            [
				'name' => \DefConst::PICKUP,
				'track_id' => 2,
            ],
            [
				'name' => \DefConst::ARRIVEDATFACILITY,
				'track_id' => 3,
            ],
            [
				'name' => \DefConst::OUTOFDELIVERY,
				'track_id' => 4,
            ],
            [
				'name' => \DefConst::DELIVERED,
				'track_id' => 5,
			],
            [
				'name' => \DefConst::COMPLETED,
				'track_id' => 6,
			],
		]);


	}
}
