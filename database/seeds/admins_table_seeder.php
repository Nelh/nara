<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class admins_table_seeder extends Seeder
{
	public function run()
	{
        DB::table('admins')->delete();

		DB::table('admins')->insert([
			[
				'active' => 'Y',
				'fname' => 'nelh',
				'lname' => 'amstrong',
				'email' => 'nelh@esponline.co.za',
				'password' => '$2y$10$8xXTv22c89NFmo46pfCtq.O1Rz558YQc4PUguzD7e1SCrufg.jsUC'
			],[
				'active' => 'Y',
				'fname' => 'test',
				'lname' => 'demo',
				'email' => 'demo@gmail.com',
				'password' => '$2y$10$8xXTv22c89NFmo46pfCtq.O1Rz558YQc4PUguzD7e1SCrufg.jsUC'
			]
		]);


	}
}
