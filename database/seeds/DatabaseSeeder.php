<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(admins_table_seeder::class);
        $this->call(sites_table_seeder::class);
        $this->call(classifications_table_seeder::class);
        $this->call(brands_table_seeder::class);
        $this->call(categories_table_seeder::class);
        $this->call(subcategories_table_seeder::class);
        $this->call(users_table_seeder::class);
        $this->call(services_table_seeder::class);
        $this->call(orders_status_table_seeder::class);
        $this->call(items_table_seeder::class);
        $this->call(cities_table_seeder::class);
    }
}
