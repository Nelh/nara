<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'sign_into_your' => 'Sign into your',
    'create_an_account' => 'Create an account',
    'administrator_account' => 'Administrator account',
    'delivers_portal' => 'Deliveries Portal',
    'business_profile' => 'Business Profile',
    'sign_into_your_account' => 'Sign into your account',
    'remember_me' => 'Remember me',
    'dont_have_an_account' => 'Don\'t have an account,',
    'already_have_an_account' => 'Already have an account,',
    'subscribe_here' => 'Subscribe here',
    'login_here' => 'Login here',
    'forgot_your_password?' => 'Forgot your password?',
    'privacy_policy' => 'Privacy policy',
    'terms_of_use' => 'Terms of use.',

    'login' => 'Login',
    'register' => 'Register',
    'enter_name' => 'Enter your name',
    'enter_email' => 'Enter your email',
    'enter_password' => 'Enter your password',
    'retype_password' => 'Retype your password',
    'reset_password' => 'Reset password',
    'email_address' => 'Email address',
    'send_password_reset_link' => 'Send password reset link',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',

];
