<?php

return [
    'save' => 'Enregistrer',
    'submit_for_approval' => 'Demande d\'acceptance',
    'delete' => 'Supprimer',
];