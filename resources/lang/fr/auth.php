<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ces informations d\'identification ne correspondent pas à nos donnees',
    'throttle' => 'Trop de tentatives de connexion. Veuillez réessayer dans :seconds secondes.',

    'sign_into_your' => 'Connectez-vous à votre',
    'create_an_account' => 'Créez un compte',
    'administrator_account' => 'Compte Administrateur',
    'delivers_portal' => 'Portail des livraisons',
    'business_profile' => 'Profil d\'entreprise',
    'sign_into_your_account' => 'Connectez-vous',
    'remember_me' => 'Souviens-toi de moi',
    'dont_have_an_account' => 'N\'a pas de compte,',
    'already_have_an_account' => 'Vous avez deja de compte,',
    'subscribe_here' => 'Abonnez-vous ici',
    'login_here' => 'Connectez-vous ici',
    'forgot_your_password?' => 'Mot de passe oublié?',
    'privacy_policy' => 'Politique de confidentialité',
    'terms_of_use' => 'Conditions d\'utilisation.',

    'login' => 'Se Connecter',
    'register' => 'S\'enregistrer',
    'enter_name' => 'Entrez votre nom',
    'enter_email' => 'Entrer votre Email',
    'enter_password' => 'Tapez votre mot de passe',
    'retype_password' => 'Retaper votre mot de passe',
    'reset_password' => 'Réinitialiser le mot de passe',
    'email_address' => 'Adresse e-mail',
    'send_password_reset_link' => 'Réinitialisation du mot de passe',
    'password' => 'Mot de passe',
    'confirm_password' => 'Confirmez le mot de passe',

];
