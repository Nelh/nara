@extends('services.service-layout')

@section('content')

<div class="container-fluid">
    @if($delivery)
    <div class="page-content note-has-grid">
        <div class="tab-content bg-transparent">
            <div class="note-has-grid row">
                <div class="col-md-12 order-note single-note-item all-category">
                    <div class="card card-body">
                        <span class="side-stick"></span>
                        <h5 class="note-title text-truncate w-75 mb-0">Order tracker
                            <i class="point fas fa-circle ml-1 font-10"></i></h5>
                        <p class="note-date font-12 text-muted">{{ $delivery->invoiceno }}</p>
                        {{-- vue components --}}
                        <div id="app">
                            <order-progress status="{{ $delivery->status }}" track_id="{{ $delivery->status_track_id }}" order_id="{{ $delivery->id }}"></order-progress>
                        </div>
                        <script src="{{ asset('js/app-vue.js') }}"></script>
                        {{-- vue components --}}
                        <div class="note-content">
                            <p class="note-inner-content text-muted">
                            {{ __('labels.delivered_to') }}: {{ $delivery->deliverysuburb }}, {{ $delivery->deliverycity }} </p>
                        </div>
                        <div class="form-group form-row">
                            <label for="status_id" class="control-label col-lg-12">Status</label>
                                <div class="col-lg-12">
                                <select id="changedstatus" class="dropdown-style input-field input-normal" name="track_id">
                                    @foreach ($orders_status as $status)
                                        <option value="{{ $status->track_id }}" data-name="{{ $status->name }}" data-id="{{ $delivery->id }}" {{ $delivery->status_track_id == $status->id ? 'selected' : '' }}>{{ $status->name }}</option>
                                    @endforeach
                                </select>
                            </div> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="page-content note-has-grid">
        @if(count($orders))
        <ul class="nav nav-pills p-3 bg-white mb-3 rounded-pill align-items-center">
            <li class="nav-item">
                <a href="#" class="nav-link rounded-pill note-link d-flex align-items-center px-2 px-md-3 mr-0 mr-md-2 active">
                    <i class="icon-layers mr-1"></i><span>All Orders</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link rounded-pill note-link d-flex align-items-center px-2 px-md-3 mr-0 mr-md-2"> 
                    <i class="icon-briefcase mr-1"></i><span>Urgent</span></a>
            </li>
        </ul>
        <div class="tab-content bg-transparent">
            <div id="note-full-container" class="note-has-grid row">
                @foreach($orders as $order)
                <div class="col-xl-3 col-lg-4 col-md-6 col-12 order-note single-note-item all-category" data-id="{{ $order->id }}">
                    <div class="card card-body">
                        <span class="side-stick"></span>
                        <h5 class="note-title text-truncate w-75 mb-0">{{ __('labels.delivery_request') }}
                            <i class="point fas fa-circle ml-1 font-10"></i></h5>
                        <p class="note-date font-12 text-muted">{{ $order->invoiceno }}</p>
                        <div class="note-content">
                            <p class="note-inner-content text-muted">
                            {{ __('labels.delivered_to') }}: {{ $order->deliverysuburb }}, {{ $order->deliverycity }} </p>
                        </div>
                        <div class="d-flex align-items-center">
                            <button class="btn btn-primary accept_order">Accepted</button>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    @endif
</div>

<script type="text/javascript">
    $('.accept_order').click(function () {
        var id = $('.order-note').data('id');
        $.post("{{ route('service.accept.delivery') }}", {
            id, _token: "{{ csrf_token() }}",
        }).done(function (data) {
            location.href = data.next;
        });
    });

    // change order status
    $('#changedstatus').change(function(){
        var id = $(this).children("option:selected").data('id');
        var name = $(this).children("option:selected").data('name');
        var track_id = $(this).children("option:selected").val();
        $.post("{{ route('service.changed.status') }}", {
            id, name, track_id, _token: "{{ csrf_token() }}",
        }).done(function(data){
            console.log(data.next);
        });
    });
</script>
@endsection
