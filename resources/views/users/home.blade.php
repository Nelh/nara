@extends('users.user-layout')

@section('content')
<!-- <div class="dlp-spacer  dlp-spacer--medium-underline"></div> -->

@if(count($item_sales))
<div id="app">
    <div class="page-content note-has-grid">
        <div class="tab-content bg-transparent">
            <div class="note-has-grid row">
                @foreach($item_sales as $isale)
                <div class="col-md-12 order-note single-note-item all-category">
                    <div class="card card-body">
                        <span class="side-stick"></span>
                        <h5 class="note-title text-truncate w-75 mb-0">{{ __('labels.order_tracker') }}
                            <i class="point fas fa-circle ml-1 font-10"></i></h5>
                        <p class="note-date font-12 text-muted">{{ $isale->invoiceno }}</p>
                        <order-progress status="{{ $isale->status }}" track_id="{{ $isale->status_track_id }}"
                            order_id="{{ $isale->id }}"></order-progress>
                        <!-- <div class="note-content">
                            <p class="note-inner-content text-muted">
                                {{ __('labels.delivered_to') }}: {{ $isale->deliverysuburb }},
                                {{ $isale->deliverycity }} </p>
                        </div> -->
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app-vue.js') }}"></script>
@else
<div class="yc-section  yc-section--standard">
    <nav class="flex-grid-group">
        <div class="flex-grid-block  flex-grid-block--m-1-2">
            <div class="flex-grid-content">
                <div class="aftersales-choice  card">
                    <a href="/myaccount.htm?action=repairs" class="block-link">
                        <div class="aftersales-choice__content">
                            <h2 class="yc-heading  yc-heading--small  yc-heading--center">
                                Repairs </h2>

                            <p class="yc-paragraph  u-text-center">If an item you purchased needs repairing we'll
                                collect it, inspect it and and get it repaired for you.</p>
                            <div class="button-wrapper  button-wrapper--align-center">

                                <button class="button  button--dark">
                                    Arrange<span class="clip-text  unclip-text--t  clip-text--s  unclip-text--ml">
                                        a</span> repair </button>

                            </div>

                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="flex-grid-block  flex-grid-block--m-1-2">
            <div class="flex-grid-content">
                <div class="aftersales-choice  card  choice-card" data-choice-card="Or">
                    <a href="/myaccount.htm?action=returns" class="block-link">
                        <div class="aftersales-choice__content">
                            <h2 class="yc-heading  yc-heading--small  yc-heading--center">
                                Returns<span class="clip-text  unclip-text--t  clip-text--s  unclip-text--ml"> &amp;
                                    Exchanges</span> </h2>

                            <p class="yc-paragraph  u-text-center">If an item you purchased arrived broken, is faulty or
                                isn't what you had in mind, we're here to help.</p>
                            <div class="button-wrapper  button-wrapper--align-center">
                                <button class="button  button--dark">
                                    Arrange<span class="clip-text  unclip-text--t  clip-text--s  unclip-text--ml">
                                        an</span> exchange </button>

                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </nav>
</div>
@endif

@endsection
