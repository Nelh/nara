@if ($paginator->hasPages())
    <nav class="pagination-wrapper">
        <ul class="pagination  pagination--centered">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="pagination__item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <a href="#">&lsaquo;</a>
                </li>
            @else
                <li class="pagination__item">
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="pagination__item disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="pagination__item pagination__item--active" aria-current="page"><a href="#">{{ $page }}</a></li>
                        @else
                            <li class="pagination__item"><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="pagination__item pagination__item--last">
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li>
            @else
                <li class="pagination__item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <a href="#">&rsaquo;</a>
                </li>
            @endif
        </ul>
    </nav>

    <p class="yc-paragraph  yc-paragraph--small  u-text-center  u-margin-top-20">
        Page {{ $paginator->currentPage() }} of {{ $paginator->lastPage() }} from {{ $paginator->total() }} results.
    </p>
@endif
