@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    {{ __('labels.classification') }}
                    <a class="btn btn-outline-dark float-right" href="{{ route('admin.classifications.add') }}">
                        <i class="fal fa-clipboard-list"></i> {{ __('labels.new_classification') }} 
                    </a>
                </div>
                <div class="card-body">
                    <div id="classificationrecords" class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-nowrap">{{ __('labels.name') }}</th>
                                <th class="text-nowrap">{{ __('labels.type') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($classifications as $classification)
                                <tr data-id="{{ $classification->id }}">
                                    <td>{{ $classification->name }}</td>
                                    <td>{{ $classification->type ?? '-' }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#classificationrecords tbody tr').click(function(){
        location.href = "{{ route('admin.classifications.edit') }}/" + $(this).data('id');
    });
</script>

@endsection