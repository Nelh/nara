@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    Site Records
                    <a class="btn btn-outline-dark float-right" href="{{ route('admin.sites.add') }}">
                        <i class="fal fa-store-alt"></i> New site 
                    </a>
                </div>
                <div class="card-body">
                    <div id="siterecords" class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-nowrap">{{ __('labels.name') }}</th>
                                <th class="text-nowrap">{{ __('labels.email') }}</th>
                                <th class="text-nowrap">{{ __('labels.classification') }}</th>
                                <th class="text-nowrap">{{ __('labels.active') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sites as $site)
                                <tr data-id="{{ $site->id }}">
                                    <td>{{ $site->site_name }}</td>
                                    <td>{{ $site->email }}</td>
                                    <td>{{ $site->classification_name ?? '-' }}</td>
                                    <td>
                                        @if($site->active == \DefConst::ACTIVE)
                                            <div class="badge badge-success"><i class="fal fa-check-circle"></i> {{ __('labels.active') }}</div>
                                        @else
                                            <div class="badge badge-danger"><i class="fal fa-ban"></i> {{ __('labels.notactive') }}</div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#siterecords tbody tr').click(function(){
        location.href = "{{ route('admin.sites.edit') }}/" + $(this).data('id');
    });
</script>

@endsection