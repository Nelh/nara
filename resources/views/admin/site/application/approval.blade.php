@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{ __('labels.application') }} No: {{ $site->recordno }}</h4>
                </div>
                <div class="card-body">
                    @if (session()->has('errors'))
                        <div class="alert alert-dark" role="alert">
                            {{ __('labels.this_site_cannot_be_approve_similar-site_already_exist_in_our_site_list') }}
                        </div>
                    @endif
                    <h5>{{ __('labels.company_name') }} {{ $site->company_name }}</h5>
                    <p>{{ __('labels.email') }} {{ $site->email }}</p>
                    <p>{{ __('labels.industry') }} {{ $site->classification }}</p>
                    <p>{{ __('labels.addr') }} {{ $site->addr }} {{ $site->zip }}</p>
                    <p>{{ __('labels.city') }} {{ $site->city }}</p>
                    <p>{{ __('labels.country') }} {{ $site->country }}</p>
                    <p>{{ __('labels.phone') }} {{ $site->phone }}</p>
                    <p>{{ __('labels.website') }} {{ $site->website ?? '-' }}</p>
                    <p>{{ __('labels.description') }}</p>
                    <p>{{ $site->description }}</p>
                </div>

                <form id="site-approved" action="{{ route('admin.sites.application.approved') }}" method="post">
                    @csrf   
                    <input type="hidden" name="a_id" value="{{ $site->id }}">
                    <input type="hidden" name="name" value="{{ $site->company_name ?? '' }}">
                    <input type="hidden" name="slug_name" value="{{ $site->company_name ?? '' }}">
                    <input type="hidden" name="password" value="{{ $site->passowrd ?? '' }}">
                    <input type="hidden" name="email" value="{{ $site->email ?? '' }}">
                    <input type="hidden" name="classification_id" value="{{ $site->classification_id ?? '' }}">
                    <input type="hidden" name="addr1" value="{{ $site->addr ?? '' }}">
                    <input type="hidden" name="city" value="{{ $site->city ?? '' }}">
                    <input type="hidden" name="zip" value="{{ $site->zip ?? '' }}">
                    <input type="hidden" name="phone1" value="{{ $site->phone ?? '' }}">
                    <input type="hidden" name="website" value="{{ $site->website ?? '' }}">
                    <input type="hidden" name="description" value="{{ $site->description ?? '' }}">
                </form>

                <form id="site-rejected" action="{{ route('admin.sites.application.rejected') }}" method="post">
                    @csrf   
                    <input type="hidden" name="a_id" value="{{ $site->id }}">
                </form>
                <div class="card-footer">
                    <button class="btn btn-success btn-approval btn-lg">{{ __('buttons.approval') }}</button>
                    <button class="btn btn-danger btn-rejected btn-lg float-right">{{ __('buttons.rejected') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.btn-approval').click(function(){
        var confirmApproval = confirm("{{ __('labels.confirm_site_approval') }}");
            if (confirmApproval == true) {
                // generating slug name
                var key = $('input[name="name"]').val()
                key = key.replace(/ /g, "-").toLowerCase();
                $('input[name="slug_name"]').val(key);
                // generating pwd
                var pwd = String.fromCodePoint(...Array.from({length: 10}, () => Math.floor(Math.random() * 57) + 65))
                $('input[name="password"]').val(pwd);
                $('#site-approved').submit();
            }
    });

    $('.btn-rejected').click(function(){
        var confirmRejection = confirm("{{ __('labels.confirm_site_rejection') }}");
            if (confirmRejection == true) {
                $('#site-rejected').submit();
            }
    });
</script>
@endsection