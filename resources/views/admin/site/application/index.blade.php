@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @if(count($applications))
                <div class="card-body">
                    <div id="siterecords" class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-nowrap">{{ __('labels.status') }}</th>
                                <th class="text-nowrap">{{ __('labels.company_name') }}</th>
                                <th class="text-nowrap">{{ __('labels.email') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($applications as $application)
                                <tr data-id="{{ $application->id }}">
                                    <td>
                                        @if($application->status == \DefConst::PENDING)
                                            <div class="badge badge-warning">{{ __('labels.pending') }}</div>
                                        @endif
                                    </td>
                                    <td>{{ $application->company_name }}</td>
                                    <td>{{ $application->email }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @else
                    <div class="text-center p-4">
                        <div><i class="fal fa-cabinet-filing fa-2x text-secondary"></i></div>
                        <h4 class="py-2">{{ __('labels.no_new_application_yet') }}</h4>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#siterecords tbody tr').click(function(){
        location.href = "{{ route('admin.sites.application.approval') }}/" + $(this).data('id');
    });
</script>

@endsection