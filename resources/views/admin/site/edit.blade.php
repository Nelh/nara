@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.sites.update') }}">
                        @csrf
                        <input type="hidden" name="id" value="{{ $site->id }}">
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="customSwitch1" name="active"
                                    @if ($site->active == \DefConst::ACTIVE) checked @endif>
                                    <label class="custom-control-label" for="customSwitch1">{{ __('labels.activate_site') }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="name" class="mb-1">{{ __('labels.name') }}*</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ?? $site->name ?? '' }}" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="slug_name" class="mb-1">{{ __('labels.slug_name') }}</label>
                                <input type="text" class="form-control" id="slug_name" name="slug_name" value="{{ old('slug_name') ?? $site->slug_name ?? '' }}" required readonly>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="email" class="mb-1">{{ __('labels.email') }}*</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') ?? $site->email ?? '' }}">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="classification" class="mb-1">{{ __('labels.classification') }}</label>
                                <select class="custom-select" id="classification" name="classification_id">
                                    <option value="">{{ __('labels.select_classification') }}</option>
                                    @foreach($classifications as $classification)
                                        <option value='{{ $classification->id }}' {{ $site->classification_id == $classification->id ? 'selected' : '' }}>{{ $classification->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="addr1" class="mb-1">{{ __('labels.address1') }}</label>
                                <input type="text" class="form-control" id="addr1" name="addr1" value="{{ old('addr1') ?? $site->addr1 ?? '' }}">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="addr2" class="mb-1">{{ __('labels.address2') }}</label>
                                <input type="text" class="form-control" id="addr2" name="addr2" value="{{ old('addr2') ?? $site->addr2 ?? '' }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="city" class="mb-1">{{ __('labels.city') }}</label>
                                <select class="custom-select" id="city" name="city" required="">
                                    <option value="">{{ __('labels.select_cities') }}</option>
                                    @foreach($cities as $city)
                                        <option value='{{ $city->city_code }}' {{ $city->city_code == $site->city ? 'selected' : ''}}>{{ $city->city_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-2 form-group">
                                <label for="zip" class="mb-1">{{ __('labels.zip') }}</label>
                                <input type="text" class="form-control" id="zip" name="zip" value="{{ old('zip') ?? $site->zip ?? '' }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="phone1" class="mb-1">{{ __('labels.phone1') }}</label>
                                <input type="text" class="form-control" id="phone1" name="phone1" value="{{ old('phone1') ?? $site->phone1 ?? '' }}">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="phone2" class="mb-1">{{ __('labels.phone2') }}</label>
                                <input type="text" class="form-control" id="phone2" name="phone2" value="{{ old('phone2') ?? $site->phone2 ?? '' }}">
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="description" class="mb-1">{{ __('labels.description') }}</label>
                                <textarea rows="5" class="form-control" id="description" name="description">{{ old('description') ?? $site->description ?? '' }}</textarea>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="website" class="mb-1">{{ __('labels.website') }}</label>
                                <input type="text" class="form-control" id="website" name="website" value="{{ old('website') ?? $site->website ?? '' }}">
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-12 form-group">
                                <button type="submit" class="btn btn-primary btn-submit btn-lg float-right">{{ __('buttons.save') }}</button>
                                <a href="{{ route('admin.sites') }}" class="btn btn-outline-dark btn-lg float-right mr-2"> {{ __('labels.back') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#name').keyup(function(){
            slugKeyName();
        });

        $('.btn-submit').click(function(){
            slugKeyName();
        });

        function slugKeyName() {
            var key = $('#name').val();
            key = key.replace(/ /g, "-").toLowerCase();
            $('#slug_name').val(key);
        }
    });
</script>
@endsection