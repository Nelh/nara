@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
        @slot('section')
        {{ 'details' }}
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    product Records
                </div>
                <div class="card-body">
                    <div id="productrecords" class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-nowrap">{{ __('labels.name') }}</th>
                                <th class="text-nowrap">{{ __('labels.recordno') }}</th>
                                <th class="text-nowrap">{{ __('labels.brand_name') }}</th>
                                <th class="text-nowrap">{{ __('labels.category_name') }}</th>
                                <th class="text-nowrap">{{ __('labels.active') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr data-id="{{ $product->id }}">
                                    <td>{{ $product->product_name }}</td>
                                    <td>{{ $product->recordno ?? '-' }}</td>
                                    <td>{{ $product->brand_name  ?? '-' }}</td>
                                    <td>{{ $product->category_name ?? '-' }}</td>
                                    <td>{{ $product->active  ?? '-'}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection