@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">{{ __('labels.upload_card_website_image') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.customisation.card.save') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <label for="subheading" class="mb-1">{{ __('labels.subheading') }}</label>
                                        <input type="text" class="form-control" id="subheading" name="subheading" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <label for="heading" class="mb-1">{{ __('labels.heading') }}</label>
                                        <input type="text" class="form-control" id="heading" name="heading" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <label for="color" class="mb-1">{{ __('labels.color') }}</label>
                                        <input type="color" class="form-control" id="color" name="color" value="#edaf55">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="mb-1">{{ __('labels.upload_card_website_image') }}</label>
                                    </div>
                                    <div class="col-lg-12 form-group input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-file-image"></i></span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="{{ \DefConst::IMGTYPE_WEB_CARD }}">
                                            <label class="custom-file-label">{{ __('labels.choose_file') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="quad-banner  js-quad-banner  flex-grid-content">
                                    <img src="{{ url('images/placeholder-item.png') }}" class="yc-simple-image  yc-simple-image--full lazyload" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-group">
                                <button type="submit" class="btn btn-dark btn-submit btn-lg">{{ __('buttons.save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection