@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">{{ __('labels.upload_card_website_image') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.customisation.card.update') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="cid" value="{{ $cardx->id }}">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <label for="subheading" class="mb-1">{{ __('labels.subheading') }}</label>
                                        <input type="text" class="form-control" id="subheading" name="subheading" value="{{ $cardx->subheading }}" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <label for="heading" class="mb-1">{{ __('labels.heading') }}</label>
                                        <input type="text" class="form-control" id="heading" name="heading" value="{{ $cardx->heading }}" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <label for="color" class="mb-1">{{ __('labels.color') }}</label>
                                        <input type="color" class="form-control" id="color" name="color" value="{{ $cardx->color ?? '#edaf55' }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="mb-1">{{ __('labels.upload_card_website_image') }}</label>
                                    </div>
                                    <div class="col-lg-12 form-group input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-file-image"></i></span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="{{ \DefConst::IMGTYPE_WEB_CARD }}">
                                            <label class="custom-file-label">{{ __('labels.choose_file') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                @if($cardx->url)
                                <div class="quad-banner  js-quad-banner  flex-grid-content">
                                    <img src="{{ $cardx->url }}" class="yc-simple-image  yc-simple-image--full lazyload" alt="">
                                    <div class="quad-banner__text  quad-banner__text--top">
                                        <span class="quad-banner__lead-in">{{ $cardx->subheading }}</span>
                                        <span class="quad-banner__title">{{ $cardx->heading }}</span>
                                    </div>
                                </div>
                                @else
                                <div class="quad-banner  js-quad-banner  flex-grid-content">
                                    <img src="{{ url('images/placeholder-item.png') }}" class="yc-simple-image  yc-simple-image--full lazyload" alt="">
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-group">
                                <button id="delete-card" type="button" class="btn btn-outline-danger btn-lg"><i class="fal fa-trash-alt"></i></button>
                                <button type="submit" class="btn btn-dark btn-submit btn-lg">{{ __('buttons.save') }}</button>
                            </div>
                        </div>
                    </form>

                    <form id="delete-form" method="POST" action="{{ route('admin.customisation.card.delete') }}">
                        @csrf
                        <input type="hidden" name="cid" value="{{ $cardx->id }}">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#delete-card').click(function(){
        $('#delete-form').submit();
    });
</script>
@endsection