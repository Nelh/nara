@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-outline-dark float-right" href="{{ route('admin.customisation.card.add') }}">
                        {{ __('labels.add_new_card') }}
                    </a>
                </div>
                <div class="card-body">
                    <div id="webcardrecords" class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-nowrap">{{ __('labels.subheading') }}</th>
                                <th class="text-nowrap">{{ __('labels.heading') }}</th>
                                <th class="text-nowrap">{{ __('labels.color') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cardsx as $card)
                                <tr data-id="{{ $card->id }}">
                                    <td>{{ $card->subheading }}</td>
                                    <td>{{ $card->heading ?? '-' }}</td>
                                    <td>{{ $card->color  ?? '-' }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#webcardrecords tbody tr').click(function(){
        location.href = "{{ route('admin.customisation.card.edit') }}/" + $(this).data('id');
    });
</script>
@endsection