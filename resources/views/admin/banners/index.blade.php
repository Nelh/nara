@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @if(count($sitex))
                <div class="card-body">
                    <div id="siteapprovals" class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-nowrap">{{ __('labels.status') }}</th>
                                <th class="text-nowrap">{{ __('labels.company_name') }}</th>
                                <th class="text-nowrap">{{ __('labels.banner_name') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sitex as $sx)
                                <tr data-id="{{ $sx->id }}">
                                    <td>
                                        @if($sx->status == \DefConst::PENDING)
                                            <div class="badge badge-warning">{{ __('labels.pending') }}</div>
                                        @endif
                                    </td>
                                    <td>{{ $sx->site_name }}</td>
                                    <td>{{ $sx->banner }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @else
                    <div class="text-center p-4">
                        <div><i class="fal fa-cabinet-filing fa-2x text-secondary"></i></div>
                        <h4 class="py-2">{{ __('labels.no_new_banner_waiting_for_approval') }}</h4>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#siteapprovals tbody tr').click(function(){
        location.href = "{{ route('admin.banner.approval') }}/" + $(this).data('id');
    });
</script>

@endsection