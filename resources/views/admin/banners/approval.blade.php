@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    <div class="yc-section  yc-section--standard">
        <div class="hero-banner">
            <div class="hero-banner__image-wrapper  u-bg-casablanca" style="background-color: {{ $sitex->banner_color }}">
                <picture>
                    <img src="{{ $sitex->site_banner ?? asset('images/placeholder-site.png') }}" class="hero-banner__image  lazyload" alt="">
                </picture>
            </div>
            <div class="hero-banner__text-wrapper  u-bg-casablanca" style="background-color: {{ $sitex->banner_color }}">
                <div class="hero-banner__text  hero-banner__text--light">
                    <div class="hero-banner__logo-wrapper">
                        <h5>{{ $sitex->site_name }}</h5>
                    </div>
                    <h2 class="hero-banner__title">{{ $sitex->banner ?? '' }}</h2>
                    <p class="hero-banner__description">{{ $sitex->note ?? '' }}</p>
                    <div class="hero-banner__cta">
                        <button class='button  button--full  button--muted-bordered'>
                            Shop Now </button>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <div class="row">
        <div class="col-lg-6 offset-lg-3">
            <div class="card">
                <div class="card-body">
                    <h5>{{ __('labels.status') }} {{ $sitex->status }}</h5>
                    <p>{{ __('labels.site_name') }} {{ $sitex->site_name }}</p>
                    <p>{{ __('labels.banner') }} {{ $sitex->banner }}</p>
                </div>
                <form id="banner-approved" action="{{ route('admin.banner.approved') }}" method="post">
                    @csrf   
                    <input type="hidden" name="banner_id" value="{{ $sitex->id }}">
                </form>

                <form id="banner-rejected" action="{{ route('admin.banner.rejected') }}" method="post">
                    @csrf   
                    <input type="hidden" name="banner_id" value="{{ $sitex->id }}">
                </form>

                <div class="card-footer">
                    <button class="btn btn-success btn-approval btn-lg">{{ __('buttons.approval') }}</button>
                    <button class="btn btn-danger btn-rejected btn-lg float-right">{{ __('buttons.rejected') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.btn-approval').click(function(){
        var confirmApproval = confirm("{{ __('labels.confirm_banner_approval') }}");
            if (confirmApproval == true) {
                $('#banner-approved').submit();
            }
    });

    $('.btn-rejected').click(function(){
        var confirmRejection = confirm("{{ __('labels.confirm_banner_rejection') }}");
            if (confirmRejection == true) {
                $('#banner-rejected').submit();
            }
    });
</script>
@endsection