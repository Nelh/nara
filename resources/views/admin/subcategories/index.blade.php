@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    {{ __('labels.subcategory') }}
                    <a class="btn btn-outline-dark float-right" href="{{ route('admin.subcategory.add') }}">
                        <i class="fal fa-clipboard-list"></i> {{ __('labels.new_subcategory') }} 
                    </a>
                </div>
                <div class="card-body">
                    <div id="subcategoryrecords" class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-nowrap">{{ __('labels.name') }}</th>
                                <th class="text-nowrap">{{ __('labels.category_name') }}</th>
                                <th class="text-nowrap">{{ __('labels.type') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($subcategories as $subcategory)
                                <tr data-id="{{ $subcategory->id }}">
                                    <td>{{ $subcategory->name }}</td>
                                    <td>{{ $subcategory->category_name ?? '-' }}</td>
                                    <td>{{ $subcategory->type ?? '-' }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#subcategoryrecords tbody tr').click(function(){
        location.href = "{{ route('admin.subcategory.edit') }}/" + $(this).data('id');
    });
</script>

@endsection