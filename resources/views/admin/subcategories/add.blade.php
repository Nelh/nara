@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.subcategory.save') }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="name" class="mb-1">{{ __('labels.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="category" class="mb-1">{{ __('labels.category') }}</label>
                                <select class="custom-select" id="category" name="category_id" required>
                                    <option value>{{ __('labels.select_category') }}</option>
                                    @foreach($categories as $category)
                                        <option value='{{ $category->id }}'>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-group">
                                <button type="submit" class="btn btn-primary btn-submit btn-lg">{{ __('buttons.save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection