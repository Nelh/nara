@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in As admin!
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mt-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Setup Store</div>
                <div class="card-body">
                    <div class = "row">
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('admin.sites') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">  
                                        <i class="fal fa-store-alt fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        {{ __('labels.store') }}
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('admin.sites.application.index') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-cabinet-filing fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        {{ __('labels.store_application') }}
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('admin.products') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-box-full fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        Stock
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('admin.category') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-archive fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        Categories
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('admin.subcategory') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-chalkboard-teacher fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        Sub Categories
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="#">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-dolly fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        Suppliers
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('admin.banner') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-money-check fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        Banner Approval
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('admin.classifications') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-clipboard-list fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        Classifications
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row justify-content-center mt-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Setup Delivery</div>
                <div class="card-body">
                    <div class = "row">
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="#">
                                <div class="row align-items-center">
                                    <div class="col-3">  
                                        <i class="fal fa-users fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        Delivers
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="#">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-location fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        Tracking
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="#">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-box-check fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        Orders
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mt-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">website customisation</div>
                <div class="card-body">
                    <div class = "row">
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('admin.customisation.card') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">  
                                        <i class="fal fa-cogs fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        {{ __('labels.create_pub_card') }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
