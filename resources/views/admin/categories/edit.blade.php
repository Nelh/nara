@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.category.update') }}">
                        @csrf
                        <input type="hidden" name="id" value="{{ $category->id }}">
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="name" class="mb-1">{{ __('labels.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ?? $category->name ?? '' }}" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="type" class="mb-1">{{ __('labels.category_type') }}</label>
                                <select class="custom-select" id="type" name="type">
                                    <option value="">{{ __('labels.select_type') }}</option>
                                    @foreach(\DefConst::TYPES as $val)
                                        <option value='{{ $val }}' {{ $category->type == $val ? 'selected' : '' }}>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-group">
                                <button type="submit" class="btn btn-primary btn-submit btn-lg">{{ __('buttons.save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection