@extends('admin.admin-layout')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    {{ __('labels.category') }}
                    <a class="btn btn-outline-dark float-right" href="{{ route('admin.category.add') }}">
                        <i class="fal fa-clipboard-list"></i> {{ __('labels.new_category') }} 
                    </a>
                </div>
                <div class="card-body">
                    <div id="categoryrecords" class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-nowrap">{{ __('labels.name') }}</th>
                                <th class="text-nowrap">{{ __('labels.type') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr data-id="{{ $category->id }}">
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->type ?? '-' }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#categoryrecords tbody tr').click(function(){
        location.href = "{{ route('admin.category.edit') }}/" + $(this).data('id');
    });
</script>

@endsection