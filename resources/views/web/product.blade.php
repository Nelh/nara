@extends('web.web-layout')

@section('content')
<div class="yc-page-content  yc-page-content--standard">
    <div class="product-page yc-section">
        <div class="notification"></div>
        <div class="flex-grid-group  flex-grid-group--align-left">
            <div class="flex-grid-block  flex-grid-block--t-full">
                <div class="flex-grid-content">
                    <ul class="breadcrumbs group">
                        <li class="breadcrumbs__item">
                            <a href="{{ route('shop.products') }}" class="breadcrumb__link">{{ __('labels.home') }}</a>
                        </li>
                        @if($product->category_name)
                        <li class="breadcrumbs__item">
                            <a href="{{ route('shop.products.sort.category', ['cat_id' => $product->category_id]) }}"
                                class="breadcrumb__link">{{ $product->category_name }}</a>
                        </li>
                        @endif
                        <li class="breadcrumbs__item">
                            <a href="#" class="breadcrumb__link">{{ $product->item_name }}</a>
                        </li>
                    </ul>
                    <h1 class="product-page-title  yc-heading  yc-heading--large  yc-heading--flush  yc-heading--sentencecase">
                        <b><a href="#" class="text-link  text-link--unmarked">{{ $product->item_name }}</a></b>
                    </h1>
                    <ul class="reviews-meta">
                        <li class="reviews-meta__item">
                            <div class="star-rating  star-rating--large  star-rating--{{ $product->ratings ?? 0}}"
                                title="{{ $product->ratings / 10 }} out of 5 stars">
                                <span class="hidden-from-view">{{ $product->ratings / 10 }} out of 5 stars</span>
                            </div>
                        </li>
                        <!-- <li class="reviews-meta__item">
                            <a href="#reviews" class="text-link  text-link--muted  js-smoothscroll">60 reviews</a>
                        </li>
                        <li class="reviews-meta__item">
                            <a class="text-link  text-link--muted  write_review" href="#">Review this item</a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="flex-grid-group  flex-grid-group--align-left  u-margin-top-24">
            <div class="flex-grid-block  flex-grid-block--t-order-02  flex-grid-block--ml-2-5">
                <div class="flex-grid-content">
                    <input type="hidden" id="noOfVariationsOnPage" value="1" />
                    <div class="product-page-price-block">
                            <!-- <p
                                class="yc-paragraph  yc-paragraph--flush  yc-paragraph--small-force  u-text-center  u-margin-bottom-8">
                                <span class="text-pill  text-pill--tiny  text-pill--success  u-text-uppercase">Free
                                    delivery</span> <span class="u-display-block--t  u-display-inline--ml">on this
                                    item.</span>
                            </p> -->

                            <div class="js-product-page-price-wrapper">
                                <p
                                    class="product-page-price  yc-heading  yc-heading--large  yc-heading--sentencecase  yc-heading--flush  u-text-center">
                                    <b>
                                        <span class="selectedPrice">
                                            <span itemprop="price"
                                                content="{{ $product->price }}">@money($product->price, \DefConst::CURRENCY)</span>
                                        </span></b>
                                </p>
                                <div class="js-price-was">
                                </div>
                            </div>
                        </div>

                        <div class="addtocartarea expandedlist  u-margin-top-24">

                            <p class="yc-paragraph  yc-paragraph--small-force  yc-paragraph--flush">{{ __('labels.item') }} Code:
                                <span class="selectedFeatureCode">{{ $product->recordno }}</span></p>

                            <div class="qtySelector  u-margin-top-24" id="selector{{ $product->id }}"
                                vid="{{ $product->id }}" show_notify="No" show_add="Yes" show_price="Yes"
                                show_wish="Yes" show_registry="Yes" price="{{ $product->price }}" new="Yes"
                                code="{{ $product->recordno }}" selling_packsize="1" display_packsize="1"
                                packdescription="Case">
                                <p class="yc-paragraph  yc-paragraph--small  yc-paragraph--flush  featureDetails">
                                    <span class="features"> </span> </p>

                                <div class="feedback  feedback--small  feedback--flush  feedback--in-stock">
                                    <div class="feedback__icon  u-hide-block  u-display-block--ml">
                                        <i class="svg-icon  svg-icon--inline  svg-icon--in-stock"></i>
                                    </div>
                                    <div class="feedback__message">
                                        <div class="feedback-title">
                                            {{ __('labels.in_stock_ship_in_few_days')}}
                                        </div>
                                        <div class="feedback-description">
                                            {{ __('labels.due_to_high_volume_might_be_delay')}}
                                        </div>
                                    </div>
                                </div>

                                <div class="u-margin-top-24">
                                    <div class="flex-grid-group">
                                        <div class="flex-grid-block">
                                            <div class="flex-grid-content">
                                                <div class="yc-input-combo  yc-input-combo--quantity  ie-group">
                                                    <button type="button"
                                                        class="decreaseQtyActive  button tooltip tooltip--top"
                                                        data-tooltip="Remove 1">-</button>
                                                    <input type="text" readonly="readonly" name="qty"
                                                        value="{{ $product->qty ?? 1 }}" size="3"
                                                        class="yc-text-input  cartqtychange" />
                                                    <button type="button"
                                                        class="increaseQtyActive  button tooltip tooltip--top"
                                                        data-tooltip="Add 1">+</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="itemAddedAlert"></div>
                            <div class="u-margin-top-24">
                                <div id="addtoCart">
                                    <button type="button" id="add_cart"
                                        class='button  button--huge  button--full  button--add-to-cart  button--dark  js-add-to-cart'
                                        data-load-with-icon='svg-icon  svg-icon--inline  svg-icon--text-left  svg-icon--loading  svg-icon--is-light  u-animate-spin'>
                                        <i class='svg-icon  svg-icon--cart  svg-icon--is-light  svg-icon--inline  svg-icon--text-left'></i>
                                        {{ __('labels.add_to_cart') }}
                                    </button>
                                </div>
                            </div>
                        </div>

                    <div class="u-margin-top-16">
                        <div class="flex-grid-group">
                            <div class="flex-grid-block  flex-grid-block--l-1-2">
                                <div class="flex-grid-content">
                                    <button type='submit' class='button  button--small  button--full  button--muted-bordered'>
                                        <i class='svg-icon  svg-icon--heart  svg-icon--is-dark  svg-icon--small  svg-icon--inline  svg-icon--text-left'></i>
                                        {{ __('labels.add_to_wishlist') }}
                                    </button>
                                </div>
                            </div>
                            <div class="flex-grid-block  flex-grid-block--l-1-2">
                                <div class="flex-grid-content">
                                    <a href="{{ route('shop.cart') }}" class='button  button--small  button--full  button--muted-bordered'>
                                        {{ __('labels.view_cart') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="yc-paragraph  yc-paragraph--small  yc-paragraph--flush  u-margin-top-16  u-text-center">
                        <i class="svg-icon  svg-icon--inline  svg-icon--small  svg-icon--lock  svg-icon--6-5  svg-icon--in-copy"></i>
                        <b>{{ __('labels.shopping_with_us_is_safe_and_secure') }}</b>
                    </p>

                    <!-- <div class="variation-qr  js-variation-qr">
                        <div class="flex-grid-group">
                            <div
                                class="flex-grid-block  flex-grid-block--full  flex-grid-block--ml-1-2  flex-grid-block--l-2-6">
                                <div class="flex-grid-content">
                                    <div class="variation-qr__image">
                                        <img src="/api/qr/variation/?margin=0" alt=""
                                            class="yc-simple-image  yc-simple-image--full  js-product-qr-uri">
                                    </div>
                                </div>
                            </div>
                            <div
                                class="flex-grid-block  flex-grid-block--full  flex-grid-block--ml-1-2  flex-grid-block--l-4-6">
                                <div class="flex-grid-content">
                                    <div class="variation-qr__text">
                                        <p class="yc-paragraph  yc-paragraph--flush"><b
                                                class="js-product-qr-name">View on mobile</b></p>
                                        <p class="yc-paragraph  yc-paragraph--small  yc-paragraph--flush">Scan the
                                            QR code to open this page on your mobile phone.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    @if($product->site_image)
                    <div class="u-block-center  u-max-width-240">
                        <img src="{{ $product->site_image }}" class="yc-simple-image  yc-simple-image--full  yc-simple-image--center  u-margin-bottom-10">
                    </div>
                    @endif
                </div>
            </div>
            <div class="flex-grid-block  flex-grid-block--t-order-01  flex-grid-block--ml-3-5">
                <div class="flex-grid-content">
                    <div class="product-image-wrapper">
                        <div class="product-image-wrapper__image-wrap  mainpic-image">
                            <img src="{{ $product->url }}" alt="{{ $product->item_name }}"
                                class="mainpic  yc-simple-image  yc-simple-image--full">

                            <span id="js-product-image-caption-text"
                                class="product-image-caption  text-pill  text-pill--medium  text-pill--light"
                                style="display: none;"></span>

                            <img style="display:none;"
                                class="js-variation-sticker-when-special  product-image-sticker  product-image-sticker--right" />

                            <div class="js-variation-sticker-when-new  product-image-sticker" style="display:none;">
                                <span class="text-pill  text-pill--medium  text-pill--new  u-text-uppercase">{{ __('labels.new') }}</span>
                            </div>
                            <a href="#"
                                class="product-image-zoom-button tooltip  tooltip--top  js-mfp-open  js-zoom-img-trigger"
                                target_class="js-image-popup" data-tooltip="Zoom Image">
                                <i class="svg-icon  svg-icon--block  svg-icon--zoom  svg-icon--is-dark"></i>
                                <span class="u-hidden-from-view">Zoom Image</span>
                            </a>
                        </div>
                        <div class="product-image-wrapper__model-wrap  mainpic-model" style="display:none">
                            <div class="sketchfab-embed-wrapper  responsive-iframe-wrapper  responsive-iframe-wrapper--1x1"
                                modeluid="">
                            </div>
                        </div>


                    </div>


                    <ul id="gallery_icons" class="product-gallery-nav">
                        <li class="product-gallery-nav__item  js-product-gallery-video-nav" style="display:none;">
                            <a href="#js-video-gallery"
                                class="product-gallery-nav__link  product-gallery-nav__link--media  js-smoothscroll">
                                <i
                                    class="svg-icon  svg-icon--block  svg-icon--film-strip  svg-icon--4-3  u-block-center"></i>
                                <span>Videos</span>
                            </a>
                        </li>
                        <li class="product-gallery-nav__item">
                            <a vid="{{ $product->id }}" alt=""
                                href="{{ $product->url ?? url('images/placeholder-item.png') }}"
                                class="gallerythumb  product-gallery-nav__link" type="image" id="picture1"
                                data-originalwidth="{{ $product->xsize }}" data-originalheight="{{ $product->ysize }}"
                                highreslink="{{ $product->url ?? url('images/placeholder-item.png') }}"
                                highressize="800 x 800"
                                zoom-img="{{ $product->url ?? url('images/placeholder-item.png') }}">
                                <img title="Click to enlarge" alt="" data-description=""
                                    src="{{ $product->url ?? url('images/placeholder-item.png') }}" width="70"
                                    class="product-gallery-nav__thumbnail">
                            </a>
                        </li>

                        <li class="product-gallery-nav__item  js-product-gallery-articles-nav" style="display: none;">
                            <a href="#related_articles"
                                class="product-gallery-nav__link  product-gallery-nav__link--media  js-smoothscroll">
                                <i class="svg-icon  svg-icon--block  svg-icon--book  svg-icon--4-3  u-block-center"></i>
                                <span>Articles</span>
                            </a>
                        </li>
                    </ul>

                    <div class="yc-popup  yc-popup--zoom  mfp-hide  js-image-popup">
                        <div class="yc-popup__zoom-content">
                            <div class="yc-popup__zoom-loader  js-zoom-img-loading" style="display: none;">
                                <div class="u-block-center  u-max-width-48">
                                    <i
                                        class="svg-icon  svg-icon--block  svg-icon--large  svg-icon--loading  svg-icon--2-2  u-animate-spin"></i>
                                </div>
                                <p class="yc-paragraph  yc-paragraph--flush  u-text-center  u-margin-top-16">
                                    Loading...</p>
                            </div>
                            <img id="zoom-img"
                                class="yc-simple-image  yc-simple-image--full  yc-simple-image--center  js-zoom-img  lazyload"
                                data-img-src="" src="" alt="" />
                        </div>
                        <div class="js-zoom-img-tip  yc-popup__zoom-tip" style="display: none;">
                            <span class="text-pill  text-pill--medium  text-pill--light">Click on image to
                                zoom</span>
                        </div>
                    </div>
                    <div class="product-content">
                        @if($product->description)
                        <div class="product-content__section">
                            <h2 class="yc-heading  yc-heading--medium"><b>{{ __('labels.item') }}</b> Info</h2>
                            <div class="yc-simple-text  yc-product-copy">
                                <p>{{ $product->description }} </p>
                            </div>
                        </div>
                        @endif

                        @if($product->specification)
                        <div class="product-content__section">
                            <h2 class="yc-heading  yc-heading--medium"><b>{{ __('labels.specification') }}</b></h2>
                            <div class="yc-simple-text  yc-product-copy">
                                {{ $product->specification }}
                            </div>
                        </div>
                        @endif

                        @if($product->warranty)
                        <div class="product-content__section">
                            <h2 class="yc-heading  yc-heading--medium"><b>Warranty</b> Info</h2>
                            <div class="yc-simple-text  yc-product-copy">
                                <p>This product comes with a {{ $product->warranty }} warranty.</p>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    $(".decreaseQtyActive").click(function () {
        var quantity = $(this).next();
        if (quantity.val() > 1) {
            quantity.val(parseInt(quantity.val()) - 1);
        }
    });
    // increase Item qty
    $(".increaseQtyActive").click(function () {
        var quantity = $(this).prev();
        quantity.val(parseInt(quantity.val()) + 1);
    });

    $('#add_cart').click(function () {
        var qty = $('.cartqtychange').val();
        $.get("{{ route('shop.additemtocart') }}", {
            id: "{{ $product->id }}",
            qty: qty
        }).done(function (data) {
            // window.location.reload();
            $('.notification').html(`
            <div class="feedback  feedback--success  ie-group">
                <div class="feedback__icon"> 
                    <i class="svg-icon  svg-icon--inline  svg-icon--cart"></i>
                </div>
                <div class="feedback__message">
                    <h4 class="feedback-title">${data.msg}</h4>
                </div>
            </div>
            `);
            $('.user-cart__count-text').html(data.items_qty);
        });
    });

</script>
@endsection
