@extends('web.web-layout')

@section('content')
<!-- <div class="static-banner__relative-content">
    <picture>
        <img src="{{ url('images/12.jpg') }}" class="static-banner__image  ls-is-cached lazyloaded" >
    </picture>
    <h1 class="title-banner__title  title-banner__title--uppercase">
        <span class="u-block">
            <span class="title-banner__main-text">{{ __('labels.trade_partner') }}</span>
        </span>
    </h1>
</div> -->

<div class="yc-page-content  yc-page-content--standard">
    <div class="yc-section  yc-section--standard">
        <header class="yc-intro-text">
            <h1 class="yc-heading  yc-heading--large  yc-heading--center">{{ __('labels.trade_partner') }}</h1>
        </header>
    </div>
    <div class="yc-section  yc-section--medium">
        <nav class="flex-grid-group">
            <div class="flex-grid-block  flex-grid-block--m-1-2">
                <div class="voucher-choice  card  flex-grid-content">
                    <a href="{{ route('shop.sites.application') }}" class="block-link">
                        <div class="voucher-choice__hero">
                            <picture>
                                <img src="{{ url('images/business.jpg') }}" class="yc-simple-image  yc-simple-image--full  lazyloaded">
                            </picture>
                        </div>
                        <div class="voucher-choice__content">
                            <h2 class="yc-heading  yc-heading--medium  yc-heading--center  yc-heading--flush">{{ __('labels.open_store') }}</h2>

                            <div class="u-max-width-320  u-block-center">
                                <p class="yc-paragraph  yc-paragraph--small  u-text-center  js-match-copy">{{ __('labels.open_store') }}</p>
                            </div>
                            <div class="voucher-choice__button-wrapper">
                                <span class="button  button--dark">{{ __('labels.apply') }}</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="flex-grid-block  flex-grid-block--m-1-2">
                <div class="voucher-choice  card  choice-card  flex-grid-content" data-choice-card="Or">
                    <a href="{{ route('service.login') }}" class="block-link">
                        <div class="voucher-choice__hero">
                            <picture>
                                <img src="{{ url('images/delivers.jpg') }}" class="yc-simple-image  yc-simple-image--full  lazyloaded">
                            </picture>
                        </div>
                        <div class="voucher-choice__content">
                            <h2 class="yc-heading  yc-heading--medium  yc-heading--center  yc-heading--flush">{{ __('labels.become_deliver') }}</h2>

                            <div class="u-max-width-320  u-block-center">
                                <p class="yc-paragraph  yc-paragraph--small  u-text-center  js-match-copy">{{ __('labels.join_our_delivery_team_today') }}</p>
                            </div>
                            <div class="voucher-choice__button-wrapper">
                                <span class="button  button--dark">{{ __('labels.apply') }}</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </nav>
    </div>
</div>

@endsection
