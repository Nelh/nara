@extends('web.web-layout')

@section('content')
<div id="content">
    <h1 class="u-text-center">{{ __('labels.shopping_cart') }}</h1>

    @if(count($products))
    <div id="cart_checkout">
        <div class="dashboard  dashboard--s-margin">
            <div class="dashboard-container ">
                <div class="card dashboard-container__content">
                    <table class="js-cart-table  yc-table  yc-table--order-view">
                        <thead class="yc-thead">
                            <tr class="yc-tr">
                                <th scope="col" class="yc-th">{{ __('labels.item') }}</th>
                                <th scope="col" class="yc-th">{{ __('labels.description') }}</th>
                                <th scope="col" class="yc-th  yc-th--centered">{{ __('labels.quantity') }}</th>
                                <th scope="col" class="yc-th">{{ __('labels.price') }}</th>
                                <th scope="col" class="yc-th">{{ __('labels.total') }}</th>
                                <th scope="col" class="yc-th  yc-th--centered"></th>
                            </tr>
                        </thead>
                        <tbody class="yc-tbody">
                            @foreach($products as $product)
                            <tr data-id="{{ $product->id }}" class="yc-tr items">
                                <td class="yc-td  yc-td--product-image">
                                    <a href="{{ route('shop.product', ['id' => $product->id ]) }}">
                                        <img src="{{ $product->url ?? url('images/placeholder-item.png') }}"
                                            style="width:60px" class="yc-td__image  lazyload" alt="">
                                    </a>
                                </td>
                                <td class="yc-td  yc-td--product-text">
                                    <a href="{{ route('shop.product', ['id' => $product->id ]) }}" class="block-link">
                                        <div class="yc-td__product-name">{{ $product->item_name }} </div>
                                        <div class="yc-td__product-description">
                                            <span class="yc-table__clip-text">{{ __('labels.item') }} </span>Code:
                                            {{ $product->recordno }}
                                        </div>
                                    </a>
                                </td>
                                <td class="yc-td  yc-td--centered  yc-td--qty-control  cart_quantity">
                                    <div class="yc-input-combo  yc-input-combo--quantity  ie-group">
                                        <button type="button" class="decreaseQtyActive  button tooltip tooltip--top"
                                            data-tooltip="Remove 1">-</button>
                                        <input type="text" readonly="readonly" name="qty"
                                            value="{{ $product->qty ?? 1 }}" size="3"
                                            class="yc-text-input  cartqtychange" />
                                        <button type="button" class="increaseQtyActive  button tooltip tooltip--top"
                                            data-tooltip="Add 1">+</button>
                                    </div>
                                </td>
                                <td class="yc-td  yc-td--product-price">
                                    <span class="yc-td__faux-table" data-th="{{ __('labels.price') }}">
                                        <span class="yc-td__faux-td">
                                            @money($product->price, \DefConst::CURRENCY)
                                        </span>
                                    </span>
                                </td>
                                <td class="yc-td  yc-td--product-price">
                                    <span class="yc-td__faux-table" data-th="{{ __('labels.total') }}">
                                        <span class="yc-td__faux-td">
                                            @money($product->total, \DefConst::CURRENCY)
                                        </span>
                                    </span>
                                </td>
                                <td class="yc-td  yc-td--centered  yc-td--options">
                                    <span class="yc-td__faux-table" data-th="{{ __('labels.delete') }}">
                                        <span class="yc-td__faux-td">
                                            <button type="button"
                                                class="button button--circle button--tiny js-cart-item-remove tooltip  tooltip--top"
                                                data-id="{{ $product->id }}" data-tooltip="Remove">
                                                <i class="svg-icon  svg-icon--block  svg-icon--delete  svg-icon--delete--is-dark"></i>
                                            </button>
                                        </span>
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="flex-grid-group flex-grid-group--ml-align-right u-text-right">
                    <div>
                        <p class="u-text-right">{{ __('labels.subtotal') }}</p>
                        <h1>@money(session()->get('cart.subtotal'), \DefConst::CURRENCY)</h1>
                    </div>
                </div>

                <div class="flex-grid-group">
                    <div class="flex-grid-block  flex-grid-block--l-1-5"></div>
                    <div class="flex-grid-block  flex-grid-block--l-1-5"></div>
                    <div class="flex-grid-block  flex-grid-block--l-1-5">
                        <div class="flex-grid-content">
                            <a href="{{ route('shop.products') }}"
                                class="text-link">{{ __('labels.continue_shopping') }}</a>
                        </div>
                    </div>
                    <div class="flex-grid-block  flex-grid-block--l-1-5">
                        <div class="flex-grid-content">
                            <button class="button button--small  button--full button--positive updateItem">{{ __('labels.update_cart') }}</button>
                        </div>
                    </div>
                    <div class="flex-grid-block  flex-grid-block--l-1-5">
                        <div class="flex-grid-content">
                            <a href="{{ route('shop.checkout') }}"
                                class="button button--small  button--full button--dark">{{ __('labels.procced_checkout') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="flex-grid-block">
        <div class="yc-section  yc-section--medium  card-section u-margin-top-40">
            <h2 class="yc-heading  yc-heading--medium  yc-heading--center">
                <div class="u-margin-bottom-40">
                    <img src="{{ url('images/not-found.png') }}" />
                </div>
                <span> {{ __('labels.empty_cart') }} </span>
            </h2>
            <div class="u-margin-top-20">
                <div class="u-block-center  u-max-width-240">
                    <a href="{{ route('shop.products') }}"
                        class="button  button--full  button--dark">
                        {{ __('labels.continue_shopping')}}
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>


<script type="text/javascript">
    $(document).ready(function () {
        // decrease Item qty
        $(".decreaseQtyActive").click(function () {
            var quantity = $(this).next();
            if (quantity.val() > 1) {
                quantity.val(parseInt(quantity.val()) - 1);
            }
        });
        // increase Item qty
        $(".increaseQtyActive").click(function () {
            var quantity = $(this).prev();
            quantity.val(parseInt(quantity.val()) + 1);
        });

        // Delete item from Cart
        $('.js-cart-item-remove').click(function () {
            $.get("{{ route('shop.removeitemfromcart') }}", {
                id: $(this).data('id'),
            }).done(function (data) {
                location.href = data.next;
            });
        });

        // Update Cart
        $('.updateItem').click(function () {
            var items = {};
            $('.items').each(function () {
                var id = $(this).data('id');
                var qty = $('.cartqtychange', this).val();
                items[id] = qty;
            });
            $.get("{{ route('shop.updatecart') }}", {
                items
            }).done(function (data) {
                location.href = data.next;
            });
        });
    });

</script>
@endsection
