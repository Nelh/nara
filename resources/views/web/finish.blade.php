@extends('web.web-layout')

@section('content')
<div class="yc-section  yc-section--medium  card-section u-margin-top-40">
    <h2 class="yc-heading  yc-heading--large  yc-heading--center">
        <span>{{ __('labels.congratulations') }}
    </h2>

    <div class="success-checkmark">
        <div class="check-icon">
            <span class="icon-line line-tip"></span>
            <span class="icon-line line-long"></span>
            <div class="icon-circle"></div>
            <div class="icon-fix"></div>
        </div>
    </div>

    <p class="yc-paragraph  yc-paragraph--large  yc-paragraph--flush  u-text-center">
        {{ __('labels.congratutation_order_msg') }}
    </p>
    <div class="u-margin-top-20">
        <div class="u-block-center u-max-width-240">
            <a href="{{ route('user.home') }}" class="button  button--full  button--muted-bordered">
                {{ __('labels.check_orders') }}
            </a>
        </div>
    </div>
</div>

@endsection
