<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/store.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet">
    <script src="{{ asset('js/store.js') }}"></script>
    @yield('extra-content')
</head>

<body id="websitebody" class="mt-top">
    @if(!session()->has('shop.location'))
    <div class="mfp-bg mfp-ready"></div>
    <div class="mfp-wrap  mfp-close-btn-in mfp-auto-cursor mfp-ready" tabindex="-1" style="overflow: hidden auto;">
        <div class="mfp-container mfp-s-ready mfp-inline-holder">
            <div class="mfp-content">
                <div class="yc-popup">
                    <h2 class="yc-heading  yc-heading--small">{{ __('labels.select_location') }}</h2>
                    <p class="yc-paragraph"></p>
                    <span class="yc-td__faux-td">
                        <label for="collect" class="ie-group  yc-radio-label">
                            <input type="radio" id="collect" name="deliveryoption" value="Collect"
                                class="js-deliveryoption  yc-radio">
                            <span class="yc-radio__graphic-wrapper"><span class="yc-radio__graphic"></span></span>
                            <span class="yc-radio__text">
                                <span class="yc-radio__title">{{ __('labels.enabled_geolocation') }}</span>
                            </span>
                        </label>
                    </span>
                    <p>{{ __('labels.geolocation_msg') }}.
                    </p>
                    <div class="button-group">
                        <a href="{{ route('shop.index', ['location' => 'AB']) }}"
                            class="button  button--small  button--dark disabled">
                            Alberta (AB) </a>
                        <!-- <a href="{{ route('shop.index', ['location' => 'BC']) }}"
                            class="button  button--small  button--muted-bordered">
                            British columbia (BC) </a>
                        <a href="{{ route('shop.index', ['location' => 'QC']) }}"
                            class="button  button--small  button--muted-bordered">
                            Quebec (BC) </a>
                        <a href="{{ route('shop.index', ['location' => 'YT']) }}"
                            class="button  button--small  button--muted-bordered">
                            Yukon (YT) </a> -->

                        <button class="button  button--small  button--dark" disabled>
                            British columbia (BC) </button>
                        <button class="button  button--small  button--dark" disabled>
                            Quebec (BC) </button>
                        <button class="button  button--small  button--dark" disabled>
                            Yukon (YT) </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <header class="header-wrapper">
        <div class="header">
            <nav class="menu-button-wrapper" aria-hidden="true">
                <a href="#" class="menu-button  js-main-nav-open-button">
                    <i class="svg-icon  svg-icon--block  svg-icon--menu"></i>
                </a>
            </nav>
            <a href="/" class="header__logo">
                <img src="{{ asset('images/logo.png') }}" width="100">
            </a>
            <nav class="solutions-nav-wrapper">
                <ul class="solutions-nav">
                    <li class="solutions-nav__item">
                        <a href="{{ route('shop.products') }}" class="solutions-nav__link">{{ __('labels.shop_online') }}</a>
                    </li>
                    <li class="solutions-nav__item">
                        <a href="{{ route('shop.partner') }}" class="solutions-nav__link">{{ __('labels.trade_partner') }}</a>
                    </li>
                    <li class="solutions-nav__item">
                        <a href="#" class="solutions-nav__link">COVID-19 {{ __('labels.resources') }}</a>
                    </li>
                </ul>
            </nav>
            <div class="search-button-wrapper">
                <a href="#" class="search-button  js-mfp-open-popup-search" data-popup-src="js-main-search-popup"
                    data-popup-focus-id="searchstr">
                    <i class="search-button__icon  svg-icon  svg-icon--block  svg-icon--search  svg-icon--is-light"></i>
                    <span class="search-button__text">{{ __('labels.search') }}</span>
                </a>
            </div>
            <ul class="user-nav">
                <li class="user-nav__item  user-nav__item--user">
                    @guest
                    <a href="{{ route('login') }}" class="user-nav__link">
                        <i class="user-nav__link-icon  svg-icon  svg-icon--user  svg-icon--is-light"></i>
                        <span class="user-nav__link-text">{{ __('labels.login') }}</span>
                    </a>
                    @else
                    <a href="{{ route('user.home') }}" class="user-nav__link">
                        <i class="user-nav__link-icon  svg-icon  svg-icon--user  svg-icon--is-light"></i>
                        <span class="user-nav__link-text">{{ __('labels.my_account') }}</span>
                    </a>
                    @endguest
                </li>
                <li id="js-cart-wrapper" class="user-nav__item  user-nav__item--cart">
                    <a href="{{ route('shop.cart') }}" class="user-cart  user-nav__link  cart-link">
                        <i class="user-nav__link-icon  svg-icon  svg-icon--cart  svg-icon--is-light"></i>
                        <span class="user-cart__count">
                            <span class="user-cart__count-text  count">
                                {{ array_sum(session()->get('cart.items', [])) }}
                            </span>
                        </span>
                    </a>
                </li>
                <li class="user-nav__item  user-nav__item--user">
                    <i class="user-nav__link-icon  svg-icon  svg-icon--globe  svg-icon--is-light"></i>
                    <select id="changeLangue" class="custom-select">
                        <option value="en" {{ app()->getLocale() == 'en' ? 'selected' : ''}}>EN</option>
                        <option value="fr" {{ app()->getLocale() == 'fr' ? 'selected' : ''}}>FR</option>
                    </select>  
                </li>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             </select>
            </ul>
        </div>
    </header>
    <div class="main-nav-overlay  js-main-nav-overlay"></div>
    <nav role="navigation" class="main-nav  js-main-nav">
        <div class="main-nav-user">
            <div class="main-nav-user__account">
                <div class="main-nav-user__button-wrapper">
                    @guest
                    <a href="{{ route('login') }}" class="button  button--small  button--full  button--reversed">{{ __('labels.login') }}</a>
                    @else
                    <a href="{{ route('user.home') }}" class="button  button--small  button--full  button--reversed">{{ __('labels.my_account')  }}</a>
                    @endguest
                </div>
            </div>
            <a href="#" class="main-nav__close-button  js-main-nav-close-button" aria-hidden="true">
                <i class="svg-icon  svg-icon--block  svg-icon--close  svg-icon--is-light"></i>
            </a>
        </div>
        <div class="main-nav__menu-wrapper">
            <ul class="main-nav-solutions">
                <li class="main-nav-solutions__item">
                    <a href="{{ route('shop.sites.application') }}" class="main-nav-solutions__link">
                        <span class="main-nav-solutions__icon">
                            <i class="svg-icon  svg-icon--block  svg-icon--trade  svg-icon--is-dark"></i>
                        </span>
                        <span class="main-nav-solutions__text">{{ __('labels.trade_partner') }}</span>
                    </a>
                </li>
            </ul>
            @if(count(session()->get('shop.cats')))
            <ul id="js-main-menu" class="main-menu  main-menu--is-extended">
                @foreach(session()->get('shop.cats') as $key => $cat)
                @if(count($cat['categories']))
                <li class="main-menu__item">
                    <a href="{{ route('shop.products.sort.category', ['cat_id' => $key]) }}"
                        class="main-menu__link  main-menu__link--has-children">
                        <span class="main-menu__icon">
                            <i class="svg-icon  svg-icon--block  svg-icon--label  svg-icon--is-dark"></i>
                        </span>
                        <span class="main-menu__text">{{ __('labels.'.$cat['category_name']) }}</span>
                    </a>
                </li>
                @else
                <li class="main-menu__item">
                    <a href="{{ route('shop.products.sort.category', ['cat_id' => $key]) }}" class="main-menu__link  main-menu__link--surf">
                        <span class="main-menu__icon">
                            <i class="svg-icon  svg-icon--label  svg-icon--is-dark  svg-icon--block"></i>
                        </span>
                        <span class="main-menu__text">{{ __('labels.'.$cat['category_name']) }}</span>
                    </a>
                </li>
                @endif
                @endforeach
            </ul>
            @endif
        </div>
    </nav>


    <main>
        @yield('content')
    </main>



    <div class="clearer"></div>
    <div class="search-bar-wrapper  js-main-search-popup  mfp-hide">
        <form action="{{ route('shop.products.sort.search') }}" method="get" id="searchForm" class="search-bar">
            <input type="search" name="search" id="searchstr" placeholder="{{ __('labels.what_are_you_looking_for') }}" autocomplete="off"
                autocapitalize="off" autocorrect="off" cores="c,p,s,h,o" origin="Header"
                class="search-bar__input  js-search-bar__input  js-instant-search">
            <div class="search-bar__submit-wrapper">
                <button type="submit" class="search-bar__submit-button  search-submit-button">
                    <i class="svg-icon  svg-icon--block  svg-icon--search  svg-icon--is-dark"></i>
                </button>
            </div>
            <nav class="search-bar__close-wrapper">
                <a href="#" class="search-bar__close-button  js-mfp-close">
                    <i class="svg-icon  svg-icon--block  svg-icon--close"></i>
                </a>
            </nav>
        </form>
    </div>

    <footer class="site-footer-wrapper">
        <div class="site-footer">
            <div class="footer-info  group">
                <nav class="footer-nav  group" role="navigation">
                    <div class="footer-nav__list-wrapper">
                        <h3 class="yc-heading  yc-heading--footer">Shop</h3>
                        <ul class="footer-nav-list" role="directory">
                            <li class="footer-nav-list__item"><a href="{{ route('shop.products') }}">{{ __('labels.shop_online') }}</a></li>
                        </ul>
                    </div>
                    <div class="footer-nav__list-wrapper">
                        <h3 class="yc-heading  yc-heading--footer">Company</h3>
                        <ul class="footer-nav-list" role="directory">
                            <li class="footer-nav-list__item"><a
                                    href="{{ route('shop.sites.application') }}">{{ __('labels.open_store') }}</a></li>
                            <li class="footer-nav-list__item"><a
                                    href="{{ route('site.login') }}">{{ __('labels.store_portal') }}</a>
                            <li class="footer-nav-list__item"><a href="#">{{ __('labels.become_deliver') }}</a></li>
                            <li class="footer-nav-list__item"><a
                                    href="{{ route('service.login') }}">{{ __('labels.delivery_portal') }}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-nav__list-wrapper">
                        <h3 class="yc-heading  yc-heading--footer">{{ __('labels.help') }}</h3>
                        <ul class="footer-nav-list" role="directory">
                            <li class="footer-nav-list__item"><a href="#">{{ __('labels.help_center') }}</a></li>
                            <li class="footer-nav-list__item"><a href="#">{{ __('labels.contact_us') }}</a></li>
                            <li class="footer-nav-list__item"><a href="#">{{ __('labels.track_your_order') }}</a></li>
                        </ul>
                    </div>
                </nav>
                <aside class="footer-contacts  group">
                    <!--newsletter-signup-->
                    <form action="#" method="post" class="footer-signup  group  js-newsletter-form" role="form">
                        <input type="hidden" name="interest_ids[]" value="5">
                        <input type="hidden" class="js-newsletter-params" name="source" value="Website: Footer" />
                        <h3 class="yc-heading  yc-heading--footer">{{ __('labels.newsletter') }}</h3>
                        <div class="js-newsletter-form-inner  js-newsletter-email">
                            <div class="yc-input-combo  js-form-field  ie-group">
                                <div class="yc-text-input-wrapper">
                                    <input type="email" name="email" id="" class="yc-text-input  js-text-input  js-ead"
                                        placeholder="e.g. name@domain.com" required><i class="yc-text-input-icon"></i>
                                </div>
                                <button type="submit" role="button"
                                    class="button  button--dark  yc-input-combo__button">{{ __('labels.submit') }}</button>
                            </div>
                            <div class="footer-signup__sending  js-form-hint" style="display: none;">Invalid email.
                                Please try again.</div>
                            <div class="footer-signup__sending  js-newsletter-sending" style="display: none;">Please
                                wait while we send your details&hellip;</div>
                        </div>
                        <div class="footer-signup__confirmed  js-newsletter-thanks" style="display: none;">Success!
                            Thank you for subscribing.</div>
                    </form>
                </aside>
            </div>
            <div class="footer-terms-wrapper  group">
                <p class="footer-terms">&copy; {{ ucfirst(config('app.name')) }} (Pty) Ltd 
                    <span class="pipe-divider  hide-when-ms">|</span> 
                    <span class="break-when-ms">
                    <a href="#">Legal &amp; Privacy Policy</a> 
                    <span class="pipe-divider  hide-when-ms">|</span> 
                    <a href="#">Returns &amp; Repairs Policy</a></span>
                    <span class="pipe-divider  hide-when-ms">|</span> 
                    <span>Your current location is {{ session()->get('shop.location') }}</span>
                </p>
            </div>
            <div class="group">
                <div class="payment-logos-wrapper">
                    <aside class="payment-logos">
                        <a href="#" class="block-link  group">
                            <span class="payment-logo  payment-logo--visa"></span>
                            <span class="payment-logo  payment-logo--mastercard"></span>
                        </a>
                    </aside>
                </div>
            </div>
        </div>
    </footer>

    @if(count(session()->get('shop.cats')))
    <ul id="js-replace-main-menu" class="main-menu">
        @foreach(session()->get('shop.cats') as $key => $cat)
        <li class="main-menu__item  main-menu__item--has-children js-collapse-wrapper">
            <div class="main-menu__item-wrapper">
                @if(count($cat['categories']))
                <a href="{{ route('shop.products.sort.category', ['cat_id' => $key]) }}"
                    class="main-menu__link  main-menu__link--has-children">
                    <span class="main-menu__icon">
                        <i class="svg-icon  svg-icon--block  svg-icon--label  svg-icon--is-dark"></i>
                    </span>
                    <span class="main-menu__text">{{ __('labels.'.$cat['category_name']) }}</span>
                </a>
                <a href="{{ route('shop.products.sort.category', ['cat_id' => $key]) }}"
                    class="main-menu__icon  main-menu__icon--accordion  js-collapse-link">
                    <i class="svg-icon  svg-icon--block  svg-icon--plus  svg-icon--is-dark  js-collapse-icon">
                        <span class="u-hidden-from-view">- Toggle Sub-menu for {{ __('labels.'.$cat['category_name']) }}</span>
                    </i>
                </a>
                @else
                <a href="{{ route('shop.products.sort.category', ['cat_id' => $key]) }}" class="main-menu__link  main-menu__link--surf">
                    <span class="main-menu__icon">
                        <i class="svg-icon  svg-icon--label  svg-icon--is-dark  svg-icon--block"></i>
                    </span>
                    <span class="main-menu__text">{{ __('labels.'.$cat['category_name']) }}</span>
                </a>
                @endif
            </div>
            @if(count($cat['categories']))
            <div class="sub-menu-wrapper  sub-menu-wrapper--2-column  js-collapse-content" style="display: none;">
                <ul class="sub-menu  sub-menu--2-column">
                    @foreach($cat['categories'] as $subcat)
                    <li class="sub-menu__item">
                        <a href="{{ route('shop.products.sort.category', ['cat_id' => $key, 'subcat_id' => $subcat['sub_category_id']]) }}"
                            class="sub-menu__link">
                            {{ $subcat['sub_category_name'] }}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
        </li>
        @endforeach
    </ul>
    @endif

    <script src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $('#changeLangue').change(function(){
            var val = $(this).children("option:selected").val();
            location.href = "{{ route('get.language') }}/" + val;
        });
    });
    </script>
</body>

</html>
