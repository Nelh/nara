@extends('web.web-layout')

@section('content')
<div class="yc-section  yc-section--standard">
    <h1 class="u-text-center">{{ __('labels.checkout') }}</h1>

    <form class="flex-grid-group  flex-grid-group--align-left" action="{{ route('shop.checkout.sale') }}" method="post">
        @csrf
        <div class="flex-grid-block  flex-grid-block--ml-4-6">
            @component('components.errors')
            @slot('section')
            {{ 'details' }}
            @endslot
            @endcomponent
            <fieldset class="checkout-fieldset">
                <h2 class="yc-heading  yc-heading--small  yc-heading--section">{{ __('labels.delivery_detail') }} </h2>

                <div class="checkout-fieldset__content">
                    <div class="js-deliveryaddress-form-">
                        <div id="js-delivery-details" class="group">
                            <div class="feedback  feedback--error  ie-group  js-delivery-incomplete-feedback"
                                style="display: none;">
                                <div class="feedback__icon">
                                    <i class="svg-icon  svg-icon--inline  svg-icon--error"></i>
                                </div>
                            </div>
                            <div class="yc-form-field  js-form-field  js-checkout-field">
                                <label for="receivername"
                                    class="yc-form-label  yc-form-label--large">{{ __('labels.receiver_name') }}</label>
                                <div class="yc-text-input-wrapper">
                                    <input type="text" id="receivername" name="receivername"
                                        value="{{ auth()->user()->name ?? old('receivername') }}"
                                        class="yc-text-input  yc-text-input--user" placeholder="e.g. Amstrong Nouni"
                                        required><i class="yc-text-input-icon"></i>
                                </div>
                            </div>
                            <div class="yc-form-field  js-form-field  js-checkout-field">
                                <label for="receivertel"
                                    class="yc-form-label">{{ __('labels.receiver_cellphone_number') }}</label>
                                <div class="yc-text-input-wrapper">
                                    <input type="tel" id="receivertel" name="receivertel"
                                        value="{{ auth()->user()->cell ?? old('receivertel') }}"
                                        class="yc-text-input  js-text-input" required><i class="yc-text-input-icon"></i>
                                </div>
                                <div class="yc-form-hint-wrapper">
                                    <span
                                        class="yc-form-hint  js-form-hint">{{ __('labels.our_agent_will_contact_your_with_this_cellphone') }}</span>
                                </div>
                            </div>
                            <!--/text-input-->
                            <div class="senddelivery  js-g-deliveryaddress">
                                <!--text-area-->
                                <div class="yc-form-field  js-form-field  js-checkout-field">
                                    <label for="deliveryaddress"
                                        class="yc-form-label  yc-form-label--large">{{ __('labels.receiver_address') }}</label>
                                    <textarea id="deliveryaddress" rows="2" cols="30" name="deliveryaddress"
                                        class="yc-text-input  yc-text-area"
                                        required>{{ old('deliveryaddress') }}</textarea>
                                </div>
                                <!--/text-area-->
                                <!--text-input-->
                                <div class="yc-form-field  js-form-field  js-checkout-field">
                                    <label for="deliverysuburb"
                                        class="yc-form-label">{{ __('labels.receiver_suburb') }}:</label>
                                    <div class="yc-text-input-wrapper">
                                        <input type="text" id="deliverysuburb" name="deliverysuburb"
                                            value="{{ old('deliverysuburb') }}" class="yc-text-input  js-text-input"
                                            required>
                                    </div>
                                </div>
                                <!--/text-input-->
                                <!--text-input-->
                                <div class="yc-form-field  js-form-field  js-checkout-field">
                                    <label for="deliverycity"
                                        class="yc-form-label">{{ __('labels.receiver_city') }}</label>
                                    <div class="yc-text-input-wrapper">
                                        <input type="text" id="deliverycity" name="deliverycity"
                                            value="{{ old('deliverycity') }}" class="yc-text-input  js-text-input"
                                            required>
                                    </div>
                                </div>
                                <!--/text-input-->
                                <!--radio-->
                                <div class="js-checkout-field">
                                    <div class="yc-form-label  yc-form-label--set">
                                        {{ __('labels.address_type') }}
                                    </div>
                                    <div class="yc-form-field  js-form-field">
                                        <label for="addresstype_0"
                                            class="ie-group  yc-radio-label  yc-form-field  js-addresstype">
                                            <input type="radio" id="addresstype_0" name="addresstype"
                                                value="Residential" class="yc-radio">
                                            <span class="yc-radio__graphic-wrapper">
                                                <span class="yc-radio__graphic"></span>
                                            </span>
                                            <span class="yc-radio__text">
                                                <span
                                                    class="yc-radio__title">{{ __('labels.residential_address') }}</span>
                                            </span>
                                        </label><label for="addresstype_1"
                                            class="ie-group  yc-radio-label  yc-form-field  js-addresstype">
                                            <input type="radio" id="addresstype_1" name="addresstype" value="Business"
                                                class="yc-radio">
                                            <span class="yc-radio__graphic-wrapper">
                                                <span class="yc-radio__graphic"></span>
                                            </span>
                                            <span class="yc-radio__text">
                                                <span class="yc-radio__title">{{ __('labels.business_address') }}</span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/radio-->
                    <div class="js-deliveryaddress-collect"></div>
                    <!--text-area-->
                    <div class="yc-form-field  js-form-field">
                        <label for="comments" class="yc-form-label">{{ __('labels.delivery_instruction') }}</label>
                        <textarea id="comments" rows="4" cols="30" name="comments"
                            class="yc-text-input  yc-text-area  js-text-input"></textarea>
                        <div class="yc-form-hint-wrapper">
                            <span class="yc-form-hint  js-form-hint">
                                <span class="js-character-countdown"><b>Characters:</b> 65 remaining.</span><br>
                                If you have any special instructions for our delivery team, please specify them
                                here.<br>
                                We'll deliver your order during business hours - we'll email you a link to track
                                it.</span>
                        </div>
                    </div>
                    <!--/text-area-->
                </div>
            </fieldset>
            <!--/delivery-details-->
            <!--/order-details-->
        </div>
        <div class="flex-grid-block  flex-grid-block--ml-2-6">
            <aside class="mini-cart">
                <h3 class="mini-cart__title">{{ __('labels.your_order') }}</h3>


                <ul class="mini-cart__list">
                @foreach($products as $product)
                    <li class="mini-cart__item">
                        <div class="group">
                            <div class="mini-cart__item-thumb">
                                <img src="{{ $product->url ?? url('images/placeholder-item.png') }}" class="yc-simple-image  yc-simple-image--full">
                            </div>
                            <div class="mini-cart__item-text">
                                <span class="mini-cart__item-title">
                                    {{ $product->item_name }}
                                </span>
                                <span class="mini-cart__item-price">{{ $product->qty }} x @money($product->price, \DefConst::CURRENCY)</span>
                            </div>
                        </div>
                    </li>
                @endforeach
                </ul>

                <div class="mini-cart__footer">
                    <div class="mini-cart__total">Total: @money(session()->get('cart.subtotal'), \DefConst::CURRENCY)
                    </div>
                </div>

                <div class="feedback  feedback--mini  feedback--neutral  ie-group">
                    <div class="feedback__message">
                        <img src="{{ url('images/momo-money.png') }}" width="40">
                        Pay Using MTN MOBILE MONEY
                    </div>
                </div>
                <div class="yc-form-field  js-form-field  js-checkout-field">
                    <div class="yc-text-input-wrapper">
                        <input type="tel" id="cellphone_payment" name="cellphone_payment"
                            placeholder="{{ __('labels.enter_your_momo_number') }}"
                            value="{{ old('cellphone_payment') }}" class="yc-text-input  js-text-input" required><i
                            class="yc-text-input-icon"></i>
                    </div>
                </div>

                <div class="order-cost-summary">
                    <small class="order-cost__security"><i class="svg-icon  svg-icon--inline  svg-icon--lock  svg-icon--6-5"></i>100% Secure.
                        Guaranteed.</small>
                </div>
                <button id="btn-paynow" type="submit" class="button  button--large button--full button--primary">{{ __('labels.pay_now') }}</button>
            </aside>
        </div>
    </form>
</div>

@endsection
