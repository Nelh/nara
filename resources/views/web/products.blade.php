@extends('web.web-layout')

@section('content')
<div class="yc-page-content  yc-page-content--standard">
    <div class="yc-section  yc-section--standard">

        <header class="search-results-header">
            <h1 class="search-results-title">Browse {{ $browsingby ?? "All Products" }}</h1>

            <div class="search-results-filter">
                <select class="js-selecter-links" name="selecter_links">
                    <option value>Order By</option>
                    <option value="{{ route('shop.products.sort.latest', ['sort' => 'asc']) }}">Latest</option>
                    <option value="{{ route('shop.products.sort.price', ['sort' => 'asc']) }}">Price (Lowest)</option>
                    <option value="{{ route('shop.products.sort.price', ['sort' => 'desc']) }}">Price (Highest)</option>
                    <option value="{{ route('shop.products.sort.ratings', ['sort' => 'asc']) }}">Rating (Lowest)
                    </option>
                    <option value="{{ route('shop.products.sort.ratings', ['sort' => 'desc']) }}">Rating (Highest)
                    </option>
                </select>
            </div>

        </header>
        <section class="flex-grid-group">
            <div class="flex-grid-block  flex-grid-block--ml-3-4  flex-grid-block--ml-order-02">
                <div class="flex-grid-content">
                    <div class="flex-grid-group  flex-grid-group--align-left">
                        @if(count($products))
                        @foreach($products as $product)
                        <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-3">
                            <article class="card  product-card  flex-grid-content">
                                <div class="card__hero">
                                    <a href="{{ route('shop.product', ['id' => $product->id]) }}" class="u-block-link">
                                        <picture>
                                            <source data-srcset="{{ $product->url }}">
                                            <img src="{{ $product->url ?? url('images/placeholder-item.png') }}"
                                                data-src="{{ $product->url }}" class="card__image  lazyload"
                                                alt="{{ $product->item_name }}">
                                        </picture>
                                        <!-- <div class="card__colour-key  card__colour-key--2  group">
                                                <div class="colour-key__description"><span
                                                        class="u-hidden-from-view">Available in </span>2<span
                                                        class="colour-key__qualifier" data-abbr="clr"><span
                                                            class="colour-key__clip"> colours</span></span></div>
                                                <div class="colour-key__wrapper">
                                                    <div class='colour-key__item ' style='background-color: #000303;'>
                                                    </div>
                                                    <div class='colour-key__item ' style='background-color: #f6f6f6;'>
                                                    </div>
                                                </div>
                                            </div> -->
                                    </a>
                                </div>
                                <a href="{{ route('shop.product', ['id' => $product->id]) }}" class="u-block-link">
                                    <div class="card__content-wrapper">
                                        <div class="card__content">
                                            <header class="card__header">
                                                <div class="product-card__meta">
                                                    <div class="star-rating  star-rating--small  star-rating--{{ $product->ratings ?? 0}}"
                                                        title="{{ $product->ratings / 10 }} out of 5 stars">
                                                        <span class="hidden-from-view">{{ $product->ratings / 10 }} out
                                                            of 5 stars</span>
                                                    </div>
                                                </div>
                                                <h1 class="product-card__title">
                                                    <span
                                                        class="product-card__brand">{{ $product->site_name ?? ''}}</span>
                                                    <span class="product-card__name">{{ $product->item_name }}</span>
                                                </h1>
                                            </header>
                                            <div class="card__description">
                                                <ul class="card-price-list  group">
                                                    <li class="card-price-list__item">
                                                        @money($product->price, \DefConst::CURRENCY)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </article>
                        </div>
                        @endforeach
                        @else
                        <div class="flex-grid-block">
                            <div class="yc-section  yc-section--medium  card-section u-margin-top-40">
                                <h2 class="yc-heading  yc-heading--medium  yc-heading--center">
                                    <div class="u-margin-bottom-40">
                                        <img src="{{ url('images/not-found.png') }}" />
                                    </div>
                                    <span>Oops!! {{ __('labels.nothing_found_from') }}</span>
                                </h2>
                                <p class="u-text-center"> {{ __('labels.not_found_message') }}</p>
                                <div class="u-margin-top-20">
                                    <div class="u-block-center  u-max-width-240">
                                        <a href="{{ route('shop.products') }}"
                                            class="button  button--full  button--dark">
                                            {{ __('labels.continue_shopping')}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-3">
                                <article class="card  product-card  flex-grid-content">
                                    <div class="card__hero">
                                        <a href="nutribullet-appliances.htm?id=14131&name=Nutribullet-600W-High-Speed-Blender"
                                            class="u-block-link">
                                            <picture>
                                                <img src="#" class="card__image  lazyload">
                                            </picture>
                                            <div class="card__colour-key  group">
                                                <div class="colour-key__description"><span
                                                        class="u-hidden-from-view">Available in </span>5<span
                                                        class="colour-key__qualifier" data-abbr="clr"><span
                                                            class="colour-key__clip"> colours</span></span></div>
                                                <div class="colour-key__wrapper">
                                                    <div class='colour-key__item ' style='background-color: #999999;'>
                                                    </div>
                                                    <div class='colour-key__item ' style='background-color: #000303;'>
                                                    </div>
                                                    <div class='colour-key__item ' style='background-color: #e3e0d1;'>
                                                    </div>
                                                    <div class='colour-key__item ' style='background-color: #d10e1b;'>
                                                    </div>
                                                    <div class='colour-key__item ' style='background-color: #0033cc;'>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <a href="nutribullet-appliances.htm?id=14131&name=Nutribullet-600W-High-Speed-Blender"
                                        class="u-block-link">
                                        <div class="card__content-wrapper">
                                            <div class="card__content">
                                                <header class="card__header">
                                                    <div class="product-card__meta">
                                                        <div class="star-rating  star-rating--small  star-rating--50"
                                                            title="5 out of 5 stars from 332 reviews">
                                                            <span class="hidden-from-view">5 out of 5 stars from 332
                                                                reviews</span>
                                                        </div>
                                                    </div>
                                                    <h1 class="product-card__title">
                                                        <span class="product-card__brand">Nutribullet</span>
                                                        <span class="product-card__name">600W High Speed Blender</span>
                                                    </h1>
                                                </header>
                                                <div class="card__description">
                                                    <ul class="card-price-list  group">
                                                        <li class="card-price-list__item  card-price-list__item--now">
                                                            <span class="u-hidden-from-view">Now </span> R1,799
                                                        </li>
                                                        <li class="card-price-list__item  card-price-list__item--was">
                                                            <span
                                                                class="clip-text--t  unclip-text--s  clip-text--ml  unclip-text--l">Was
                                                            </span>R1,999</li>
                                                    </ul>
                                                    <div class="card__price-sticker-wrapper">
                                                        <div class="card-sticker  card-sticker--price">
                                                            Save R200 </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </article>
                            </div> -->
                    </div>
                    <div class="flex-grid-group">
                        <div class="flex-grid-block  flex-grid-block--t-full">
                            <div class="flex-grid-content">
                                <nav class="pagination-wrapper">
                                    {{ $products->withQueryString()->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <aside class="flex-grid-block  flex-grid-block--ml-1-4  flex-grid-block--ml-right">
                <div class="flex-grid-content">
                    <div class="sidebar">
                        <!-- <nav class="search-filter-wrapper  js-collapse-wrapper">
                                <h3 class="search-filter-heading">
                                    <a href="#" class="search-filter-heading__link  js-collapse-link">
                                        <span class="search-filter-heading__text">Made In</span>
                                        <span class="search-filter-heading__icon-wrapper">
                                            <i
                                                class="search-filter-heading__icon  svg-icon  svg-icon--block  svg-icon--minus  js-collapse-icon">
                                                <span class="u-hidden-from-view">- Collapse this Section</span>
                                            </i>
                                        </span>
                                    </a>
                                </h3>
                                <ul class="search-filter  js-collapse-content">
                                    <li class="search-filter__item">
                                        <a href="?fs_madein=South+Africa&sort=Popularity"
                                            class="search-filter__item-link  search-filter__item--feature  js-search-filter-feature-link">
                                            <label for="feature-checkbox-1-1"
                                                class="search-filter__checkbox-label  yc-checkbox-label  yc-checkbox-label--inline">
                                                <input type="checkbox" name="radio" id="feature-checkbox-1-1"
                                                    value="radio"
                                                    class="search-filter__checkbox  yc-checkbox  js-search-filter-feature-checkbox">
                                                <span class="yc-checkbox__graphic"></span>
                                            </label>
                                            <span class="search-filter__item-text">South Africa</span>
                                            <span class="search-filter__item-count-wrapper">
                                                <span class="search-filter__item-count">1179</span>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </nav> -->
                        <nav class="search-filter-wrapper  js-collapse-wrapper">
                            <h3 class="search-filter-heading">
                                <a href="#" class="search-filter-heading__link  js-collapse-link">
                                    <span class="search-filter-heading__text">Filter by Price</span>
                                    <span class="search-filter-heading__icon-wrapper">
                                        <i
                                            class="search-filter-heading__icon  svg-icon  svg-icon--block  svg-icon--minus  js-collapse-icon">
                                            <span class="u-hidden-from-view">- Collapse this Section</span>
                                        </i>
                                    </span>
                                </a>
                            </h3>
                            <ul class="search-filter  js-collapse-content">
                                <li class="search-filter__item">
                                    <a href="{{ route('shop.products.sort.range', ['minprice' => 500]) }}"
                                        class="search-filter__item-link">
                                        <span class="search-filter__item-text">Less than @money(500, \DefConst::CURRENCY)</span>
                                    </a>
                                </li>
                                <li class="search-filter__item">
                                    <a href="{{ route('shop.products.sort.range', ['min' => 500, 'max' => 1000]) }}"
                                        class="search-filter__item-link">
                                        <span class="search-filter__item-text">@money(500, \DefConst::CURRENCY) to @money(1000, \DefConst::CURRENCY)</span>
                                    </a>
                                </li>
                                <li class="search-filter__item">
                                    <a href="{{ route('shop.products.sort.range', ['min' => 1000, 'max' => 5000]) }}"
                                        class="search-filter__item-link">
                                        <span class="search-filter__item-text">@money(1000, \DefConst::CURRENCY) to @money(5000, \DefConst::CURRENCY)</span>
                                    </a>
                                </li>
                                <li class="search-filter__item">
                                    <a href="{{ route('shop.products.sort.range', ['min' => 5000, 'max' => 10000]) }}"
                                        class="search-filter__item-link">
                                        <span class="search-filter__item-text">@money(5000, \DefConst::CURRENCY) to @money(10000, \DefConst::CURRENCY)</span>
                                    </a>
                                </li>
                                <li class="search-filter__item">
                                    <a href="{{ route('shop.products.sort.range', ['maxprice' => 10000]) }}"
                                        class="search-filter__item-link">
                                        <span class="search-filter__item-text">Greater than @money(10000, \DefConst::CURRENCY)</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <nav class="search-filter-wrapper  js-collapse-wrapper">
                            <h3 class="search-filter-heading">
                                <a href="#" class="search-filter-heading__link  js-collapse-link">
                                    <span class="search-filter-heading__text">Filter by Brands</span>
                                    <span class="search-filter-heading__icon-wrapper">
                                        <i
                                            class="search-filter-heading__icon  svg-icon  svg-icon--block  svg-icon--minus  js-collapse-icon">
                                            <span class="u-hidden-from-view">- Collapse this Section</span>
                                        </i>
                                    </span>
                                </a>
                            </h3>
                            <ul class="search-filter  js-collapse-content">
                                @foreach($brands as $brand)
                                <a href="{{ route('shop.products.sort.brands', ['brand_id' => $brand->id]) }}"
                                    class="search-filter__item-link  search-filter__item--feature  js-search-filter-feature-link">
                                    <label for="feature-checkbox-1-1"
                                        class="search-filter__checkbox-label  yc-checkbox-label  yc-checkbox-label--inline">
                                        <input type="checkbox" name="radio" id="feature-checkbox-1-1" value="radio"
                                            @if(request()->brand_id == $brand->id) checked @endif
                                        class="search-filter__checkbox yc-checkbox js-search-filter-feature-checkbox">
                                        <span class="yc-checkbox__graphic"></span>
                                    </label>
                                    <span class="search-filter__item-text">{{ $brand->name }}</span>
                                </a>
                                </li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
            </aside>
        </section>
    </div>
</div>

<script type="text/javascript">
    $('.js-selecter-links').change(function () {
        window.location = $(this).val();
    });

</script>
@endsection
