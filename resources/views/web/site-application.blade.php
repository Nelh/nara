@extends('web.web-layout')

@section('content')
<div class="header-banner  header-banner--no-image  u-bg-terracotta">
    <div class="header-banner__text-wrapper">
        <div class="header-banner__text  u-text-white">
            <h1 class="header-banner__title">{{ __('labels.open_store') }}</h1>
        </div>
    </div>
</div>
<div class="yc-section  yc-section--medium-underline">
    <div class="yc-intro-text">
        <p class="yc-paragraph">{{ __('labels.become_partner_msg') }}
            <a href="#js-application-form" class="text-link  text-link--muted  js-smoothscroll">{{ __('labels.apply_online') }}</a>.</p>
    </div>
    <div class="u-block-center u-margin-top-20 u-max-width-480">
        <div class="flex-grid-group">
            <div class="flex-grid-block  flex-grid-block--m-1-2  flex-grid-block--m-order-02">
                <div class="flex-grid-content">
                    <a href="{{ route('site.login') }}" class="button  button--full  button--negative  js-smoothscroll">
                        {{ __('labels.store_portal') }} </a>
                </div>
            </div>
            <div class="flex-grid-block  flex-grid-block--m-1-2">
                <div class="flex-grid-content">
                    <a href="#js-application-form" class="button  button--full  button--muted-bordered  js-smoothscroll">
                    {{ __('labels.apply_online') }} </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="js-application-form" class="yc-section  yc-section--large">
    @component('components.errors')
    @slot('section')
    {{ 'details' }}
    @endslot
    @endcomponent
    <h2 class="yc-heading  yc-heading--medium  yc-heading--center">{{ __('labels.apply_to_become_a_partner') }}</h2>
    <form action="{{ route('admin.sites.application.save') }}" id="fid3" method="post"
        class="yc-form  yc-form--center  yc-comp-form  u-max-width-480">
        @csrf
        <div class="yc-form-field js-form-field">
            <label for="company_name" class="yc-form-label  js-form-label">{{ __('labels.company_name') }}</label>
            <div class="yc-text-input-wrapper">
                <input name="company_name" id="company_name" type="text" class="yc-text-input  js-text-input"
                    value="{{ old('company_name') }}" required />
                <i class="yc-text-input-icon"></i>
            </div>
        </div>
        <div class="yc-form-field js-form-field">
            <label for="email" class="yc-form-label  js-form-label">{{ __('labels.email') }}</label>
            <div class="yc-text-input-wrapper">
                <input type="email" name="email" id="email" class="yc-text-input  js-text-input"
                    value="{{ old('email') }}" required />
                <i class="yc-text-input-icon"></i>
            </div>
        </div>
        <div class="yc-form-field">
            <label for="industry" class="yc-form-label  js-form-label">{{ __('labels.industry') }}</label>
            <div class="yc-select">
                <select name="classification_id" id="classification" value="{{ old('classification_id') }}"
                    required="required">
                    <option value="">- {{ __('labels.select') }} -</option>
                    @foreach($classifications as $classification)
                    <option value='{{ $classification->id }}'>{{ $classification->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="yc-form-field">
            <label for="addr" class="yc-form-label js-form-label">{{ __('labels.address') }}</label>
            <input type="addr" name="addr" id="addr" class="yc-text-input js-text-input" value="{{ old('addr') }}"
                required />
        </div>
        <div class="yc-form-field">
            <label for="zip" class="yc-form-label js-form-label">{{ __('labels.zip') }}</label>
            <input type="zip" name="zip" id="zip" class="yc-text-input js-text-input" value="{{ old('zip') }}"
                required />
        </div>
        <div class="yc-form-field">
            <label for="city" class="yc-form-label js-form-label">{{ __('labels.city') }}</label>
            <div class="yc-select">
                <select id="city" name="city" value="{{ old('city') }}" required>
                    <option value="">- {{ __('labels.select') }} -</option>
                    @foreach($cities as $city)
                    <option value='{{ $city->city_code }}'>{{ $city->city_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="yc-form-field">
            <label for="country" class="yc-form-label js-form-label">{{ __('labels.country') }}</label>
            <input type="country" name="country" id="country" class="yc-text-input js-text-input"
                value="{{ old('country') }}" required />
        </div>
        <div class="yc-form-field js-form-field">
            <label for="phone" class="yc-form-label js-form-label">{{ __('labels.phone') }}</label>
            <div class="yc-text-input-wrapper">
                <input type="phone" name="phone" id="phone" class="yc-text-input  js-text-input"
                    value="{{ old('phone') }}" required />
                <i class="yc-text-input-icon"></i>
            </div>
        </div>
        <div class="yc-form-field js-form-field">
            <label for="website" class="yc-form-label js-form-label">{{ __('labels.website') }}</label>
            <input type="text" name="website" id="website" class="yc-text-input  js-text-input"
                value="{{ old('website') }}" />
        </div>
        <div class="yc-form-field">
            <label for="description" class="yc-form-label  js-form-label">{{ __('labels.description') }}</label>
            <textarea name="description" id="description" rows="5"
                class="yc-text-input  yc-text-area  js-text-input">{{ old('description') }}</textarea>
        </div>
        <label for="autologger" class="yc-checkbox-label  yc-form-field  ie-group">
            <input type="checkbox" id="autologger" value="true" class="yc-checkbox" required="">
            <span class="yc-checkbox__graphic-wrapper">
                <span class="yc-checkbox__graphic"></span>
            </span>
            <span class="yc-checkbox__text">
                <span class="yc-checkbox__title">{{ __('labels.accept_terms_conditions') }}</span>
            </span>
        </label>
        <div class="yc-form-control">
            <button type="submit" class='button button--dark'>{{  __('labels.submit_application') }}</button>
        </div>
    </form>
</div>
@endsection
