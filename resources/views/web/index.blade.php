@extends('web.web-layout')

@section('content')
<div class="yc-page-content  yc-page-content--standard">
    @if($sitex)
    <div class="yc-section  yc-section--standard">
        <a href="{{ route('shop.store', ['slug' => $sitex->slug_name ]) }}" class="hero-banner">
            <div class="hero-banner__image-wrapper  u-bg-casablanca"
                style="background-color: {{ $sitex->banner_color }}">
                <picture>
                    <img src="{{ $sitex->site_banner }}"
                        class="hero-banner__image  lazyload">
                </picture>
            </div>
            <div class="hero-banner__text-wrapper  u-bg-casablanca"
                style="background-color: {{ $sitex->banner_color }}">
                <div class="hero-banner__text  hero-banner__text--light">
                    <div class="hero-banner__logo-wrapper">
                        <h5>{{ $sitex->site_name }}</h5>
                    </div>
                    <h2 class="hero-banner__title">{{ $sitex->banner ?? '' }}</h2>
                    <p class="hero-banner__description">{{ $sitex->note ?? '' }}</p>
                    <div class="hero-banner__cta">
                        <button class='button  button--full  button--muted-bordered'>
                            {{ __('labels.shop_now') }} </button>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @endif

    <!-- <div class="category-slider  yc-slick-carousel-wrapper">
        <nav class="yc-slick-carousel  js-category-carousel">
            <article>
                <div class="flex-grid-block  flex-grid-block--flush">
                    <div class="flex-grid-content">
                        <a href="/products-made-in-south-africa.htm?ref=category-carousel"
                            class="category-slider-item  u-block-link">
                            <div
                                class="category-slider-item__content  category-slider-item__content--no-image  card  card--no-border">
                                <header class="category-slider__title-wrapper  u-bg-greenery">
                                    <h3 class="category-slider__title  u-text-wrap-normal">Made in SA</h3>
                                </header>
                            </div>
                        </a>
                    </div>
                </div>
            </article>
            <article>
                <div class="flex-grid-block  flex-grid-block--flush">
                    <div class="flex-grid-content">
                        <a href="/deals.htm?ref=category-carousel" class="category-slider-item  u-block-link">
                            <div
                                class="category-slider-item__content  category-slider-item__content--no-image  card  card--no-border">
                                <header class="category-slider__title-wrapper  u-bg-red">
                                    <h3 class="category-slider__title"><span
                                            class="u-text-uppercase">Deals</span></h3>
                                </header>
                            </div>
                        </a>
                    </div>
                </div>
            </article>
            <article>
                <div class="flex-grid-block  flex-grid-block--flush">
                    <div class="flex-grid-content">
                        <a href="/gift-vouchers.htm?ref=category-carousel"
                            class="category-slider-item  u-block-link">
                            <div
                                class="category-slider-item__content  category-slider-item__content--no-image  card  card--no-border">
                                <header class="category-slider__title-wrapper  u-bg-greenery">
                                    <h3 class="category-slider__title">Vouchers</h3>
                                </header>
                            </div>
                        </a>
                    </div>
                </div>
            </article>
            <article>
                <div class="flex-grid-block  flex-grid-block--flush">
                    <div class="flex-grid-content">
                        <a href="/registry.htm?ref=category-carousel"
                            class="category-slider-item  u-block-link">
                            <div
                                class="category-slider-item__content  category-slider-item__content--no-image  card  card--no-border">
                                <header class="category-slider__title-wrapper  u-bg-iris">
                                    <h3 class="category-slider__title">Registry</h3>
                                </header>
                            </div>
                        </a>
                    </div>
                </div>
            </article>
            <article>
                <div class="flex-grid-block  flex-grid-block--flush">
                    <div class="flex-grid-content">
                        <a href="/gifting.htm?ref=category-carousel" class="category-slider-item  u-block-link">
                            <div
                                class="category-slider-item__content  category-slider-item__content--no-image  card  card--no-border">
                                <header class="category-slider__title-wrapper  u-bg-malibu">
                                    <h3 class="category-slider__title">Gifting</h3>
                                </header>
                            </div>
                        </a>
                    </div>
                </div>
            </article>
            <article>
                <div class="flex-grid-block  flex-grid-block--flush">
                    <div class="flex-grid-content">
                        <a href="/subscriptions.htm?ref=category-carousel"
                            class="category-slider-item  u-block-link">
                            <div
                                class="category-slider-item__content  category-slider-item__content--no-image  card  card--no-border">
                                <header class="category-slider__title-wrapper  u-bg-peacock">
                                    <h3 class="category-slider__title">Subscriptions</h3>
                                </header>
                            </div>
                        </a>
                    </div>
                </div>
            </article>
            <div></div>
        </nav>
    </div> -->

    <section class="yc-section  yc-section--standard">
        <div class="flex-grid-group">
            <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-4">
                <a href="{{ route('shop.products.sort.price') }}" class='button  button--full  button--muted-bordered'>
                    Shop by Price </a>
            </div>
            <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-4">
                <a href="#" class='button  button--full  button--muted-bordered'> Shop by Brand </a>
            </div>
            <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-4">
                <a href="{{ route('shop.products.sort.latest') }}"
                    class='button  button--full  button--muted-bordered'>Shop Latest </a>
            </div>
            <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-4">
                <a href="{{ route('shop.products.sort.ratings', ['sort' => 'desc']) }}"
                    class='button  button--full  button--muted-bordered'>Most Rated </a>
            </div>
        </div>
        <div class="u-block-center  u-max-width-540  u-margin-top-20">
            <p class="yc-paragraph  yc-page-content--flush  u-text-center"><span class="clip-text  unclip-text--m">
                    <i class='svg-icon  svg-icon--check  svg-icon--is-dark  svg-icon--inline  svg-icon--in-copy'></i></span>{{ __('labels.free_delivery_for_items_over') }} @money(10000, \DefConst::CURRENCY). <a
                    href="#" class="text-link  text-link--muted  js-mfp-open-url">Find out
                    more.</a></p>
        </div>
    </section>

    <div class="yc-section  yc-section--standard">
        <ul class="product-icons  product-icons--center  group">
            <li class="product-icons__item">
                <img src="{{ url('images/logo-1.png') }}" class="yc-simple-image  yc-simple-image--full  yc-simple-image--center">
            </li>
            <li class="product-icons__item">
                <img src="{{ url('images/logo-2.png') }}" class="yc-simple-image  yc-simple-image--full  yc-simple-image--center">
            </li>
            <li class="product-icons__item">
                <img src="{{ url('images/logo-3.png') }}" class="yc-simple-image  yc-simple-image--full  yc-simple-image--center">
            </li>
            <li class="product-icons__item">
                <img src="{{ url('images/logo-4.png') }}" class="yc-simple-image  yc-simple-image--full  yc-simple-image--center">
            </li>
            <li class="product-icons__item">
                <img src="{{ url('images/logo-5.png') }}" class="yc-simple-image  yc-simple-image--full  yc-simple-image--center">
            </li>
            <li class="product-icons__item">
                <img src="{{ url('images/logo-6.png') }}" class="yc-simple-image  yc-simple-image--full  yc-simple-image--center">
            </li>
        </ul>
    </div>

    
    <div class="yc-section  yc-section--standard">
        <div class="flex-grid-group">
        @if(count($cardsx))
            @foreach($cardsx as $card)
            <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-4">
                <a href="#" class="quad-banner  js-quad-banner  flex-grid-content">
                    <img src="{{ $card->url }}"
                        class="yc-simple-image  yc-simple-image--full  lazyload" alt="">
                    <div class="quad-banner__text  quad-banner__text--top">
                        <span class="quad-banner__lead-in">{{ $card->subheading }}</span>
                        <span class="quad-banner__title">{{ $card->heading }}</span>
                    </div>
                </a>
            </div>
            @endforeach
        @else
            <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-4">
                <a href="#" class="quad-banner  js-quad-banner  flex-grid-content">
                    <img src="{{ url('images/shop01.jpg') }}"
                        class="yc-simple-image  yc-simple-image--full  lazyload" alt="">
                    <div class="quad-banner__text  quad-banner__text--top">
                        <span class="quad-banner__lead-in">{{ __('Fresh') }}</span>
                        <span class="quad-banner__title">{{ __('Epices') }}</span>
                    </div>
                </a>
            </div>
            <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-4">
                <a href="#" class="quad-banner  js-quad-banner  flex-grid-content">
                    <img src="{{ url('images/shop27.jpg') }}"
                        class="yc-simple-image  yc-simple-image--full  lazyload" alt="">
                    <div class="quad-banner__text  quad-banner__text--top">
                        <span class="quad-banner__lead-in">{{ __('Premium') }}</span>
                        <span class="quad-banner__title">{{ __('Microwave') }}</span>
                    </div>
                </a>
            </div>
            <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-4">
                <a href="#" class="quad-banner  js-quad-banner  flex-grid-content">
                    <img src="{{ url('images/shop46.jpg') }}"
                        class="yc-simple-image  yc-simple-image--full  lazyload" alt="">
                    <div class="quad-banner__text  quad-banner__text--top">
                        <span class="quad-banner__lead-in">{{ __('Premium') }}</span>
                        <span class="quad-banner__title">{{ __('Ustensil de Cuisine') }}</span>
                    </div>
                </a>
            </div>
            <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-4">
                <a href="#" class="quad-banner  js-quad-banner  flex-grid-content">
                    <img src="{{ url('images/shop166.jpg') }}"
                        class="yc-simple-image  yc-simple-image--full  lazyload" alt="">
                    <div class="quad-banner__text  quad-banner__text--top">
                        <span class="quad-banner__lead-in">{{ __('Premium') }}</span>
                        <span class="quad-banner__title">{{ __('Sucrerie et Cremerie') }}</span>
                    </div>
                </a>
            </div>
        @endif
        </div>
    </div>

    @if(count($latests))
    <div class="yc-section  yc-section--large  card-section">
        <h2 class='yc-heading  yc-heading--medium'> {{ __('labels.latest_products') }} </h2>
        <div class="flex-grid-group  flex-grid-group--small  flex-grid-group--align-left">
            @foreach($latests as $latest)
            <div class="flex-grid-block  flex-grid-block--small  flex-grid-block--t-1-2  flex-grid-block--ml-1-3  flex-grid-block--l-1-6">
                <article class="card  product-card  product-card--small  flex-grid-content">
                    <div class="card__hero">
                        <a href="{{ route('shop.product', ['id' => $latest->id]) }}"
                            class="u-block-link">
                            <picture>
                                <source data-srcset="{{ $latest->url }}">
                                <img src="{{ $latest->url ?? url('images/placeholder-item.png') }}" data-src="{{ $latest->url }}" class="card__image  lazyload" alt="{{ $latest->item_name }}">
                            </picture>
                        </a>
                    </div>
                    <a href="{{ route('shop.product', ['id' => $latest->id]) }}"
                        class="u-block-link">
                        <div class="card__content-wrapper">
                            <div class="card__content">
                                <header class="card__header">
                                    <div class="product-card__meta">
                                        <div class="star-rating  star-rating--small  star-rating--{{ $latest->ratings ?? 0}}"
                                            title="5 out of 5 stars from 197 reviews">
                                            <span class="hidden-from-view">{{ $latest->ratings / 10 }} out of 5 stars</span>
                                        </div>
                                    </div>
                                    <h1 class="product-card__title">
                                        <span class="product-card__brand">{{ $latest->site_name ?? ''}}</span>
                                        <span class="product-card__name">{{ $latest->item_name }}</span>
                                    </h1>
                                </header>
                                <div class="card__description">
                                    <ul class="card-price-list  group">
                                        <li class="card-price-list__item">
                                            @money($latest->price, \DefConst::CURRENCY)
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </a>
                </article>
            </div>
            @endforeach
        </div>
        <div class="u-block-center  u-max-width-240  u-margin-top-20">
            <a href="{{ route('shop.products.sort.latest') }}" class='button  button--full  button--muted-bordered'>
                {{ __('labels.all_products') }}
                <i class='svg-icon  svg-icon--arrow-right  svg-icon--is-dark  svg-icon--medium  svg-icon--inline'></i>
            </a>
        </div>
    </div>
    @endif

    <!-- <div class="yc-section  yc-section--standard">
        <h2 class='yc-heading  yc-heading--medium'>{{ __('labels.our_partner') }}</h2>
        <div class="flex-grid-group  flex-grid-group--small  flex-grid-group--align-left">
            <div class="flex-grid-block  flex-grid-block--small  flex-grid-block--t-1-2  flex-grid-block--ml-1-3  flex-grid-block--l-1-6">
                <article class="card  product-card  product-card--small  flex-grid-content">
                    <div class="card__hero">
                        <a href="#" class="u-block-link">
                            <picture>
                                <img src="#" class="card__image  lazyload">
                            </picture>
                            <div class="card__new-sticker-wrapper">
                                <div class="card-sticker card-sticker--new">New</div>
                            </div>
                        </a>
                    </div>
                    <a href="#" class="u-block-link">
                        <div class="card__content-wrapper">
                            <div class="card__content">
                                <header class="card__header">
                                    <div class="product-card__meta">
                                        <div class="star-rating  star-rating--small  star-rating--0"
                                            title="This product has not been reviewed yet.">
                                            <span class="hidden-from-view">This product has not been reviewed
                                                yet.</span>
                                        </div>
                                    </div>
                                    <h1 class="product-card__title">
                                        <span class="product-card__brand">Linen House</span>
                                        <span class="product-card__name">White Deluxe Waffle Duvet Cover
                                            Set</span>
                                    </h1>
                                </header>
                                <div class="card__description">
                                    <ul class="card-price-list  group">
                                        <li class="card-price-list__item">
                                            From R1,449
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </a>
                </article>
            </div>
            <div
                class="flex-grid-block  flex-grid-block--small  flex-grid-block--t-1-2  flex-grid-block--ml-1-3  flex-grid-block--l-1-6">
                <a href="linen-house.htm?sort=Added" class="card  more-card  flex-grid-content">
                    <div class="more-card__content">
                        <div class="more-card__text">
                            <b>More</b>
                            <div class="more-card__arrow">
                                <i
                                    class="svg-icon  svg-icon--inline  svg-icon--medium-large  svg-icon--arrow-right  svg-icon--is-dark"></i>
                            </div>
                        </div>
                    </div>
                </a> </div>
        </div>
    </div> -->
</div>
@endsection
