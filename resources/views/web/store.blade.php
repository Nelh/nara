@extends('web.web-layout')

@section('content')
<div class="header-banner  max-height-225" style="background-color: {{ $sitex->banner_color ?? '' }}">
    <div class="header-banner__text-wrapper">
        <div class="header-banner__text  u-text-white">
            <h1 class="header-banner__title">
                {{ $sitex->site_name ?? '' }}
                <span class="header-banner__subtitle"><span class="u-hidden-from-view"> — </span>{{ $sitex->banner ?? '' }}</span> </h1>
        </div>
    </div>
    <div class="header-banner__image-wrapper header-banner-display-none">
        <picture>
            <img src="{{ $sitex->site_banner }}" class="@if($sitex->site_banner) header-banner__image  @endif  lazyloaded">
        </picture>
    </div>
</div>
<div class="u-block-center  u-max-width-240">
    <img src="{{ $sitex->site_logo }}" class="yc-simple-image  yc-simple-image--full  yc-simple-image--center  u-margin-bottom-10">
</div>
<div class="yc-page-content  yc-page-content--standard">
    <div class="yc-section  yc-section--standard">
        <section class="flex-grid-group">
            <div class="flex-grid-block  flex-grid-block--ml-3-4  flex-grid-block--ml-order-02">
                <div class="flex-grid-content">
                    <div class="flex-grid-group  flex-grid-group--align-left">
                        @if(count($products))
                        @foreach($products as $product)
                        <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-3">
                            <article class="card  product-card  flex-grid-content">
                                <div class="card__hero">
                                    <a href="{{ route('shop.product', ['id' => $product->id]) }}" class="u-block-link">
                                        <picture>
                                            <source data-srcset="{{ $product->url }}">
                                            <img src="{{ $product->url ?? url('images/placeholder-item.png') }}"
                                                data-src="{{ $product->url }}" class="card__image  lazyload"
                                                alt="{{ $product->item_name }}">
                                        </picture>
                                    </a>
                                </div>
                                <a href="{{ route('shop.product', ['id' => $product->id]) }}" class="u-block-link">
                                    <div class="card__content-wrapper">
                                        <div class="card__content">
                                            <header class="card__header">
                                                <div class="product-card__meta">
                                                    <div class="star-rating  star-rating--small  star-rating--{{ $product->ratings ?? 0}}"
                                                        title="{{ $product->ratings / 10 }} out of 5 stars">
                                                        <span class="hidden-from-view">{{ $product->ratings / 10 }} out
                                                            of 5 stars</span>
                                                    </div>
                                                </div>
                                                <h1 class="product-card__title">
                                                    <span
                                                        class="product-card__brand">{{ $product->site_name ?? ''}}</span>
                                                    <span class="product-card__name">{{ $product->item_name }}</span>
                                                </h1>
                                            </header>
                                            <div class="card__description">
                                                <ul class="card-price-list  group">
                                                    <li class="card-price-list__item">
                                                        @money($product->price, \DefConst::CURRENCY)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </article>
                        </div>
                        @endforeach
                        @else
                        <div class="flex-grid-block">
                            <div class="yc-section  yc-section--medium  card-section u-margin-top-40">
                                <h2 class="yc-heading  yc-heading--medium  yc-heading--center">
                                    <div class="u-margin-bottom-40">
                                        <img src="{{ url('images/icons8-shopping-bag-80.png') }}" />
                                    </div>
                                    <span>Oops!! {{ __('labels.nothing_found_from') }}</span>
                                </h2>
                                <p class="u-text-center"> {{ __('labels.not_product') }}</p>
                                <div class="u-margin-top-20">
                                    <div class="u-block-center  u-max-width-240">
                                        <a href="{{ route('shop.products') }}"
                                            class="button  button--full  button--dark">
                                            {{ __('labels.continue_shopping')}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="flex-grid-group">
                        <div class="flex-grid-block  flex-grid-block--t-full">
                            <div class="flex-grid-content">
                                <nav class="pagination-wrapper">
                                    {{ $products->withQueryString()->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script type="text/javascript">
    $('.js-selecter-links').change(function () {
        window.location = $(this).val();
    });

</script>
@endsection
