@extends('web.web-layout')
@section('content')
<!-- <div class="header-banner  header-banner--no-image  u-bg-terracotta">
    <div class="header-banner__text-wrapper">
        <div class="header-banner__text  u-text-white">
            <h1 class="header-banner__title">Trade Partners</h1>
        </div>
    </div>
</div>
<div class="yc-section  yc-section--medium-underline">
    <div class="yc-intro-text">
        <p class="yc-paragraph">If you're in the architecture and interiors, corporate relations or hospitality
            industries, join our Trade Partner Program and get excellent deals and discounts on bulk orders from our
            curated range of kitchen and homeware products. <a href="#js-application-form"
                class="text-link  text-link--muted  js-smoothscroll">Apply online</a>.</p>
        <div class="u-block-center  u-max-width-480">
            <a href='#js-application-form' class='button  button--dark  js-smoothscroll'>
                Apply Online </a>
        </div>
    </div>
</div>
<div class="yc-section  yc-section--standard">
    <h2 class="yc-heading  yc-heading--medium  yc-heading--center"><b>Four reasons</b> to choose Nara</h2>
    <div class="flex-grid-group">
        <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-3  flex-grid-block--l-1-4">
            <div class="flex-grid-content">
                <article class="js-match-article">
                    <div class="u-block-center  u-max-width-104">
                        <img src="#"
                            class="yc-simple-image  yc-simple-image--full  yc-simple-image--center  u-margin-bottom-10">
                    </div>
                    <h3 class="yc-heading  yc-heading--tiny  yc-heading--center  js-match-heading">Free delivery
                    </h3>
                    <p
                        class="yc-paragraph  yc-paragraph--small  yc-paragraph--flush  u-text-center  js-match-paragraph">
                        We offer free, guaranteed delivery by courier throughout South Africa.</p>
                </article>
            </div>
        </div>
        <div class="flex-grid-block  flex-grid-block--t-1-2  flex-grid-block--ml-1-3  flex-grid-block--l-1-4">
            <div class="flex-grid-content">
                <article class="js-match-article">
                    <div class="u-block-center  u-max-width-104">
                        <img src="#"
                            alt="Nara Customer Service Team is here to help with your order"
                            class="yc-simple-image  yc-simple-image--full  yc-simple-image--center  u-margin-bottom-10">
                    </div>
                    <h3 class="yc-heading  yc-heading--tiny  yc-heading--center  js-match-heading">Personalised
                        service</h3>
                    <p
                        class="yc-paragraph  yc-paragraph--small  yc-paragraph--flush  u-text-center  js-match-paragraph">
                        Our team of personal trade assistants are here to help you.</p>
                </article>
            </div>
        </div>
    </div>
</div>
<div class="yc-section  yc-section--medium-underline">
    <aside class="pinned-review">
        <p class="pinned-review__quote">Nara combines excellent customer service with a vast range of top
            quality products that keeps us coming back.</p>
        <small class="pinned-review__author">- ARRCC Interior Design</small>
    </aside>
</div> -->

<div id="js-application-form" class="yc-section  yc-section--large">
    <h2 class="yc-heading  yc-heading--medium  yc-heading--center"><b>Apply</b> to become a member</h2>
    <form action="#" id="fid3" method="post" class="yc-form  yc-form--center  yc-comp-form  u-max-width-480">
        @csrf    
        <div class="yc-form-field  js-form-field">
            <div class="yc-form-label-group  group">
                <label for="firstname" class="yc-form-label  js-form-label">First name:</label>
                <label for="lastname" class="yc-form-label  js-form-label">Last name:</label>
            </div>
            <div class="yc-text-input-group  yc-text-input-group--joined  group  js-input-joined">
                <div class="yc-text-input-wrapper">
                    <input type="text" name="firstname" id="firstname"
                        class="yc-text-input  js-text-input  js-input-joined-first" placeholder="e.g. Jane"
                        required="required" />
                </div>
                <div class="yc-text-input-wrapper">
                    <input type="text" name="lastname" id="lastname"
                        class="yc-text-input  yc-text-input--user  js-text-input  js-input-joined-last"
                        placeholder="e.g. Smith" required="required" /><i class="yc-text-input-icon"></i>
                </div>
            </div>
        </div>
        <div class="for-the-bears">
            <label for="surname">Surname:</label>
            <input type="text" name="sname" id="sname">
        </div>
        <div class="yc-form-field js-form-field">
            <label for="company" class="yc-form-label  js-form-label">Company name:</label>
            <div class="yc-text-input-wrapper">
                <input name="company" id="company" type="text" class="yc-text-input  js-text-input"
                    placeholder="e.g. Smith Co. Interiors" required /><i class="yc-text-input-icon"></i>
            </div>
        </div>
        <div class="yc-form-field">
            <label for="trade" class="yc-form-label  js-form-label">Industry:</label>
            <div class="yc-select">
                <select name="trade" id="trade" required="required">
                    <option value="">- Select -</option>
                    <option value=2735>Architect, interior designer or kitchen designer</option>
                    <option value=2739>B&B, lodge, hotel or guesthouse</option>
                    <option value=2736>Chef, caterer, restaurant or deli</option>
                    <option value=2737>Corporate Rewards programmes</option>
                    <option value=2740>Other Trade</option>
                    <option value=2738>Promotions, gifting, marketing or advertising</option>

                </select>
            </div>
        </div>
        <div class="yc-form-field js-form-field">
            <label for="email" class="yc-form-label  js-form-label">Email address:</label>
            <div class="yc-text-input-wrapper">
                <input type="email" name="email" id="email" class="yc-text-input  js-text-input"
                    placeholder="e.g. jane.smith@domain.com" required /><i class="yc-text-input-icon"></i>
            </div>
        </div>
        <div class="yc-form-field js-form-field">
            <label for="tel" class="yc-form-label  js-form-label">Phone number:</label>
            <div class="yc-text-input-wrapper">
                <input type="tel" name="tel" id="tel" class="yc-text-input  js-text-input"
                    placeholder="e.g. 012 000 0000" required /><i class="yc-text-input-icon"></i>
            </div>
        </div>
        <div class="yc-form-field">
            <label for="physical" class="yc-form-label  js-form-label">Physical address:</label>
            <textarea name="physical" id="physical" class="yc-text-input  yc-text-area  js-text-input"></textarea>
        </div>
        <div class="yc-form-control">
            <input type="submit" value="Submit Application" type='submit' class='button  g-recaptcha  button--dark'
                data-sitekey=6Ld3r3UUAAAAAPPFBFIv1dnAhkqZ5Des2crX0s-p data-callback='onSubmit' />
        </div>
    </form>
    <aside class="yc-comp-terms  u-max-width-480">
        <h3 class="yc-heading  yc-heading--tiny  yc-heading--uppercase"><b>Terms</b> and conditions</h3>
        <ol>
            <li>Delivery charges apply.</li>
            <li>Offers and discounts applicable to Nara Trade Partners only.</li>
            <li>Trade discount cannot be used in conjunction with another Nara discount or promotional
                vouchers.</li>
            <li>Nara reserves the right to change the terms and conditions of the Trade Partner Program.</li>
            <li>Automatic subscription to the Nara newsletter.</li>
        </ol>
    </aside>
</div>

@endsection