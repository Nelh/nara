@extends('auth.auth-layout')

@section('content')
<div class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
        <div class="card login-card">
            <div class="row no-gutters">
                <div class="col-md-5">
                    <img src="{{ url('images/deliver-man.jpg') }}" alt="login" class="login-card-img">
                </div>
                <div class="col-md-7">
                    <div class="card-body">
                        <div class="brand-wrapper">
                            <img src="{{ url('images/logo.png') }}" alt="logo" class="logo">
                        </div>
                        <p class="login-card-description">{{ __('auth.sign_into_your') }} <br/> <strong>{{ __('auth.delivers_portal') }}</strong></p>
                        <form method="POST" action="{{ route('service.login.submit') }}">
                            @csrf
                            <div class="form-group">
                                <label for="email" class="sr-only">{{ __('auth.email_address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('auth.enter_email') }}" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password" class="sr-only">{{ __('auth.password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('auth.enter_password') }}">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        {{ __('auth.remember_me') }}
                                    </label>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-block login-btn mb-4">
                                {{ __('auth.login') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="forgot-password-link" href="{{ route('service.password.request') }}">
                                    {{ __('auth.forgot_your_password?') }}
                                </a>
                            @endif
                            <p class="login-card-footer-text">{{ __('auth.dont_have_an_account') }} <a href="#" class="text-reset">{{ __('auth.subscribe_here') }}</a></p>
                            <nav class="login-card-footer-nav">
                                <a href="#">{{ __('auth.terms_of_use') }}</a>
                                <a href="#">{{ __('auth.privacy_policy') }}</a>
                            </nav>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection