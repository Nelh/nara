@extends('auth.auth-layout')

@section('content')
<div class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <div class="card login-card">
                    <div class="card-body">
                        <div class="brand-wrapper">
                            <img src="{{ url('images/logo.png') }}" alt="logo" class="logo">
                        </div>
                        <p class="login-card-description">{{ __('auth.create_an_account') }}</p>
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group">
                                <label for="name" class="sr-only">{{ __('auth.name') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="{{ __('auth.enter_name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email" class="sr-only">{{ __('auth.email_address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="{{ __('auth.enter_email') }}" required autocomplete="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password" class="sr-only">{{ __('auth.password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="{{ __('auth.enter_password') }}" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="sr-only">{{ __('auth.confirm_password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="{{ __('auth.retype_password') }}" autocomplete="new-password">
                            </div>


                            <button type="submit" class="btn btn-block login-btn mb-4">
                                {{ __('auth.register') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="forgot-password-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                            <p class="login-card-footer-text">{{ __('auth.already_have_an_account') }} <a href="{{ route('login') }}" class="text-reset">{{ __('auth.login_here') }}</a></p>
                            <nav class="login-card-footer-nav">
                                <a href="#">{{ __('auth.terms_of_use') }}</a>
                                <a href="#">{{ __('auth.privacy_policy') }}</a>
                            </nav>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
