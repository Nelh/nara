
@if (count($errors))
	<div class="alert alert-danger" role="alert">
		{{ $errors->first() }}
	</div>
@endif
@if (session()->has('success'))
	<div class="alert alert-success" role="alert">
		{{ session()->get("success") }}
	</div>
@endif

@if (isset($section) && session()->has("{$section}_error"))
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		{{ session()->get("{$section}_error") }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif
@if (isset($section) && session()->has("{$section}_success"))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		{{ session()->get("{$section}_success") }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif

@if (isset($section) && session()->has("{$section}_web_error"))
<div class="feedback  feedback--error  ie-group">
	<div class="feedback__icon"> 
		<i class="svg-icon  svg-icon--inline  svg-icon--info"></i>
	</div>
	<div class="feedback__message">
		<h4 class="feedback-title">{{ session()->get("{$section}_web_error") }}</h4>
	</div>
</div>
@endif

@if (isset($section) && session()->has("{$section}_title_success"))
<div class="feedback  feedback--success  ie-group">
	<div class="feedback__icon"> 
		<i class="svg-icon  svg-icon--inline  svg-icon--info"></i>
	</div>
	<div class="feedback__message">
		<h4 class="feedback-title">{{ session()->get("{$section}_title_success") }}</h4>
		<p class="feedback-description">
			{{ session()->get("{$section}_description_success") }}
		</p>
	</div>
</div>
@endif