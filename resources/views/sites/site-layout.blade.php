<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="wrapper">
		<!-- Sidebar  -->
		<nav id="sidebar">
			<div id="sidebar-menu-button">
				<span class="d-block d-md-none"><i class="fal fa-times"></i></span>
				<span class="d-none d-md-block"><i class="fal fa-bars"></i></span>
            </div>
			<ul class="list-unstyled components">
				<li class="sidebar-item">
					<a href="{{ route('site.dashboard') }}" title="{{ __('labels.dashboard') }}">
						<i class="fal fa-tachometer-alt"></i> <span>{{ __('labels.dashboard') }}</span>
					</a>
				</li>
				<li class="sidebar-item">
					<a href="{{ route('sites.products') }}" title="{{ __('labels.products') }}">
						<i class="fal fa-box"></i> <span>{{ __('labels.products') }}</span>
					</a>
				</li>
				<li class="sidebar-item">
					<a href="{{ route('sites.settings') }}" title="{{ __('labels.settings') }}">
						<i class="fal fa-cog"></i> <span>{{ __('labels.settings') }}</span>
					</a>
				</li>
			</ul>
		</nav>

		<!-- Page Content  -->
		<div id="content">
			<div class="overlay"></div>
			<nav class="navbar navbar-expand bg-white fe-nav-size my-0">
				<div class="container-fluid">
					<button type="button" id="sidebarCollapse" class="btn btn-outline-secondary mr-3">
						<i class="fal fa-align-justify"></i>
					</button>
					<ul class="navbar-nav mr-auto d-none d-sm-block">
						<li class="nav-item">
							<a class="nav-link title-nav px-0" href="#">{{ isset($page_title) ? $page_title : "" }}</a>
						</li>
					</ul>
					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown">
							<a class="nav-link bg-lightblue icons-nav p-0 mr-2" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                {{ substr(Auth::user()->name, 0, 1) }}
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item bg-white"> 
									<i class="far fa-user mr-2"></i><span>{{ Auth::user()->name }}</span>
								</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="{{ route('site.logout') }}">
									<i class="fal fa-sign-out-alt mr-2"></i><span>{{ __('labels.logout') }}</span>
								</a>
							</div>
						</li>
					</ul>
				</div>
			</nav>
			<div id="app">
				@yield('content')
			</div>
		</div>
	</div>

    <script type="text/javascript">
		$(document).ready(function () {
            $('#sidebarCollapse, #sidebar-menu-button, .overlay').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                if ($('#sidebar, #content').hasClass('active')) {
                    $(this).addClass('active');
                    $('.overlay').addClass('active');
                    $('.collapse.in').toggleClass('in');
					$('a[aria-expanded=true]').attr('aria-expanded', 'false');
					$('.sidebar-item a').tooltip('disable');
                } else {
                    $(this).removeClass('active');
                    $('.overlay').removeClass('active');
					$('.sidebar-item a').tooltip('enable');
                }
			});
			// bs4 tooltips 
			$('.sidebar-item a').tooltip({
				placement: "right",
				boundary: 'viewport'
			});
        });
    </script>
</body>
</html>
