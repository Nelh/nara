@extends('sites.site-layout')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <div class="card-body">
                            <div class="container">
                            @if($sitex)
                                @if($sitex->status == \DefConst::PENDING)
                                <div class="alert alert-warning" role="alert">
                                    We are reviewing your banner, once approved it will randomly showed on our home page
                                </div>
                                @elseif($sitex->status == \DefConst::APPROVED)
                                <div class="alert alert-success" role="alert">
                                    Great, Your banner has been approved, it will start appeared on the site home page, if you edit the banner
                                    it will go to our reviewing process again.
                                </div>
                                @elseif($sitex->status == \DefConst::REJECTED)
                                <div class="alert alert-danger" role="alert">
                                    You're banner has been rejected, plese follow our design mockup for a better outcome.
                                </div>
                                @endif
                            </div>
                            <div class="yc-section  yc-section--standard">
                                <div class="hero-banner">
                                    <div class="hero-banner__image-wrapper  u-bg-casablanca" style="background-color: {{ $sitex->banner_color }}">
                                        <picture>
                                            <img src="{{ $sitex->site_banner ?? asset('images/placeholder-site.png') }}" class="hero-banner__image  lazyload" alt="">
                                        </picture>
                                    </div>
                                    <div class="hero-banner__text-wrapper  u-bg-casablanca" style="background-color: {{ $sitex->banner_color }}">
                                        <div class="hero-banner__text  hero-banner__text--light">
                                            <div class="hero-banner__logo-wrapper">
                                                <h5>{{ Auth::user()->name }}</h5>
                                            </div>
                                            <h2 class="hero-banner__title">{{ $sitex->banner ?? '' }}</h2>
                                            <p class="hero-banner__description">{{ $sitex->note ?? '' }}</p>
                                            <div class="hero-banner__cta">
                                                <button class='button  button--full  button--muted-bordered'>
                                                    Shop Now </button>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-8 offset-lg-2 col-12">
                        <div class="card-body">
                            @component('components.errors')
                                @slot('section')
                                {{ 'details' }}
                                @endslot
                            @endcomponent
                            <form method="POST" action="{{ route('sites.banner.save') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <input type="hidden" name="banner_id" value="{{ $sitex->site_id ?? null }}">
                                    <div class="col-lg-12 form-group">
                                        <label for="banner_name" class="mb-1">{{ __('labels.banner_name') }}</label>
                                        <input type="text" class="form-control" id="banner_name" name="banner_name" value="{{ $sitex->banner ?? old('banner_name') ?? '' }}" required>
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <label for="note" class="mb-1">{{ __('labels.note') }}</label>
                                        <textarea class="form-control" id="note" name="note">{{ $sitex->note ?? old('note') ?? '' }}</textarea>
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <label for="banner_color" class="mb-1">{{ __('labels.banner_color') }}</label>
                                        <input type="color" class="form-control" id="banner_color" name="banner_color" value="{{ $sitex->banner_color ?? old('banner_color') ?? '#edaf55' }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="mb-1">{{ __('labels.upload_item_image') }}</label>
                                    </div>
                                    <div class="col-lg-12 form-group input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-file-image"></i></span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="{{ \DefConst::IMGTYPE_SITE_BANNER }}">
                                            <label class="custom-file-label">{{ __('labels.choose_file') }}</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-lg-12 form-group mb-3">
                                        <p>{{ __('labels.images_has-to_be_645_x_361_ratio') }}</p>
                                        <img class="img-thumbnail" src="{{ $sitex->site_banner ?? asset('images/placeholder-site.png') }}">
                                    </div> -->
                                </div>
                                <div class="row">
                                    <div class="col-12 form-group">
                                        <button type="submit" class="btn btn-dark btn-submit btn-lg">{{ __('buttons.save') }}</button>
                                        @if($sitex && $sitex->status != \DefConst::PENDING && $sitex != null)
                                        <button type="button" id="submitapproval" class="btn btn-success btn-submit btn-lg">{{ __('buttons.submit_for_approval') }}</button>
                                        @endif
                                    </div>
                                </div>
                            </form>

                            @if($sitex)
                            <form id="formapproval" method="POST" action="{{ route('sites.banner.approval') }}">
                                @csrf
                                <input type="hidden" name="banner_id" value="{{ $sitex->site_id ?? null }}">
                            </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#submitapproval').click(function(){
            $('#formapproval').submit();
        });
    });
</script>
@endsection
