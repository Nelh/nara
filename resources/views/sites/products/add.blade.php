@extends('sites.site-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
            <div class="card-header">{{ __('labels.create_product') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('sites.products.save') }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="name" class="mb-1">{{ __('labels.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="price" class="mb-1">{{ __('labels.price') }}</label>
                                <input type="text" class="form-control" id="price" price="price" name="price" value="{{ old('price') }}" required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="description" class="mb-1">{{ __('labels.description') }}</label>
                                <textarea rows="5" class="form-control" id="description" name="description" required>{{ old('description')}}</textarea>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="size" class="mb-1">{{ __('labels.size') }}</label>
                                <input type="text" class="form-control" id="size" size="size" name="size" value="{{ old('size') }}">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="list_item_brand" class="mb-1">{{ __('labels.list_item_brands') }}</label>
                                <select class="custom-select" id="list_item_brand" name="list_item_brand_id">
                                    <option value="">{{ __('labels.select_list_item_brands') }}</option>
                                    @foreach($list_item_brands as $list_item_brand)
                                        <option value='{{ $list_item_brand->id }}'>{{ $list_item_brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="list_item_category" class="mb-1">{{ __('labels.list_item_category') }}</label>
                                <select class="custom-select" id="list_item_category" name="list_item_category_id">
                                    <option value="">{{ __('labels.select_list_item_category') }}</option>
                                    @foreach($list_item_categories as $list_item_category)
                                        <option value='{{ $list_item_category->id }}'>{{ $list_item_category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="subcategory" class="mb-1">{{ __('labels.subcategory') }}</label>
                                <select class="custom-select" id="subcategory" name="sub_category_id">
                                    <option value="">{{ __('labels.select_subcategory') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-group">
                                <button type="submit" class="btn btn-primary btn-submit btn-lg">{{ __('buttons.save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#list_item_category').change(function(){
            $('#subcategory').val('');
            var cat_id = $('#list_item_category').val();
            $.get("{{ route('helper.getsubcategories') }}", {cat_id}).done(function(response){
                $('#subcategory').html("");
                $.each(response, function(idx, res){
                    $("#subcategory").append(
                        "<option value="+res.id+">"+res.name+"</option>" 
                    );
                });
            });
        });
    });
</script>
@endsection