@extends('sites.site-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
		@slot('section')
            {{ 'details' }}
        @endslot
	@endcomponent
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('sites.products') }}" class="btn btn-outline-dark btn-circle my-2 mr-4"><i class="fal fa-arrow-left fa-2x"></i></a>
                    {{ __('labels.edit_product') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('sites.products.update') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{ $product->id }}">
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="customSwitch1" name="active"
                                    @if ($product->active == \DefConst::ACTIVE) checked @endif>
                                    <label class="custom-control-label" for="customSwitch1">{{ __('labels.activate_product') }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <label for="recordno" class="mb-1">{{ __('labels.recordno') }}</label>
                                        <input type="text" class="form-control" id="recordno" value="{{ $product->recordno ?? '' }}" readonly>
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <label for="name" class="mb-1">{{ __('labels.name') }}</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ?? $product->item_name ?? '' }}" required>
                                    </div>

                                    <div class="col-lg-12 form-group">
                                        <label for="price" class="mb-1">{{ __('labels.price') }}</label>
                                        <input type="text" class="form-control" id="price" price="price" name="price" value="{{ old('price') ?? $product->price ?? '' }}" required>
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <label for="size" class="mb-1">{{ __('labels.size') }}</label>
                                        <input type="text" class="form-control" id="size" size="size" name="size" value="{{ old('size') ?? $product->size ?? '' }}">
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <label for="deduction" class="mb-1">{{ __('labels.deduction') }}</label>
                                        <input type="text" class="form-control" id="deduction" name="deduction" value="{{ old('deduction') ?? $product->deduction ?? '' }}">
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <label for="description" class="mb-1">{{ __('labels.description') }}</label>
                                        <textarea rows="5" class="form-control" id="description" name="description" required>{{ old('description') ?? $product->description ?? ''}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="mb-1">{{ __('labels.upload_item_image') }}</label>
                                    </div>
                                    <div class="col-lg-12 form-group input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-file-image"></i></span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="{{ \DefConst::IMGTYPE_ITEM }}">
                                            <label class="custom-file-label">{{ __('labels.choose_file') }}</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 form-group mb-3">
                                        <img class="img-thumbnail" src="{{ $product->url ?? asset('images/placeholder-item.png') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="group_item" class="mb-1">{{ __('labels.group_item') }}</label>
                                <select class="custom-select" id="group_item" name="group_item_id">
                                    <option value="">{{ __('labels.select_group_item') }}</option>
                                    @foreach($group_items as $group_item)
                                        <option value='{{ $group_item->id }}' {{ $product->item_groups_id == $group_item->id ? 'selected' : '' }}>{{ $group_item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="list_item_brand" class="mb-1">{{ __('labels.list_item_brands') }}</label>
                                <select class="custom-select" id="list_item_brand" name="list_item_brand_id">
                                    <option value="">{{ __('labels.select_list_item_brands') }}</option>
                                    @foreach($list_item_brands as $list_item_brand)
                                        <option value='{{ $list_item_brand->id }}' {{ $product->list_item_brand_id == $list_item_brand->id ? 'selected' : '' }}>{{ $list_item_brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="list_item_category" class="mb-1">{{ __('labels.list_item_category') }}</label>
                                <select class="custom-select" id="list_item_category" name="list_item_category_id">
                                    <option value="">{{ __('labels.select_list_item_category') }}</option>
                                    @foreach($list_item_categories as $list_item_category)
                                        <option value='{{ $list_item_category->id }}' {{ $product->list_item_category_id == $list_item_category->id ? 'selected' : '' }}>{{ $list_item_category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="subcategory" class="mb-1">{{ __('labels.subcategory') }}</label>
                                <select class="custom-select" id="subcategory" name="sub_category_id">
                                    <option value="">{{ __('labels.select_subcategory') }}</option>
                                    @foreach($subcategories as $subcategory)
                                        <option value='{{ $subcategory->id }}' {{ $product->sub_category_id == $subcategory->id ? 'selected' : '' }}>{{ $subcategory->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="specification" class="mb-1">{{ __('labels.specification') }}</label>
                                <textarea rows="5" class="form-control" id="specification" name="specification">{{ old('specification') ?? $product->specification ?? ''}}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-group">
                                <button type="button" id="delete-product" class="btn btn-danger btn-lg float-right">{{ __('buttons.delete') }}</button>
                                <button type="submit" class="btn btn-primary btn-submit btn-lg">{{ __('buttons.save') }}</button>
                            </div>
                        </div>
                    </form>
                    <form id="formdeleted" method="POST" action="{{ route('sites.products.delete') }}">
                        @csrf
                        <input type="hidden" name="id" value="{{ $product->id }}">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#delete-product').click(function(){
            var confirmDelete = confirm("{{ __('labels.confirm_item_delete') }}");
            if (confirmDelete == true) {
                $('#formdeleted').submit();
            }
        });

        $('#list_item_category').change(function(){
            $('#subcategory').val('');
            var cat_id = $('#list_item_category').val();
            $.get("{{ route('helper.getsubcategories') }}", {cat_id}).done(function(response){
                $('#subcategory').html("");
                $.each(response, function(idx, res){
                    $("#subcategory").append(
                        "<option value="+res.id+">"+res.name+"</option>" 
                    );
                });
            });
        });

    })
</script>
@endsection