@extends('sites.site-layout')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h2>{{ Auth::user()->name }}</h2>
                    <small></small>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <span>{{ Auth::user()->email }}</span>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-header">Product Setup</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('sites.products') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">  
                                        <i class="fal fa-box-full fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        {{ __('labels.products') }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-header">Site Config</div>
                <div class="card-body">
                    <div class="row">
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('sites.banner') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-cog fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        {{ __('labels.banner') }}
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class = "col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                            <a href="{{ route('sites.settings') }}">
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-cog fa-2x text-secondary"></i>
                                    </div>
                                    <div class="col-9 py-2 text-secondary">
                                        {{ __('labels.settings') }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
