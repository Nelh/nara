@extends('sites.site-layout')

@section('content')
<div class="container-fluid">
    @component('components.errors')
        @slot('section')
        {{ 'details' }}
        @endslot
    @endcomponent
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card  border-0 shadow-sm mb-4">
                <div class="card-header bg-white">
                    <div class="float-right">
                        @if($site->active == \DefConst::ACTIVE)
                            <i class="fal fa-check-circle text-success"></i> {{ __('labels.active') }}
                        @else
                            <i class="fal fa-ban text-danger"></i> {{ __('labels.notactive') }}
                        @endif
                    </div>
                    <h5>{{ __('labels.change_site_details') }}</h5>
                </div>
                <div class="card-body">
                    <div class="row mb-3 p-2 mx-0 bg-light">
                        <div class="col-lg-4">
                            {{ __('labels.name') }}: {{ $site->site_name }}
                        </div>
                        <div class="col-lg-4">
                            {{ __('labels.city') }}: {{ $site->city }}
                        </div>
                        <div class="col-lg-4">
                            {{ __('labels.classification') }}: {{ $site->classification_name }}
                        </div>
                    </div>
                    <form method="POST" action="{{ route('site.settings.save.details') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="name" class="mb-1">{{ __('labels.name') }}*</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ?? $site->site_name ?? '' }}" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="email" class="mb-1">{{ __('labels.email') }}*</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') ?? $site->email ?? '' }}">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="slug_name" class="mb-1">{{ __('labels.slug_name') }}</label>
                                <input type="text" class="form-control" id="slug_name" name="slug_name" value="{{ old('slug_name') ?? $site->slug_name ?? '' }}" required readonly>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="addr1" class="mb-1">{{ __('labels.address1') }}</label>
                                <input type="text" class="form-control" id="addr1" name="addr1" value="{{ old('addr1') ?? $site->addr1 ?? '' }}">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="addr2" class="mb-1">{{ __('labels.address2') }}</label>
                                <input type="text" class="form-control" id="addr2" name="addr2" value="{{ old('addr2') ?? $site->addr2 ?? '' }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="phone1" class="mb-1">{{ __('labels.phone1') }}</label>
                                <input type="text" class="form-control" id="phone1" name="phone1" value="{{ old('phone1') ?? $site->phone1 ?? '' }}">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="phone2" class="mb-1">{{ __('labels.phone2') }}</label>
                                <input type="text" class="form-control" id="phone2" name="phone2" value="{{ old('phone2') ?? $site->phone2 ?? '' }}">
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="description" class="mb-1">{{ __('labels.description') }}</label>
                                <textarea rows="5" class="form-control" id="description" name="description">{{ old('description') ?? $site->description ?? '' }}</textarea>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label for="website" class="mb-1">{{ __('labels.website') }}</label>
                                <input type="text" class="form-control" id="website" name="website" value="{{ old('website') ?? $site->website ?? '' }}">
                            </div>
                            <div class="col-lg-2 form-group">
                                <label for="zip" class="mb-1">{{ __('labels.zip') }}</label>
                                <input type="text" class="form-control" id="zip" name="zip" value="{{ old('zip') ?? $site->zip ?? '' }}">
                            </div>
                        </div>
                        
                        <div class="my-1">
                            <h4>{{ __('labels.upload_change_logo') }}</h4>
                            <p>{{ __('labels.images_has-to_be_300_x_85_ratio') }}</p>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 form-group input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-file-image"></i></span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="{{ \DefConst::IMGTYPE_SITE_LOGO }}">
                                    <label class="custom-file-label">{{ __('labels.choose_file') }}</label>
                                </div>
                            </div>
                        </div>
                        <div>
                            <img class="img-thumbnail" src="{{ $site->site_logo ?? asset('images/placeholder-site.png') }}">
                        </div>
                    
                        <div class="row">
                            <div class="col-12 form-group">
                                <button type="submit" class="btn btn-primary btn-submit btn-lg float-right">{{ __('buttons.save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card border-0 shadow-sm my-4">
                <div class="card-header bg-white">
                    <h5>{{ __('labels.change_password') }}</h5>
                </div>
                <form method="POST" action="{{ route('site.settings.save.password') }}">
                    <div class="card-body">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-4 col-md-3 col-lg-2 col-form-label" for="current_password">{{ __('labels.current_password') }}</label>
                            <div class="col-sm-8 col-md-9 col-lg-10">
                                <input type="password" id="current_password" name="current_password" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-md-3 col-lg-2 col-form-label" for="new_password">{{ __('labels.new_password') }}</label>
                            <div class="col-sm-8 col-md-9 col-lg-10">
                                <input type="password" id="new_password" name="new_password" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-md-3 col-lg-2 col-form-label" for="retype_password">{{ __('labels.retype_password') }}</label>
                            <div class="col-sm-8 col-md-9 col-lg-10">
                                <input type="password" id="retype_password" name="retype_password" class="form-control" value="">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary btn-lg float-right ml-2">{{ __('buttons.save') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#name').keyup(function(){
            slugKeyName();
        });

        $('.btn-submit').click(function(){
            slugKeyName();
        });

        function slugKeyName() {
            var key = $('#name').val();
            key = key.replace(/ /g, "-").toLowerCase();
            $('#slug_name').val(key);
        }
    });
</script>
@endsection