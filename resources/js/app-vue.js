window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo';

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    forceTLS: true
});


window.Vue = require('vue');

import OrderComponent from './components/OrderProgress.vue';

Vue.component('order-progress', OrderComponent);

// Vue application
const app = new Vue({
    el: '#app',
    mounted() {
        window.Echo.channel('Order-tracker')
        .listen('OrderStatusChanged', (e) => {
            console.log('oomg real time bro...');
        });
    }
});