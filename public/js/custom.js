function getParameterByName(a) {
    a = a.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var b = new RegExp("[\\?&]" + a + "=([^&#]*)"),
        c = b.exec(location.search);
    return null == c ? "" : decodeURIComponent(c[1].replace(/\+/g, " "))
}

function setMainPicById(a) {
    var b = $("a.gallerythumb[vid='" + a + "']").first();
    return setMainPicByThumb(b, !1)
}

function setMainPicByThumb(a, b) {
    if (a.attr("href") && "image" == a.attr("type") || !a.attr("type")) {
        $(".mainpic-model").hide(), $(".mainpic-image").show(), $(".gallerythumb").removeClass("selected"), $(".mainpic").attr("src", "images/image-loader.gif"), $(".mainpic").attr("src", a.attr("href")), $(".js-zoom-img").attr("data-src", a.attr("zoom-img")), $(".main-image-highres").length && $(".main-image-highres").html("<a href='" + a.attr("highreslink") + "' target='_blank' class='button  button--small  button--dark'>Download Hi-Res Image</a><div><small>Image Dimensions: " + a.attr("highressize") + '. To download: Right click and choose "Save link as."</small></div></div>'), a.addClass("selected"), b && a.attr("vid") > 0 && setSelectedByVid(a.attr("vid"), $(".addToCart"), !1);
        var c = a.attr("data-originalwidth"),
            d = a.attr("data-originalheight");
        c > 800 || d > 800 ? $(".js-zoom-img-trigger").show() : $(".js-zoom-img-trigger").hide();
        var e = a.find("img").attr("data-description");
        "" !== e ? $("#js-product-image-caption-text").show(0).html(e) : $("#js-product-image-caption-text").hide(0).html("")
    } else if (a.attr("href") && "model" == a.attr("type")) {
        googleConversionEvent("productmodelview", []), $(".gallerythumb").removeClass("selected"), a.addClass("selected"), $(".mainpic-image").hide();
        var f = $(".sketchfab-embed-wrapper").attr("modeluid");
        f != a.attr("modeluid") && ($(".sketchfab-embed-wrapper").html('<iframe\nwidth="100%"\nsrc="' + a.attr("href") + '"\nframeborder="0"\nallow="autoplay; fullscreen; vr"\nmozallowfullscreen="true"\nwebkitallowfullscreen="true">\n</iframe>'), $(".sketchfab-embed-wrapper").attr("modeluid", a.attr("modeluid"))), $(".mainpic-model").show(), $("#js-product-image-caption-text").hide(0).html("3D Model of product")
    } else $(".mainpic").attr("src", "images/no-image-available.png"), $("#js-product-image-caption-text").html("No image available");
    return !1
}

function setSelectedByVid(a, b, c) {
    var d = $(b).find('.qtySelector[vid="' + a + '"]').attr("feature1"),
        e = $(b).find('.qtySelector[vid="' + a + '"]').attr("feature2"),
        f = $(b).find('.qtySelector[vid="' + a + '"]').attr("code"),
        g = !1,
        h = $(b).find('.qtySelector[vid="' + a + '"]').attr("selling_packsize"),
        i = $(b).find('.qtySelector[vid="' + a + '"]').attr("display_packsize");
    h || (h = 1), i || (i = 1), $(b).find(".qtySelector").hide(), $(b).find(".addToCartQty").val("0"), $(b).find('.qtySelector[vid="' + a + '"]').show(), $(b).find('.qtySelector[vid="' + a + '"]').find(".addToCartQty").val(h);
    var j = $(b).find('.qtySelector[vid="' + a + '"]').attr("ribbon");
    j ? ($(".js-variation-sticker-when-special").attr("src", j), $(".js-variation-sticker-when-special").show()) : $(".js-variation-sticker-when-special").hide(), "Yes" == $(b).find('.qtySelector[vid="' + a + '"]').attr("new") ? $(".js-variation-sticker-when-new").show() : $(".js-variation-sticker-when-new").hide();
    var k = {
        show_add: "#addToCartButton",
        show_notify: "#notify_area",
        show_wish: "#addToWishListButton",
        show_registry: "#addToRegistryButton"
    };
    for (var l in k) "Yes" == $(b).find('.qtySelector[vid="' + a + '"]').attr(l) ? $(b).find(k[l]).show() : $(b).find(k[l]).hide();
    showNotify(a), "Yes" != $(b).find('.qtySelector[vid="' + a + '"]').attr("show_add") && $('.qtySelector[vid="' + a + '"]').attr("outofstock_special") && $.get($('.qtySelector[vid="' + a + '"]').attr("outofstock_special") + "?overrideincludes=popup", function (a) {
        openDialog("Special", a, 500, 500)
    }, "html"), "Yes" == $('.qtySelector[vid="' + a + '"]').attr("show_price") ? (price = number_format(parseFloat($('.qtySelector[vid="' + a + '"]').attr("price").replace(",", "") * h, 2).toFixed(2), 2), priceparts = price.split("."), $(b).find(".selectedPrice").html('<span itemprop="price" content="' + price.replace(",", "") + '">R' + priceparts[0] + "<sup class='product-page-price__decimal'>." + priceparts[1] + "</sup></span>"), $('.qtySelector[vid="' + a + '"]').attr("price_was") ? ($(b).find(".js-price-was").html("<p class='yc-paragraph  yc-paragraph--small  yc-paragraph--flush  u-text-center'><span class='product-was-price'>Was R" + $('.qtySelector[vid="' + a + '"]').attr("price_was") + "</span>"), $(b).find(".selectedPrice").addClass("u-text-red")) : ($(b).find(".js-price-was").html(""), $(b).find(".selectedPrice").removeClass("u-text-red"))) : $(b).find(".selectedPrice").html("TBA"), $(b).find(".selectedFeatureCode").html($('.qtySelector[vid="' + a + '"]').attr("code")), i > 1 ? $(".js-selected-pack-size").html($(b).find('.qtySelector[vid="' + a + '"]').attr("packdescription") + " of " + i) : $(".js-selected-pack-size").html(""), $(b).find("#selectedFeature1").html(d), $(b).find("#selectedFeature2").html(e), $(b).find("#selectedFeatureCode").html(f);
    var m = "";
    m = d && e ? e + ", " + d : e ? e : d, m && $(".js-product-qr-name").html(m), $(".js-product-qr-uri").attr("src", "/api/qr/variation/" + a + ".png?size=182&margin=0&lookup=WEB"), $(b).find(".featureSelector").removeClass("inactive"), $(b).find(".featureSelector").removeClass("featureSelectorSelected"), c || $(b).find(".featureSelector").removeClass("select"), $(b).find(".featureSelector").removeClass("sold"), $(b).find(".soldtext").remove(), $(b).find('.featureSelectorContainer[feature="feature2"]').find(".featureSelector").each(function () {
        $(this).attr("feature2") == e && ($(this).addClass("featureSelectorSelected"), c || $(this).addClass("select")), 0 == $(b).find('.featureSelectorContainer[feature="feature1"]').length && "Yes" != vidForFeatureSet(b, d, $(this).attr("feature2"), !1, a, "show_add") && $(this).addClass("sold"), vidForFeatureSet(b, d, $(this).attr("feature2"), !1, a, "vid") || ($(this).hasClass("featureSelectorSelected"), $(this).addClass("inactive"))
    }), $(b).find('.featureSelectorContainer[feature="feature1"]').find(".featureSelector").each(function () {
        $(this).attr("feature1") == d && ($(this).addClass("featureSelectorSelected"), c || $(this).addClass("select")), "Yes" != vidForFeatureSet(b, $(this).attr("feature1"), e, !1, a, "show_add") && vidForFeatureSet(b, $(this).attr("feature1"), e, !1, a, "vid") && $(this).addClass("sold"), vidForFeatureSet(b, $(this).attr("feature1"), e, !1, a, "vid") || ($(this).hasClass("featureSelectorSelected") && (g = 1), $(this).addClass("inactive"))
    }), $(".featureSelectorContainer .featureSelector").each(function () {
        0 == $(this).children().size() && $(this).hide()
    }), "undefined" != typeof dataLayer && dataLayer.push({
        event: "productDetailImpression",
        ecommerce: {
            detail: {
                products: [{
                    name: $("#product").attr("productname") || "",
                    id: a,
                    price: price,
                    brand: $("#product").attr("brand") || "",
                    category: $("#product").attr("google-category") || "",
                    variant: (d ? d : "") + (e ? ", " + e : "")
                }]
            }
        }
    })
}

function vidForFeatureSet(a, b, c, d, e, f) {
    var g = !1;
    return $(a).find(".qtySelector").each(function () {
        d && $(this).attr("vid") == e ? g = $(this).attr(f) : $(this).attr("feature2") && c && $(this).attr("feature2") != c || $(this).attr("feature1") && b && $(this).attr("feature1") != b || (g = $(this).attr(f))
    }), g
}

function showNotify(a) {
    $("#notifyForm").show(), $("#notifyForm #notifyvariationid").val(a), $("#notify_area #notificationMessage").html("")
}

function erpround(a) {
    var b = !1;
    return a < 0 ? (a *= -1, b = !0) : b = !1, bigval = parseInt(1e5 * a), smallval = 10 * parseInt(1e4 * a), diffvals = bigval - smallval, a = diffvals >= 5 ? 1 * (bigval + "").slice(0, -1) + 1 : 1 * (bigval + "").slice(0, -1), bigval = 1 * (a + "").slice(0, -3), smallval = 10 * (a + "").slice(0, -4), diffvals = bigval - smallval, a = diffvals >= 5 ? 1 * (bigval + "").slice(0, -1) + 1 : 1 * (bigval + "").slice(0, -1), b && (a *= -1), a *= 1
}

function number_format(a, b) {
    for (b === !1 && (b = 2), void 0 == b && (b = 2), multiplier = Math.pow(10, b), a < 0 ? (a *= -1, negative = !0) : negative = !1, nval = Math.round(a * multiplier) + "", len = nval.length, 1 == len ? addzero = "0" : addzero = "", deci = addzero + nval.substr(len - b, b), multiplier += "", deci = 1 * deci ? deci : multiplier.substr(1), num = nval.substr(0, len - b), num = num ? num : "0", temp = num.split("").reverse(), cnt = 3; temp[cnt];) "-" != temp[cnt] && (temp[cnt] = temp[cnt] + ","), cnt += 3;
    return num = temp.reverse().join(""), nval = deci ? num + "." + deci : num, negative && (nval = "-" + nval), nval
}
$(function () {
    hs.headingOverlay.fade = !1, ($(".jcarousel").length > 0 || $(".jcarousel-2wide").length > 0 || $(".jcarousel-3wide").length > 0) && ($(".jcarousel").jcarousel({
        scroll: 4,
        visible: 4
    }), $(".jcarousel-3wide").jcarousel({
        scroll: 3,
        visible: null
    }), $(".jcarousel-2wide").jcarousel({
        scroll: 1,
        visible: 1
    }))
});
var clickedvid, hovervid;
$(function () {
    $(".notify_me_link").click(function () {
        clickedvid = $(this).closest(".qtySelector").attr("vid"), $("#notify_area").show(), showNotify(clickedvid)
    }), $(".addtocartarea").each(function () {
        var a = $(this);
        a.hasClass("selector") && (a.find(".featureDetails").hide(), a.find(".js-product-page-price-wrapper").hide(), a.find(".featureLabel").hide(), a.find(".selectQuantityLabel").show(), a.find(".qtySelector").hide(), a.find(".featureSelector").click(function () {
            var a = $(this).closest(".addToCart"),
                b = $(this).closest(".featureSelectorContainer");
            $(b).find(".featureSelector").removeClass("featureSelectorSelected"), $(b).find(".featureSelector").removeClass("select"), $(this).addClass("featureSelectorSelected"), $(this).addClass("select");
            var c = $(this).attr("vid"),
                d = $(a).find('.featureSelectorContainer[feature="feature1"]').find(".featureSelectorSelected").attr("feature1"),
                e = $(a).find('.featureSelectorContainer[feature="feature2"]').find(".featureSelectorSelected").attr("feature2"),
                f = $(a).find('.featureSelectorContainer[feature="code"]').find(".featureSelectorSelected").html(),
                g = vidForFeatureSet(a, d, e, f, c, "vid");
            return clickedvid = g, setMainPicById(clickedvid), setSelectedByVid(g, a, !1), !1
        }), getParameterByName("vid") ? (setMainPicById(getParameterByName("vid")), setSelectedByVid(getParameterByName("vid"), $(".addToCart"), !1)) : a.find(".featureSelectorContainer").each(function () {
            $(this).find(".featureSelector").first().click()
        })), $(".gallerythumb").click(function () {
            return setMainPicByThumb($(this), a.hasClass("selector")), !1
        })
    }), "1" == $("#noOfVariationsOnPage").val() && (clickedvid = $(".addtocartarea").find(".qtySelector").first().attr("vid"), setMainPicById(clickedvid), setSelectedByVid(clickedvid, $(".addToCart"), !1), "Yes" != $(".addtocartarea").find('.qtySelector[vid="' + clickedvid + '"]').attr("show_add") && $('.qtySelector[vid="' + clickedvid + '"]').attr("outofstock_special") && $.get($('.qtySelector[vid="' + clickedvid + '"]').attr("outofstock_special") + "?overrideincludes=popup", function (a) {
        openDialog("Oh no, weâ€™ve sold out! But all is not lost...", a, 400, 540)
    }, "html")), $("<img/>")[0].src = "images/image-loader.gif", 1 == $("#gallery_icons").find(".gallerythumb").length && ($("#gallery_icons").hide(), $(".thumb_tab_elements").hide()), $(".addthis-item--pinterest a").live("click", function () {
        picurl = $(".mainpic").attr("src"), pinurl = $(this).attr("data-pinterest-url") + picurl, $(this).attr("href", pinurl);
        var a = window.screen.height / 2 - 218,
            b = window.screen.width / 2 - 313;
        return window.open(this.href, "sharer", "toolbar=0,status=0,width=626,height=256,top=" + a + ",left=" + b), !1
    }), $("body").on("click", ".js-zoom-img-trigger", function () {
        $(".js-zoom-img-loading").show(), $(".js-zoom-img-tip").hide(), $(".js-zoom-img").attr("src", "");
        var a = $(".js-zoom-img").attr("data-src");
        $(".js-zoom-img").attr("src", a), $("#zoom-img").trigger("zoom.destroy"), $("#zoom-img").css("display", "block").parent().zoom({
            url: a,
            on: "click",
            callback: function () {
                $(".js-zoom-img-loading").hide(), $(".js-zoom-img-tip").show()
            }
        }), lazySizes.loader.checkElems()
    })
}), $(function () {
    $("#notifyForm").submit(function () {
        $(".js-notify-me-loading").show(), $("#notifyForm").hide();
        var a = $("#notifyForm").serialize();
        return $.post("notify.htm?overrideincludes=blank&format=json", a, function (a) {
            $(".js-notify-me-loading").hide(), a.success ? ($(".js-notify-me-success").show().find(".js-notify-me-message").html(a.message), $(".js-notify-me-info").hide()) : ($(".js-notify-me-info").show().find(".js-notify-me-message").html(a.message), $(".js-notify-me-success").hide(), $("#notifyForm").show())
        }, "json"), !1
    })
}), $(function () {
    $(".js-repeat-orders-option").change(function () {
        "subscribe" == $("input:radio[name=subscribe]:checked").val() ? ($("#js-repeat-orders-block").show(), $("#addToRegistryButton").hide()) : ($("#js-repeat-orders-block").hide(), $("#addToRegistryButton").show())
    })
}), $(function () {
    $(".js-product-image-guide-button").on("click", function (a) {
        a.preventDefault(), $(".js-product-image-guide").hasClass("yc-product-image-guide--is-open") ? ($(".js-product-image-guide").removeClass("yc-product-image-guide--is-open"), $(".js-product-image-guide-button").attr("data-tooltip", "Show Guides")) : ($(".js-product-image-guide").addClass("yc-product-image-guide--is-open"), $(".js-product-image-guide-button").attr("data-tooltip", "Hide Guides"))
    })
}), Math.round = erpround;


function pushSuggestions(suggestions, ar, name) {
    if (ar) {
        suggestions.push({"heading":name});
        $.each(ar, function(i, val){
            suggestions.push(val);
        });
    }
}

$(".js-instant-search").live('keyup.autocomplete', function(){
    var inpbox = $(this);
    $(this).autocomplete({
        source: function(req, add){
            //pass request to server
            req.cores = $(inpbox).attr("cores");
            req.resultlength = $(inpbox).attr("resultlength");
            req.origin = $(inpbox).attr("origin");
            $.getJSON("/searchsuggest.php?", req, function(data) {
                //create array for response objects
                var suggestions = [];
                pushSuggestions(suggestions,data.o,"Track My Order");
                pushSuggestions(suggestions,data.c,"Categories &amp; Brands");
                pushSuggestions(suggestions,data.p,"Popular Products");
                pushSuggestions(suggestions,data.s,"Popular Searches");
                pushSuggestions(suggestions,data.h,"Help Center");
                pushSuggestions(suggestions,data.f,"Community Forum");
                //pass array to callback
                add(suggestions);
            });
        },

        appendTo: $(this).closest("div"),

        minLength: 3,

        autoFocus: true,

        focus: function( event, ui ) {
            return false;
        },

        select: function( event, ui ) {
            $(inpbox).val(ui.item.name.replace(/(<([^>]+)>)/ig,""));
            $(inpbox).closest("form").find(".search-button").addClass("search-button--loading");
            setTimeout(function() { window.location = searchRedirect(ui.item); }, 1);
            return false;
        }

    }).data("autocomplete")._renderItem = function( ul, item ) {
        if (item.heading) {
            return $( "<li class='predictive-search__item  predictive-search__item--heading'></li>" )
                .append("<div class='predictive-search__heading'><div class='predictive-search__text'>"+item.heading+"</div></div>")
                .appendTo( ul );
        } else {
            var patt = new RegExp(item.term,"i");
            var name = item.name.replace(patt,'<b>$&</b>');
            return $( "<li class='predictive-search__item'></li>" )
                .data( "item.autocomplete", item )
                .append( "<a href='"+searchRedirect(item)+"' class='predictive-search__link'>" + (item.thumbnail ? "<div class='predictive-search__image-wrapper'><img src='"+item.thumbnail+"' alt='' class='predictive-search__image'/></div>" : "") + "<div class='predictive-search__text'>" + name + "</div>" + "</a>" )
                .appendTo( ul );
        }
    };
    $(this).autocomplete("widget").addClass("predictive-search");
    $(this).autocomplete("widget").removeClass("ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all");
});
function searchRedirect(item) {
    return "/search.php?action=click"+(item.id ? "&id="+item.id : "")+"&sec="+item.sec+"&origin="+item.origin+"&q="+encodeURIComponent(item.term)+(item.url_params ? "&"+item.url_params : "")+"&url="+encodeURIComponent(item.url);
}

