<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locale/{locale?}', function ($locale){
    session()->put('locale', $locale);
    return redirect()->back();
})->name('get.language');

Route::group(['middleware' => 'Shop'], function () {
	////////////////////////////////////////////////
	Route::get('/shop/products/search', 'FilterProductsController@search')->name('shop.products.sort.search');
	Route::get('/shop/products/price', 'FilterProductsController@price')->name('shop.products.sort.price');
	Route::get('/shop/products/ratings', 'FilterProductsController@ratings')->name('shop.products.sort.ratings');
	Route::get('/shop/products/latest', 'FilterProductsController@latest')->name('shop.products.sort.latest');
	Route::get('/shop/products/range', 'FilterProductsController@range')->name('shop.products.sort.range');
	Route::get('/shop/products/category', 'FilterProductsController@category')->name('shop.products.sort.category');
	Route::get('/shop/products/brands', 'FilterProductsController@brands')->name('shop.products.sort.brands');

	Route::get('/shop/finish', 'WebController@finish')->name('shop.finish');
	Route::post('/shop/checkout/sale', 'WebController@checkoutSale')->name('shop.checkout.sale');
	Route::get('/shop/checkout', 'WebController@checkout')->name('shop.checkout');
	Route::get('/shop/cart', 'WebController@cart')->name('shop.cart');
	Route::get('/shop/updatecart', 'WebController@updateCart')->name('shop.updatecart');
	Route::get('/shop/removecart', 'WebController@removeItemFromCart')->name('shop.removeitemfromcart');
	Route::get('/shop/additemcart', 'WebController@addItemToCart')->name('shop.additemtocart');
	Route::get('/shop/products', 'WebController@products')->name('shop.products');
	Route::get('/shop/product/{id?}', 'WebController@product')->name('shop.product');
	Route::get('/shop/sites/application', 'WebController@site_application')->name('shop.sites.application');
	Route::get('/shop/partner', 'WebController@partner')->name('shop.partner');
	Route::get('/shop/resources', 'WebController@resources')->name('shop.resources');
	Route::get('/shop/{slug}', 'WebController@store')->name('shop.store');
	Route::get('/', 'WebController@index')->name('shop.index');
});

// Helpers
Route::get('/subcategories', 'HelperController@getsubcategories')->name('helper.getsubcategories');


Route::prefix('user')->group(function() {
	Route::get('/', 'HomeController@index')->name('user.home');
});

Route::match(['get','post'],'/logout','Auth\LoginController@logout')->name('logout');
Auth::routes();

Route::prefix('admin')->group(function() {
	//Products
	Route::get('/products', 'Admin\ProductController@index')->name('admin.products');
	// Categories
	Route::get('/category', 'Admin\CategoryController@index')->name('admin.category');
	Route::get('/category/add', 'Admin\CategoryController@add')->name('admin.category.add');
	Route::get('/category/edit/{id?}', 'Admin\CategoryController@edit')->name('admin.category.edit');
	Route::post('/category/save', 'Admin\CategoryController@save')->name('admin.category.save');
	Route::post('/category/update', 'Admin\CategoryController@update')->name('admin.category.update');
	// Sub Categories
	Route::get('/subcategory', 'Admin\SubCategoryController@index')->name('admin.subcategory');
	Route::get('/subcategory/add', 'Admin\SubCategoryController@add')->name('admin.subcategory.add');
	Route::get('/subcategory/edit/{id?}', 'Admin\SubCategoryController@edit')->name('admin.subcategory.edit');
	Route::post('/subcategory/save', 'Admin\SubCategoryController@save')->name('admin.subcategory.save');
	Route::post('/subcategory/update', 'Admin\SubCategoryController@update')->name('admin.subcategory.update');
	// Classifications
	Route::get('/classifications', 'Admin\ClassificationController@index')->name('admin.classifications');
	Route::get('/classifications/add', 'Admin\ClassificationController@add')->name('admin.classifications.add');
	Route::get('/classifications/edit/{id?}', 'Admin\ClassificationController@edit')->name('admin.classifications.edit');
	Route::post('/classifications/save', 'Admin\ClassificationController@save')->name('admin.classifications.save');
	Route::post('/classifications/update', 'Admin\ClassificationController@update')->name('admin.classifications.update');
	// Banners
	Route::get('/banner', 'Admin\BannerController@index')->name('admin.banner');
	Route::get('/banner/approval/{id?}', 'Admin\BannerController@approval')->name('admin.banner.approval');
	Route::post('/banner/approved', 'Admin\BannerController@approved')->name('admin.banner.approved');
	Route::post('/banner/rejected', 'Admin\BannerController@rejected')->name('admin.banner.rejected');
	// Card PUB
	Route::get('/customisation/card', 'Admin\CustomisationController@card')->name('admin.customisation.card');
	Route::get('/customisation/card/add', 'Admin\CustomisationController@card_add')->name('admin.customisation.card.add');
	Route::get('/customisation/card/edit/{id?}', 'Admin\CustomisationController@card_edit')->name('admin.customisation.card.edit');
	Route::post('/customisation/card/save', 'Admin\CustomisationController@card_save')->name('admin.customisation.card.save');
	Route::post('/customisation/card/update', 'Admin\CustomisationController@card_update')->name('admin.customisation.card.update');
	Route::post('/customisation/card/delete', 'Admin\CustomisationController@card_delete')->name('admin.customisation.card.delete');
	// Site Config
	Route::get('/sites', 'Admin\SiteController@index')->name('admin.sites');
	Route::get('/sites/add','Admin\SiteController@add')->name('admin.sites.add');
	Route::get('/sites/edit/{id?}', 'Admin\SiteController@edit')->name('admin.sites.edit');
	Route::post('/sites/save', 'Admin\SiteController@save')->name('admin.sites.save');
	Route::post('/sites/update', 'Admin\SiteController@update')->name('admin.sites.update');
	Route::get('/sites/applications', 'Admin\SiteController@site_application_index')->name('admin.sites.application.index');
	Route::get('/sites/applications/edit/{id?}', 'Admin\SiteController@site_application_edit')->name('admin.sites.application.edit');
	Route::post('/sites/application/save', 'Admin\SiteController@site_application_save')->name('admin.sites.application.save');
	Route::get('/sites/application/approval/{id?}', 'Admin\SiteController@site_application_approval')->name('admin.sites.application.approval');
	Route::post('/sites/application/approved', 'Admin\SiteController@site_application_approved')->name('admin.sites.application.approved');
	Route::post('/sites/application/rejected', 'Admin\SiteController@site_application_rejected')->name('admin.sites.application.rejected');
    // admin login & logout routes
	Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/logout','Auth\AdminLoginController@adminLogout')->name('admin.logout');
	// admin pw reset routes
	Route::post('/password/email','Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	Route::get('/password/reset','Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	Route::post('/password/reset','Auth\AdminResetPasswordController@reset');
	Route::get('/password/reset/{token}','Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    // admin homepage (dashboard)
    Route::get('/','Admin\HomeController@index')->name('admin.dashboard');
});

Route::prefix('site')->group(function() {
	// Products
	Route::get('/products', 'Site\ProductController@index')->name('sites.products');
	Route::get('/products/add', 'Site\ProductController@add')->name('sites.products.add');
	Route::get('/products/edit/{id?}', 'Site\ProductController@edit')->name('sites.products.edit');
	Route::post('/products/save', 'Site\ProductController@save')->name('sites.products.save');
	Route::post('/products/update', 'Site\ProductController@update')->name('sites.products.update');
	Route::post('/products/delete', 'Site\ProductController@delete')->name('sites.products.delete');
	// Site banner
	Route::get('/banner', 'Site\BannerController@index')->name('sites.banner');
	Route::post('/banner', 'Site\BannerController@save')->name('sites.banner.save');
	Route::post('/banner/approval', 'Site\BannerController@approval')->name('sites.banner.approval');
	// Site setting
	Route::get('/settings', 'Site\SettingController@index')->name('sites.settings');
	Route::post('/settings/save/details', 'Site\SettingController@save_details')->name('site.settings.save.details');
	Route::post('/settings/save/password', 'Site\SettingController@save_password')->name('site.settings.save.password');
    // site login & logout routes
	Route::get('/login','Auth\SiteLoginController@showLoginForm')->name('site.login');
	Route::post('/login','Auth\SiteLoginController@login')->name('site.login.submit');
	Route::get('/logout','Auth\SiteLoginController@siteLogout')->name('site.logout');
	// site pw reset routes
	Route::post('/password/email','Auth\SiteForgotPasswordController@sendResetLinkEmail')->name('site.password.email');
	Route::get('/password/reset','Auth\SiteForgotPasswordController@showLinkRequestForm')->name('site.password.request');
	Route::post('/password/reset','Auth\SiteResetPasswordController@reset');
	Route::get('/password/reset/{token}','Auth\SiteResetPasswordController@showResetForm')->name('site.password.reset');
    // site homepage (dashboard)
    Route::get('/','Site\HomeController@index')->name('site.dashboard');
});

Route::prefix('service')->group(function() {
    // service login & logout routes
	Route::get('/login','Auth\ServiceLoginController@showLoginForm')->name('service.login');
	Route::post('/login','Auth\ServiceLoginController@login')->name('service.login.submit');
	Route::get('/logout','Auth\ServiceLoginController@serviceLogout')->name('service.logout');
	// service pw reset routes
	Route::post('/password/email','Auth\ServiceForgotPasswordController@sendResetLinkEmail')->name('service.password.email');
	Route::get('/password/reset','Auth\ServiceForgotPasswordController@showLinkRequestForm')->name('service.password.request');
	Route::post('/password/reset','Auth\ServiceResetPasswordController@reset');
	Route::get('/password/reset/{token}','Auth\ServiceResetPasswordController@showResetForm')->name('service.password.reset');
	
	// Accept Delivery
	Route::post('/acceptdelivery', 'Service\HomeController@acceptdelivery')->name('service.accept.delivery');
	Route::post('/changedstatus', 'Service\HomeController@changedstatus')->name('service.changed.status');
	// service homepage (dashboard)
    Route::get('/','Service\HomeController@index')->name('service.dashboard');
});