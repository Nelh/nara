<?php

class DefConst {

	public const ACTIVE = 'Y';		
	public const NOTACTIVE = 'N';

	public const YES = 1;		
	public const NO = 0;

	public const TAX = 15;
	public const CURRENCY = "CAD";
	public const ITEMCODE = 'ITEMCODE00';
	public const INVOICE = 'INVOICE';
	public const APPLICATIONCODE = 'APPLICATIONCODE00';


	public const PAID = 'PAID';
	public const NOTPAID = 'NOTPAID';

	// Status
	public const PENDING = 'pending';
	public const APPROVED = 'approved';
	public const REJECTED = 'rejected';

	// Order Status
	public const PLACED = "Placed";
	public const PICKUP = "Pickup";
	public const ARRIVEDATFACILITY = "Arrived At facility";
	public const OUTOFDELIVERY = "Out of delivery";
	public const DELIVERED = "Delivered";
	public const COMPLETED = "Completed";

	public const ORDER = [
		self::PLACED,
		self::PICKUP,
		self::ARRIVEDATFACILITY,
		self::OUTOFDELIVERY,
		self::DELIVERED,
	];

	public const CITY_POINTENOIRE = "pointe-noire";
	public const CITY_BRAZZAVILLE = "brazzaville";
	public const CITIES = [
		self::CITY_POINTENOIRE,
		self::CITY_BRAZZAVILLE,
	];

	public const RETAIL = "R";
	public const SALE = "S";
	public const TYPES = [
		self::RETAIL,
		self::SALE,
	];


	// images names type
	public const IMGTYPE_SITE_LOGO = "site_logo";
    public const IMGTYPE_SITE_BANNER = "site_banner";
	public const IMGTYPE_USER = "user_image";
	public const IMGTYPE_ITEM = "item_image";
	public const IMGTYPE_SHOP_HERO = "site_shop_hero";
	public const IMGTYPE_WEB_CARD = "web_image_card";
	// images rotation angles
	public const ROTATE_LEFT = 90;
	public const ROTATE_RIGHT = -90;

}
