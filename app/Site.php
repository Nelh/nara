<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\SitePasswordResetNotification;

class Site extends Authenticatable
{
    use Notifiable;

    protected $guard = 'site';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','active', 'name', 'slug_name', 'email', 'address', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // overridden function for sending pw reset notification 
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new SitePasswordResetNotification($token));
    }

    public function images()
	{
		return $this->belongsToMany('\App\Models\Image','site_images');
	}
}
