<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item_groups extends Model
{
    protected $table = 'item_groups';

    protected $fillable = [
		'site_id', 'name', 'type'
	];
}
