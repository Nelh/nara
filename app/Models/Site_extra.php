<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site_extra extends Model
{
    protected $table = 'site_extra';

    protected $fillable = [
        'status', 'banner', 'banner_color', 'note', 'image_id'
    ];
}
