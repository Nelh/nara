<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
	protected $table = 'items';
	

	protected $fillable = [
        'site_id', 'list_item_category_id', 'list_item_brand_id', 'list_groups_id', 'recordno', 'active',
        'name', 'description', 'specification', 'size', 'deduction', 'warranty', 'price', 'location'
	];
	
	protected $casts = [
		'price' => 'double',
	];

	public function images()
	{
		return $this->belongsToMany('\App\Models\Image','item_images');
	}
}
