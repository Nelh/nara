<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $table = 'images';

	protected $fillable = [
		'xsize','ysize','image_type','name','original_name','url',
	];


	public function sites()
	{
		return $this->belongsToMany('App\Site','site_images');
	}

	public function items()
	{
		return $this->belongsToMany('App\Models\Item','item_images');
	}


}
