<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Web_card extends Model
{
    protected $table = 'web_card';

	protected $fillable = [
        'subheading','heading', 'color'
    ];
}
