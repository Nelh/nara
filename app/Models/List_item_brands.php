<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class List_item_brands extends Model
{
    protected $table = 'list_item_brands';

	protected $fillable = [
        'id','active', 'image_id', 'type', 'name', 'country', 'notes'
    ];
}
