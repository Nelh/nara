<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shopping_cart_items extends Model
{
    protected $table = 'shopping_cart_items';

    protected $fillable = [
        'shopping_cart_id', 'site_id', 'item_id', 'qty', 'price', 'deleted_at'
    ];
}
