<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'user_id', 'item_sales_id', 'invoiceno', 'status', 'status_track_id', 'paid_at', 'latestupdate', 'receivername', 'receivertel', 
        'deliveryaddress', 'deliverysuburb', 'deliverycity', 'addresstype', 'comments', 'deleted_at'
    ];
}
