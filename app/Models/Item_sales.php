<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item_sales extends Model
{
    protected $table = 'item_sales';

    protected $fillable = [
        'user_id', 'shopping_cart_id', 'invoiceno', 'items', 'qty', 'subtotal', 'total', 'paid_at', 'saledate'
    ];
}
