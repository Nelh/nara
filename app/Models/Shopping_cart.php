<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shopping_cart extends Model
{
    protected $table = 'shopping_cart';

    protected $fillable = [
        'user_id', 'invoiceno', 'paid_at', 'deleted_at'
    ];
}
