<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class List_item_category extends Model
{
    protected $table = 'list_item_categories';

	protected $fillable = [
        'id','active','name'
    ];
}
