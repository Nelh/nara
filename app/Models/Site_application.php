<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site_application extends Model
{
    protected $table = 'site_applications';

    protected $fillable = [
        'status','company_name', 'email', 'email', 'industry', 'addr', 'zip', 'city', 'country', 'phone', 'website', 'description'
    ];
}
