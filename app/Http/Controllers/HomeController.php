<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Classification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $page_title;

    public function __construct()
    {
        $this->middleware('auth');
        $this->page_title = __('labels.dashboard');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $item_sales = DB::table('item_sales')
                            ->join('orders', 'orders.item_sales_id', 'item_sales.id')
                            ->where('item_sales.user_id', auth()->id())
                            ->where('orders.active', \DefConst::ACTIVE)
                            ->where('orders.status', '!=', \DefConst::COMPLETED)
                            ->paginate(12);
                            
        return view('users.home', [
            'page_title' => $this->page_title,
            'item_sales' => $item_sales,
        ]);
    }

}
