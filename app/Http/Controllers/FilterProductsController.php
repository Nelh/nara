<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\List_item_brands;
use App\Models\Items;

class FilterProductsController extends Controller
{
    /////////////////////////////////////////////////////////////////////////
    // search
    public function search(Request $request)
    {
        $brands = List_item_brands::all();
        $search = $request->search ?? null;

        $queryAppends = [
			'search' => $search,
		];

        $products = Items::leftjoin('item_images', 'item_images.item_id', 'items.id')
                    ->leftjoin('images', 'images.id', 'item_images.image_id')
                    ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                    ->leftjoin('sites', 'sites.id', 'items.site_id')
                    ->where('sites.active', \DefConst::ACTIVE)
                    ->where('items.location', session()->get('shop.location'))
                    ->when($search, function ($query, $search) {
                        if (strlen($search) <= 128) {                               
                            $sanitizedSearch = preg_replace('/[^A-Za-z0-9.]/',' ', $search);
                            $keywordArray = array_map(function($val){return $val;}, explode(' ',$sanitizedSearch));
                            foreach ($keywordArray as $keyword) {
                                if (strlen($keyword) >= 1) {
                                    $keywordArrayClean[] = "+{$keyword}*";
                                }
                            }

                            if (isset($keywordArrayClean)) {
                                $s4 = implode(' ',$keywordArrayClean);
                                return $query->whereRaw("match (items.name, items.description) against ('{$s4}' in boolean mode)");
                            }
                        }

                        return false;
                    })
                    ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.active',
                                'items.sub_category_id','items.name as item_name', 'items.size', 'items.price', 
                                'images.url', 'item_extras.ratings', 'sites.name as site_name')
					
                    ->paginate(12)
                    ->appends($queryAppends);

        return view('web.products', [
            'products' => $products,
            'browsingby' => 'by Search',
            'brands' => $brands
        ]);
    }

    /////////////////////////////////////////////////////////////////////////
    // Categories
    public function category(Request $request)
    {
        $brands = List_item_brands::all();
        $cat_id = $request->cat_id ?? null;
        $subcat_id = $request->subcat_id ?? null;

        $queryAppends = [
            'cat_id' => $cat_id,
            'subcat_id' => $subcat_id
        ];
        
        $products = DB::table('items')
                        ->leftjoin('item_images', 'item_images.item_id', 'items.id')
                        ->leftjoin('images', 'images.id', 'item_images.image_id')
                        ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                        ->leftjoin('sites', 'sites.id', 'items.site_id')
                        ->where('sites.active', \DefConst::ACTIVE)
                        ->where('items.location', session()->get('shop.location'))
                        ->where('items.list_item_category_id', $cat_id)
                        ->where('items.sub_category_id', $subcat_id)
                        ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.active',
                                'items.sub_category_id','items.name as item_name', 'items.size', 'items.price', 
                                'images.url', 'item_extras.ratings', 'sites.name as site_name')
                        ->paginate(12)
                        ->appends($queryAppends);

        return view('web.products', [
            'products' => $products,
            'browsingby' => 'by Category',
            'brands' => $brands
        ]);
    }

    /////////////////////////////////////////////////////////////////////////
    // Ratings
    public function ratings(Request $request)
    {
        $brands = List_item_brands::all();
        $sort = $request->sort ?? 'asc';
        
        $products = $this->products('item_extras.ratings', $sort, ['sort' => $sort]);

        return view('web.products', [
            'products' => $products,
            'browsingby' => 'by Ratings ' . (($sort == "asc") ? "(Lowest)" : "(Highest)"),
            'brands' => $brands
        ]);
    }

    /////////////////////////////////////////////////////////////////////////
    // Brands
    public function brands(Request $request)
    {
        $brands = List_item_brands::all();
        $brand_id = $request->brand_id ?? null;

        $queryAppends = [
            'brand_id' => $brand_id
        ];
        
        $products = DB::table('items')
                        ->leftjoin('item_images', 'item_images.item_id', 'items.id')
                        ->leftjoin('images', 'images.id', 'item_images.image_id')
                        ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                        ->leftjoin('sites', 'sites.id', 'items.site_id')
                        ->where('sites.active', \DefConst::ACTIVE)
                        ->where('items.location', session()->get('shop.location'))
                        ->where('items.list_item_brand_id', $brand_id)
                        ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.active',
                                'items.sub_category_id','items.name as item_name', 'items.size', 'items.price', 
                                'images.url', 'item_extras.ratings', 'sites.name as site_name')
                        ->paginate(12)
                        ->appends($queryAppends);

        return view('web.products', [
            'products' => $products,
            'browsingby' => 'by Brands',
            'brands' => $brands
        ]);
    }

    /////////////////////////////////////////////////////////////////////////
    // price
    public function price(Request $request)
    {
        $brands = List_item_brands::all();
        $sort = $request->sort ?? 'asc';
        
        $products = $this->products('items.price', $sort, ['sort' => $sort]);

        return view('web.products', [
            'products' => $products,
            'browsingby' => 'by Price ' . (($sort == "asc") ? "(Lowest)" : "(Highest)"),
            'brands' => $brands
        ]);
    }

    // price range
    public function range(Request $request)
    {
        $brands = List_item_brands::all();
        $min = $request->min ?? null;
        $max = $request->max ?? null;
        $minprice = $request->minprice ?? null;
        $maxprice = $request->maxprice ?? null;
        
        $products = DB::table('items')
                        ->leftjoin('item_images', 'item_images.item_id', 'items.id')
                        ->leftjoin('images', 'images.id', 'item_images.image_id')
                        ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                        ->leftjoin('sites', 'sites.id', 'items.site_id')
                        ->where('sites.active', \DefConst::ACTIVE)
                        ->where('items.location', session()->get('shop.location'))
                        ->when($min, function($q) use ($min, $max) {
                            return $q->whereBetween('items.price', [$min, $max]);
                        })
                        ->when($minprice, function($q, $minprice){
                            return $q->where('items.price', '<=', $minprice);
                        })
                        ->when($maxprice, function($q, $maxprice){
                            return $q->where('items.price', '>=', $maxprice);
                        })
                        ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.active',
                                'items.name as item_name', 'items.size', 'items.price', 
                                'images.url', 'item_extras.ratings', 'sites.name as site_name')
                        ->orderBy('items.price')
                        ->paginate(12);

        return view('web.products', [
            'products' => $products,
            'browsingby' => 'by Price',
            'brands' => $brands
        ]);
    }

    /////////////////////////////////////////////////////////////////////////
    // Latest
    public function latest(Request $request)
    {
        $brands = List_item_brands::all();
        $sort = $request->sort ?? 'asc';
        
        $products = $this->products('items.created_at', $sort, ['sort' => $sort]);

        return view('web.products', [
            'products' => $products,
            'browsingby' => __('by Latest Product'),
            'brands' => $brands
        ]);
    }

    private function products($sortTable, $sortQuery, $queryAppends)
    {
        return DB::table('items')
                ->leftjoin('item_images', 'item_images.item_id', 'items.id')
                ->leftjoin('images', 'images.id', 'item_images.image_id')
                ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                ->leftjoin('sites', 'sites.id', 'items.site_id')
                ->where('sites.active', \DefConst::ACTIVE)
                ->where('items.location', session()->get('shop.location'))
                ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.active',
                        'items.name as item_name', 'items.size', 'items.price', 
                        'images.url', 'item_extras.ratings', 'sites.name as site_name')
                ->orderBy($sortTable, $sortQuery)
                ->paginate(12)
                ->appends($queryAppends);
    }

}
