<?php

namespace App\Http\Controllers\Service;  

use App\Events\OrderStatusChanged;
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Order;


class HomeController extends Controller
{
    private $page_title;

    public function __construct()
    {
        $this->middleware('auth:service');
        $this->page_title = __('labels.dashboard');
    }

    public function index()
    {        
        $delivery = DB::table("orders")
                        ->join('order_delivery', 'order_delivery.order_id', 'orders.id')
                        ->where('service_id', Auth::guard('service')->id())
                        ->where('orders.status', '!=', \DefConst::PLACED)
                        ->where('orders.status', '!=', \DefConst::COMPLETED)
                        ->where('orders.active', \DefConst::ACTIVE)
                        ->select('orders.id', 'orders.invoiceno', 'orders.active', 'orders.status', 'orders.status_track_id', 'orders.priority', 'orders.duedate',
                                'orders.receivername','orders.receivertel', 'orders.deliveryaddress', 'orders.deliverysuburb', 'orders.deliverycity', 'orders.addresstype')
                        ->first();
        

        if(!$delivery) {
            $orders = DB::table('orders')
                        ->leftjoin('order_delivery', 'order_delivery.order_id', 'orders.id')
                        ->where('orders.status',\DefConst::PLACED)
                        ->where('orders.active', \DefConst::ACTIVE)
                        ->whereNull('order_delivery.service_id')
                        ->select('orders.id', 'orders.invoiceno', 'orders.active', 'orders.status', 'orders.priority', 'orders.duedate',
                                'orders.deliveryaddress', 'orders.deliverysuburb', 'orders.deliverycity', 'orders.addresstype')
                        ->get();
        }


        return view('services.home', [
            'delivery' => $delivery,
            'orders' => $orders ?? null,
            'orders_status' => DB::table('orders_status')->get() ?? null,
            'page_title' => $this->page_title,
        ]);
    }

    public function acceptdelivery(Request $request)
    {
        $order = Order::where('id', $request->get('id'))
                        ->where('status',\DefConst::PLACED)
                        ->first();
        if($order){
            $order->status = \DefConst::PICKUP;
            $order->status_track_id = 2;
            $order->save();
        }
        event(new OrderStatusChanged($order));

        DB::table("order_delivery")->insert([
            "order_id"=> $request->get('id'),
            "service_id"=> Auth::guard('service')->id(),
            "invoiceno" => $order->invoiceno,
        ]);

        return response()->json([
            'next' => route('service.dashboard'),
        ]);
    }

    public function changedstatus(Request $request)
    {
        $order = Order::where('id', $request->get('id'))
                        ->first();
        if($order){
            $order->status = $request->get('name');
            $order->status_track_id = $request->get('track_id');
            $order->save();
        }
        // Desactivate Orderafter Completed
        if($order->status == \DefConst::COMPLETED){
            $order->active = \DefConst::NOTACTIVE;
            $order->save();
        }

        event(new OrderStatusChanged($order));

        return response()->json([
            'next' => "successfully",
        ]);
    }
}