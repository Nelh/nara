<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Site;
use App\Models\Classification;
use App\Models\Site_extra;
use App\Models\Shopping_cart;
use App\Models\Shopping_cart_items;
use App\Models\List_item_brands;
use App\Models\Item_sales;
use App\Models\Order;
use App\Models\Web_card;
use App\Helpers\PaymentHelper;


class WebController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only('checkout');
    }

    public function index()
    {

        $sitex = Site_extra::leftjoin('sites', 'sites.id', 'site_extra.site_id')
                            ->where('sites.active', \DefConst::ACTIVE)
                            ->where('site_extra.status', \DefConst::APPROVED)
                            ->select('site_extra.site_id','site_extra.status', 'site_extra.banner', 'site_extra.banner_color', 'site_extra.note', 'sites.name as site_name', 'sites.slug_name')
                            ->selectRaw('(select url
                                        from images
                                        inner join site_images on site_images.image_id = images.id
                                        where site_images.site_id = site_extra.site_id
                                        and images.image_type = ?) as site_banner', [\DefConst::IMGTYPE_SITE_BANNER])
                            ->inRandomOrder()
                            ->first();

        $cardsx = Web_card::leftjoin('web_images', 'web_images.web_card_id', 'web_card.id')
                            ->leftjoin('images', 'images.id', 'web_images.image_id')
                            ->where('web_card.active', \DefConst::ACTIVE)
                            ->inRandomOrder()
                            ->take(4)
                            ->get();

        $latests = DB::table('items')
                            ->leftjoin('item_images', 'item_images.item_id', 'items.id')
                            ->leftjoin('images', 'images.id', 'item_images.image_id')
                            ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                            ->leftjoin('sites', 'sites.id', 'items.site_id')
                            ->where('sites.active', \DefConst::ACTIVE)
                            ->where('items.location', session()->get('shop.location'))
                            ->select('items.id', 'items.site_id', 'items.active',
                                    'items.name as item_name', 'items.price', 
                                    'images.url', 'item_extras.ratings', 'sites.name as site_name')
                            ->orderBy('items.created_at', 'asc')
                            ->take(6)
                            ->get();


        return view('web.index', [
            'sitex' => $sitex,
            'cardsx' => $cardsx,
            'latests' => $latests,
        ]);
    }

    public function partner(Request $request)
    {               
        return view('web.partner');
    }

    public function resources(Request $request)
    {
        return view('web.resources');
    }

    public function store(Request $request, $slug = null)
    {
        $sitex = Site::join('site_extra', 'site_extra.site_id', 'sites.id')
                    ->where('sites.active', \DefConst::ACTIVE)
                    ->where('sites.slug_name', $slug)
                    ->select('site_extra.site_id','site_extra.status', 'site_extra.banner', 'site_extra.banner_color', 'site_extra.note', 'sites.name as site_name', 'sites.slug_name')
                    ->selectRaw('(select url
                                from images
                                inner join site_images on site_images.image_id = images.id
                                where site_images.site_id = site_extra.site_id
                                and images.image_type = ?) as site_banner', [\DefConst::IMGTYPE_SITE_BANNER])
                    ->selectRaw('(select url
                                from images
                                inner join site_images on site_images.image_id = images.id
                                where site_images.site_id = sites.id
                                and images.image_type = ?) as site_logo', [\DefConst::IMGTYPE_SITE_LOGO])
                    ->first();

        $products = DB::table('items')
                    ->leftjoin('item_images', 'item_images.item_id', 'items.id')
                    ->leftjoin('images', 'images.id', 'item_images.image_id')
                    ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                    ->leftjoin('sites', 'sites.id', 'items.site_id')
                    ->where('sites.active', \DefConst::ACTIVE)
                    ->where('sites.slug_name', $slug)
                    ->where('items.location', session()->get('shop.location'))
                    ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.active',
                            'items.name as item_name', 'items.size', 'items.price', 
                            'images.url', 'item_extras.ratings', 'sites.name as site_name')
                    ->inRandomOrder()
                    ->paginate(12);

        return view('web.store', [
            'products' => $products,
            'sitex' => $sitex,
        ]);
    }

    public function products(Request $request)
    {
        $brands = List_item_brands::all();

        $products = DB::table('items')
                    ->leftjoin('item_images', 'item_images.item_id', 'items.id')
                    ->leftjoin('images', 'images.id', 'item_images.image_id')
                    ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                    ->leftjoin('sites', 'sites.id', 'items.site_id')
                    ->where('sites.active', \DefConst::ACTIVE)
                    ->where('items.location', session()->get('shop.location'))
                    ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.active',
                            'items.name as item_name', 'items.size', 'items.price', 
                            'images.url', 'item_extras.ratings', 'sites.name as site_name')
                    ->inRandomOrder()
                    ->paginate(12);

        return view('web.products', [
            'products' => $products,
            'brands' => $brands
        ]);
    }

    public function product(Request $request, $id)
    {

        $product = DB::table('items')
                    ->leftjoin('item_images', 'item_images.item_id', 'items.id')
                    ->leftjoin('images', 'images.id', 'item_images.image_id')
                    ->leftjoin('sites', 'sites.id', 'items.site_id')
                    ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                    ->leftjoin('list_item_brands', 'list_item_brands.id', 'items.list_item_brand_id')
                    ->leftjoin('list_item_categories', 'list_item_categories.id', 'items.list_item_category_id')
                    ->where('items.id', $id)
                    ->where('sites.active', \DefConst::ACTIVE)
                    ->where('items.location', session()->get('shop.location'))
                    ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.active',
                            'items.name as item_name', 'items.description', 'items.specification', 'items.warranty', 'items.size', 'items.deduction', 'items.price', 
                            'images.url', 'images.xsize', 'images.ysize', 'item_extras.ratings', 'list_item_brands.logo as brand_logo', 'list_item_categories.id as category_id', 'list_item_categories.name as category_name',
                            'sites.name as site_name', 'sites.email as site_email', 'sites.phone1 as site_phone')
                    ->selectRaw('(select url
                            from images
                            inner join site_images on site_images.image_id = images.id
                            where site_images.site_id = sites.id
                            and images.image_type = ?) as site_image', [\DefConst::IMGTYPE_SITE_LOGO])
                    ->first();

        if(!$product){
            return redirect()->route('shop.products');
        }

        $product->qty = $request->session()->get('cart.items'.'.'.$product->id);

        return view('web.product', compact('product'));
    }

    public function cart(Request $request)
    {
        $sitems = $request->session()->get('cart.items', []);

        $products = DB::table('items')
                    ->leftjoin('item_images', 'item_images.item_id', 'items.id')
                    ->leftjoin('images', 'images.id', 'item_images.image_id')
                    ->leftjoin('sites', 'sites.id', 'items.site_id')
                    ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                    ->leftjoin('list_item_brands', 'list_item_brands.id', 'items.list_item_brand_id')
                    ->leftjoin('list_item_categories', 'list_item_categories.id', 'items.list_item_category_id')
                    ->whereIn('items.id', array_keys($sitems))
                    ->where('sites.active', \DefConst::ACTIVE)
                    ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.active',
                            'items.name as item_name', 'items.description', 'items.specification', 'items.warranty', 'items.size', 'items.deduction', 'items.price', 
                            'images.url', 'images.xsize', 'images.ysize', 'item_extras.ratings', 'list_item_brands.logo as brand_logo', 'list_item_categories.name as category_name',
                            'sites.name as site_name', 'sites.email as site_email', 'sites.phone1 as site_phone')
                    ->get();

        foreach($products as $product) {
            $product->qty = $sitems[$product->id];
            $product->total = $product->price * $sitems[$product->id];
        }


        $request->session()->put([
            'cart.subtotal' => $products->sum("total"),
        ]);

        if(!$request->session()->has('cart.invoice')){
            $request->session()->put('cart.invoice', \DefConst::INVOICE . substr(str_replace('.','',microtime(true)),0,12));
        }

        return view('web.cart', compact('products'));
    }

    ////////////////////////////////////////////////////////////////////////
	// Update cart
	public function addItemToCart(Request $request)
	{
        $item_id = $request->get('id', null);
        $item_qty = $request->get('qty', null);
        $items = $request->session()->get('cart.items', []);

        $items[$item_id] = $item_qty;
        $request->session()->put('cart.items', $items);
        
        return response()->json([
            'msg' => $item_qty . ' '. __('labels.items_added_to_cart'),
            'items_qty' => array_sum(session()->get('cart.items', [])),
        ]);
    }

    // Update Quantity Item inside the Cart
	public function updateCart(Request $request)
	{
        $items = $request->get('items');
        $request->session()->put('cart.items', $items);

        return response()->json([
            'next' => route('shop.cart'),
            'msg' => __('labels.quantity_items_updated_successfully'),
        ]);

    }
    
	// Delect item form session Cart
	public function removeItemFromCart(Request $request)
	{
        $item_id = $request->get('id');
        $request->session()->forget('cart.items'.'.'.$item_id);

        return response()->json([
            'next' => route('shop.cart'),
            'msg' => __('labels.item_removed_from_cart')
        ]);
    }

    public function site_application(Request $request)
    {
        $classifications = Classification::get();
        $cities = DB::table('cities')->get();

        return view('web.site-application', [
            'classifications' => $classifications,
            'cities' => $cities,
        ]);
    }

    private function save_cart()
    {
        if(session()->has('cart.invoice') && session()->has('cart.items')) {
            $sx = Shopping_cart::where('invoiceno', session()->get('cart.invoice'))->first();
            if(session()->has('cart.invoice') != ($sx->invoiceno ?? null)) {
                $sx = new Shopping_cart;
                $sx->user_id = auth()->id();
                $sx->invoiceno = session()->get('cart.invoice');
                $sx->subtotal = session()->get('cart.subtotal');
                $sx->paid_at = \DefConst::NOTPAID;
                $sx->save();
            } else {
                $sx->user_id = auth()->id();
                $sx->subtotal = session()->get('cart.subtotal');
                $sx->paid_at = \DefConst::NOTPAID;
                $sx->save();
            }

            $items = DB::table('items')
                        ->whereIn('items.id', array_keys(session()->get('cart.items')))
                        ->select('items.id', 'items.site_id', 'items.recordno', 'items.price')
                        ->get();
            
            foreach($items as $item){
                $shi = Shopping_cart_items::where('shopping_cart_id', $sx->id)->where('item_id', $item->id)->first();
                if($shi) {
                    $shi->qty = session()->get('cart.items'.'.'.$item->id);
                    $shi->save();
                } else {
                    $shi = new Shopping_cart_items;
                    $shi->shopping_cart_id = $sx->id;
                    $shi->site_id = auth()->id();
                    $shi->item_id = $item->id;
                    $shi->invoiceno = session()->get('cart.invoice');
                    $shi->recordno = $item->recordno;
                    $shi->qty = session()->get('cart.items'.'.'.$item->id);
                    $shi->price = $item->price;
                    $shi->save();
                }
            }

            return $sx;
        }
        return false;
    }

    public function checkout(Request $request)
    {
        if(!$request->session()->has('cart.items')) {
            return redirect()->route('shop.cart');
        }

        if($request->session()->get('cart.subtotal') <= 0.00) {
            return redirect()->route('shop.products');
        }

        if(!$request->session()->has('cart.invoice')){
            $request->session()->put('cart.invoice', \DefConst::INVOICE . substr(str_replace('.','',microtime(true)),0,12));
        }
        
        $sitems = $request->session()->get('cart.items', []);

        $products = DB::table('items')
                    ->leftjoin('item_images', 'item_images.item_id', 'items.id')
                    ->leftjoin('images', 'images.id', 'item_images.image_id')
                    ->leftjoin('sites', 'sites.id', 'items.site_id')
                    ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                    ->leftjoin('list_item_brands', 'list_item_brands.id', 'items.list_item_brand_id')
                    ->leftjoin('list_item_categories', 'list_item_categories.id', 'items.list_item_category_id')
                    ->whereIn('items.id', array_keys($sitems))
                    ->where('sites.active', \DefConst::ACTIVE)
                    ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.active',
                            'items.name as item_name', 'items.description', 'items.specification', 'items.warranty', 'items.size', 'items.deduction', 'items.price', 
                            'images.url', 'images.xsize', 'images.ysize', 'item_extras.ratings', 'list_item_brands.logo as brand_logo', 'list_item_categories.name as category_name',
                            'sites.name as site_name', 'sites.email as site_email', 'sites.phone1 as site_phone')
                    ->get();

        foreach($products as $product) {
            $product->qty = $sitems[$product->id];
            $product->total = $product->price * $sitems[$product->id];
        }

        return view('web.checkout', compact('products'));
    }

    public function finish(Request $request)
    {   
        if(!$request->session()->has('cart_proccess')) {
            return redirect()->route('shop.index');
        };

        $request->session()->forget('cart');
        $request->session()->forget('cart_proccess');

        return view('web.finish');
    }

    public function checkoutSale(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "receivername" => "required",
            "receivertel" => "required",
            "deliveryaddress" => "required",
            "deliverysuburb" => "required",
            "deliverycity" => "required",
            "cellphone_payment" => "required",
        ]);

        // If validation fails it will return the translated message key
        if ($validator->fails()) {
            $errors = $validator->failed();
            $validation_key = key(array_slice($errors , 0, 1, true));
            $errorMessage = __("errors.{$validation_key}");
            return redirect()->route('shop.checkout')->with(['details_web_error' => $errorMessage]);
        }

        if(!empty($this->save_cart())){
            // save Shopping Cart and items
            $item_cart = $this->save_cart();
            try {
                // transform cart to Sale
                $item_sale = Item_sales::where('invoiceno', session()->get('cart.invoice'))->first();
                if(!$item_sale) {
                    $item_sale = new Item_sales;
                    $item_sale->user_id = $item_cart->user_id;
                    $item_sale->shopping_cart_id = $item_cart->id;
                    $item_sale->invoiceno = $item_cart->invoiceno;
                    $item_sale->items = json_encode(array_keys(session()->get('cart.items')));
                    $item_sale->qty = array_sum(session()->get('cart.items'));
                    $item_sale->subtotal = $item_cart->subtotal;
                    $item_sale->total = $item_cart->subtotal;
                    $item_sale->paid_at = $item_cart->paid_at;
                    $item_sale->saledate = NOW();
                    $item_sale->save();
                }

            } catch(QueryException $e) {
                return false;
            }

            $payerMessage = 'Payment for goods';
            $payernote = "Goods";

            // Payment Process
            $response = $this->payment($item_cart->id, $item_cart->subtotal, $request->input('cellphone_payment'), $payerMessage, $payernote);
            
            try {
                $is = Item_sales::where('invoiceno', session()->get('cart.invoice'))->first();
                $is->paid_at = \DefConst::PAID;
                $is->save();
                // will send email to confirm successful payment
                $order = Order::where('invoiceno', session()->get('cart.invoice'))->first();
                if(!$order) {
                    $order = new Order;
                    $order->user_id = $is->user_id;
                    $order->item_sales_id = $is->id;
                    $order->invoiceno = $is->invoiceno;
                    $order->active = \DefConst::ACTIVE;
                    $order->status = \DefConst::PLACED;
                    $order->status_track_id = 1;
                    $order->paid_at = \DefConst::PAID;
                    $order->latestupdate = NOW();
                    $order->receivername = $request->input('receivername');
                    $order->receivertel = $request->input('receivertel');
                    $order->deliveryaddress = $request->input('deliveryaddress');
                    $order->deliverysuburb = $request->input('deliverysuburb');
                    $order->deliverycity = $request->input('deliverycity');
                    $order->addresstype = $request->input('addresstype');
                    $order->comments = $request->input('comments');
                    $order->save();
                }

                // update Shopping cart
                $sx = Shopping_cart::where('invoiceno', session()->get('cart.invoice'))->first();
                $sx->paid_at = \DefConst::PAID;
                $sx->save();

                $request->session()->put('cart_proccess', 1);

                return redirect()->route('shop.finish');

            } catch(QueryException $e) {
                return false;
            }
        }

    }

    private function payment($external_id, $amount, $accountnumber, $payerMessage, $payernote)
    {            
        $payment = new PaymentHelper;
        return $payment->processMtnMomo($external_id, $amount, $accountnumber, $payerMessage, $payernote);
    }

}


