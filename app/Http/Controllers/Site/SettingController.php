<?php

namespace App\Http\Controllers\Site;  

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Site;
use App\Helpers\ImageHelper;


class SettingController extends Controller
{ 
    private $page_title;
    
    public function __construct()
    {
        $this->middleware('auth:site');
        $this->page_title = __('labels.settings');
    }

    public function index()
    {
        $site = DB::table('sites')
                    ->leftjoin('classifications', 'classifications.id', 'sites.classification_id')
                    ->where('sites.id', Auth::guard('site')->id())
                    ->select('sites.id','sites.active', 'sites.name as site_name', 'sites.slug_name', 'sites.email', 'classifications.name as classification_name',
                            'sites.addr1', 'sites.addr2', 'sites.phone1', 'sites.phone2', 'sites.zip', 'sites.description', 'sites.website', 'sites.city')
                    ->selectRaw('(select url
                            from images
                            inner join site_images on site_images.image_id = images.id
                            where site_images.site_id = sites.id
                            and images.image_type = ?) as site_logo', [\DefConst::IMGTYPE_SITE_LOGO])
                    ->first();
        
        return view('sites.settings.index', [
            'site' => $site,
			'page_title' => $this->page_title,
        ]);
    }

    public function save_details(Request $request) 
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
			'slug_name' => 'required',
			'email' => 'required|email',
			"site_logo" =>"image|mimes:jpeg,jpg,png|max:5000000",
		]);

		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }
        
        $site = Site::where('id', Auth::guard('site')->id())->first();
        $site->name = $request->input("name");
		$site->slug_name = $request->input("slug_name");
		$site->email = $request->input("email");
		$site->addr1 = $request->input("addr1");
		$site->addr2 = $request->input("addr2");
		$site->zip = $request->input("zip");
		$site->phone1 = $request->input("phone1");
		$site->phone2 = $request->input("phone2");
        $site->description = $request->input("description");
        $site->website = $request->input("website");
        $site->save();

        if ($request->hasFile( \DefConst::IMGTYPE_SITE_LOGO)){
			$img_helper = new ImageHelper;
			$file = $request->file(\DefConst::IMGTYPE_SITE_LOGO);
			$process = $img_helper->processSiteLogoImage($site->id, $file, $file->getClientOriginalName(), \DefConst::IMGTYPE_SITE_LOGO);
		}

        return redirect()->route('sites.settings')->with(['details_success' => __('errors.data_saved_successfully')]);
    }

    public function save_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required|password:site',
            'new_password' => 'required|min:8|same:retype_password|max:255',
            'retype_password' => 'required',
        ]);

        // If validation fails it will return the translated message key
        if ($validator->fails()) {
            // $errors = $validator->errors();
            $errors = $validator->failed();
            // Return the key for the failed validation
            $validation_key = key(array_slice($errors , 0, 1, true));
            // Return the validation rule for the failed validation required, max etc
            $validation_type_key = strtolower(key(array_slice($errors[$validation_key], 0, 1, true)));

            $errorMessage = __("errors.{$validation_key}_{$validation_type_key}");

            return redirect()->route('sites.settings')->with(['details_error' => $errorMessage]);
        }

        $site = Site::where('id', Auth::guard('site')->id())->first();
        $site->password =  Hash::make($request->input('new_password'));
        $site->save();

        return redirect()->route('sites.settings')->with(['details_success' => __('errors.user_password_update_success')]);
    }
}