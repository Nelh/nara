<?php

namespace App\Http\Controllers\Site;  

use App\Http\Controllers\Controller;  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{

    private $page_title;
    
    public function __construct()
    {
        $this->middleware('auth:site');
        $this->page_title = __('labels.dashboard');
    }

    public function index()
    {
        return view('sites.home', [
			'page_title' => $this->page_title,
        ]);
    }

}