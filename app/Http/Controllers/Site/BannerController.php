<?php

namespace App\Http\Controllers\Site;  

use App\Http\Controllers\Controller;  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Helpers\ImageHelper;
use App\Models\Site_extra;


class BannerController extends Controller
{

    private $page_title;
    
    public function __construct()
    {
        $this->middleware('auth:site');
        $this->page_title = __('labels.config');
    }

    public function index()
    {
        $sitex = Site_extra::leftjoin('site_images', 'site_images.site_id', 'site_extra.site_id')
                ->leftjoin('images', 'images.id', 'site_images.image_id')
                ->where('site_extra.site_id', auth()->id())
                ->select('site_extra.site_id','site_extra.status', 'site_extra.banner', 'site_extra.banner_color', 'site_extra.note')
                ->selectRaw('(select url
                            from images
                            inner join site_images on site_images.image_id = images.id
                            where site_images.site_id = site_extra.site_id
                            and images.image_type = ?) as site_banner', [\DefConst::IMGTYPE_SITE_BANNER])
                ->first();

        return view('sites.banners.index', [
            'page_title' => $this->page_title,
            'sitex' => $sitex,
        ]);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'banner_name' => 'required|max:26',
			'note' => 'max:50',
            'banner_color' => 'regex:/^#[0-9a-f]{3}/i',
			"site_banner" =>"image|mimes:jpeg,jpg,png|max:5000000",
        ]);
        
        // If validation fails it will return the translated message key
        if ($validator->fails()) {
            // $errors = $validator->errors();
            $errors = $validator->failed();
            // Return the key for the failed validation
            $validation_key = key(array_slice($errors , 0, 1, true));
            // Return the validation rule for the failed validation required, max etc
            $validation_type_key = strtolower(key(array_slice($errors[$validation_key], 0, 1, true)));

            $errorMessage = __("errors.{$validation_key}_{$validation_type_key}");

            return redirect()->back()->with(['details_error' => $errorMessage]);
        }
        
        if($request->input("banner_id") == null && $request->input("banner_id") != auth()->id()) {
            $sitex = new Site_extra;
            $sitex->site_id = auth()->id();
            $sitex->banner = $request->input('banner_name');
            $sitex->note = $request->input('note');
            $sitex->banner_color = $request->input('banner_color');
            $sitex->save();
            
        } else {
            $sitex = Site_extra::where('site_id', auth()->id())->first();
            $sitex->banner = $request->input('banner_name');
            $sitex->note = $request->input('note');
            $sitex->banner_color = $request->input('banner_color');
            $sitex->save();
        }

        if ($request->hasFile( \DefConst::IMGTYPE_SITE_BANNER)){
            $img_helper = new ImageHelper;
            $file = $request->file(\DefConst::IMGTYPE_SITE_BANNER);
            $process = $img_helper->processSiteBannerImage(auth()->id(), $file, $file->getClientOriginalName(), \DefConst::IMGTYPE_SITE_BANNER);
        }
            
        return redirect()->route('sites.banner');

    }

    public function approval(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'banner_id' => 'required',
        ]);

        // If validation fails it will return the translated message key
        if ($validator->fails()) {
            // $errors = $validator->errors();
            $errors = $validator->failed();
            // Return the key for the failed validation
            $validation_key = key(array_slice($errors , 0, 1, true));
            // Return the validation rule for the failed validation required, max etc
            $validation_type_key = strtolower(key(array_slice($errors[$validation_key], 0, 1, true)));

            $errorMessage = __("errors.{$validation_key}_{$validation_type_key}");

            return redirect()->back()->with(['details_error' => $errorMessage]);
        }

        $sitex = Site_extra::where('site_id', auth()->id())->first();
        $sitex->status = \DefConst::PENDING;
        $sitex->save();

        return redirect()->back();
    }

}