<?php

namespace App\Http\Controllers\Site;  

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Helpers\ImageHelper;
use App\Site;
use App\Models\Items;
use App\Models\Item_groups;
use App\Models\List_item_brands;
use App\Models\List_item_category;
use App\Models\SubCategory;

class ProductController extends Controller
{

    private $page_title;
    
    public function __construct()
    {
        $this->middleware('auth:site');
        $this->page_title = __('labels.settings');
    }

    public function index()
    {
        $products = Items::where('items.site_id', Auth::guard('site')->id())
                            ->leftjoin('list_item_categories', 'list_item_categories.id', 'items.list_item_category_id')
                            ->leftjoin('sub_categories', 'sub_categories.id', 'items.sub_category_id')
                            ->leftjoin('list_item_brands', 'list_item_brands.id', 'items.list_item_brand_id')
                            ->select('items.id', 'items.name as product_name', 'items.recordno', 'list_item_categories.name as category_name','items.active',
                                    'list_item_brands.name as brand_name', 'sub_categories.name as sub_category_name')
                            ->orderBy('product_name')
                            ->paginate(12);

        return view('sites.products.index', [
            'products' => $products,
			'page_title' => $this->page_title,
        ]);
    }

    public function add() 
    {
        return view('sites.products.add', [
            'group_items' => Item_groups::get(),
            'list_item_brands' => List_item_brands::get(),
            'list_item_categories' => List_item_category::get(),
			'page_title' => $this->page_title,
        ]);
    }

    public function save(Request $request) 
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
			'description' => 'required|max:1000',
            'price' => 'required|numeric|regex:/^\d+(\.\d{1,13})?$/',
		]);

		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }

        $product = new Items;
        $product->site_id = Auth::guard('site')->id();
        $product->list_item_category_id = $request->input('list_item_category_id');
        $product->list_item_brand_id = $request->input('list_item_brand_id');
        $product->sub_category_id = $request->input('sub_category_id');
        $product->item_groups_id = $request->input('item_groups_id');
        $product->active = \DefConst::ACTIVE;
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->specification = $request->input('specification');
        $product->price = $request->input('price');
        $product->location = Auth::guard('site')->user()->location;
        $product->save();

        $product->recordno = \DefConst::ITEMCODE . $product->id;
        $product->save();

        return redirect()->route('sites.products.edit', ['id' => $product->id])->with(['details_success' => __('errors.data_saved_successfully')]);
    }

    public function edit(Request $request, $id) 
    {
        $product = Items::leftjoin('item_images', 'item_images.item_id', 'items.id')
                    ->leftjoin('images', 'images.id', 'item_images.image_id')
                    ->leftjoin('item_extras', 'item_extras.item_id', 'items.id')
                    ->where('.items.id', $id)
                    ->where('items.site_id', Auth::guard('site')->id())
                    ->select('items.id', 'items.site_id', 'items.recordno', 'items.list_item_category_id', 'items.list_item_brand_id', 'items.item_groups_id', 'items.sub_category_id', 'items.active',
                            'items.name as item_name', 'items.description', 'items.specification', 'items.size', 'items.deduction', 'items.price',
                            'images.url', 'item_extras.ratings')
                    ->first();

        if(!$product) {
            return redirect()->route('sites.products');
        }


        $cat_id = $request->cat_id ?? $product->list_item_category_id;
        $subcategories = SubCategory::when($cat_id, function($q, $cat_id){
            return $q->where('list_item_category_id', $cat_id);
        })->get();


        return view('sites.products.edit', [
            'product' => $product,
            'group_items' => Item_groups::get(),
            'list_item_brands' => List_item_brands::get(),
            'list_item_categories' => List_item_category::get(),
            'subcategories' => $subcategories,
			'page_title' => $this->page_title,
        ]);
    }


    public function update(Request $request) 
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
			'description' => 'required|max:1000',
			'specification' => 'max:1000',
            'price' => 'required|numeric||regex:/^\d+(\.\d{1,13})?$/',
            'size' => 'max:13',
			"item_image" =>"image|mimes:jpeg,jpg,png|max:5000000",
		]);

		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }

        $product = Items::where('id', $request->input('id'))
                    ->where('site_id', Auth::guard('site')->id())
                    ->first();
        
        if(!$product) {
            return redirect()->route('sites.products');
        }

        $product->list_item_category_id = $request->input('list_item_category_id');
        $product->list_item_brand_id = $request->input('list_item_brand_id');
        $product->sub_category_id = $request->input('sub_category_id');
        $product->item_groups_id = $request->input('item_groups_id');
        $product->active = $request->input("active") ? \DefConst::ACTIVE : \DefConst::NOTACTIVE;
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->specification = $request->input('specification');
        $product->deduction = $request->input('deduction');
        $product->price = $request->input('price');
        $product->size = $request->input('size');
        $product->save();

        if ($request->hasFile( \DefConst::IMGTYPE_ITEM)){
			$img_helper = new ImageHelper;
			$file = $request->file(\DefConst::IMGTYPE_ITEM);
			$process = $img_helper->processItemImage($product->id, $file, $file->getClientOriginalName(), \DefConst::IMGTYPE_ITEM);
        }
        
        return redirect()->route('sites.products')->with(['details_success' => __('errors.data_updated_successfully')]);
    }

    public function delete(Request $request) 
    {
        $product = Items::where('id', $request->input('id'))
                    ->where('site_id', Auth::guard('site')->id())
                    ->first();

        if(!$product) {
            return redirect()->route('sites.products');
        }

        $product->delete();

        return redirect()->route('sites.products')->with(['details_success' => __('errors.data_deleted_successfully')]);
    }
}