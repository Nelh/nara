<?php

namespace App\Http\Controllers\Admin;  

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Helpers\ImageHelper;

use App\Models\Web_card;

class CustomisationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function card(Request $request)
    {
        $cardsx = Web_card::leftjoin('web_images', 'web_images.web_card_id', 'web_card.id')
                            ->leftjoin('images', 'images.id', 'web_images.image_id')
                            ->where('web_card.active', \DefConst::ACTIVE)
                            ->select('web_card.id', 'web_card.active', 'web_card.subheading', 'web_card.heading', 'web_card.color', 'images.url')
                            ->paginate(12);

        return view('admin.customisations.card', compact('cardsx'));
    }

    public function card_add(Request $request)
    {
        return view('admin.customisations.add-card');
    }

    public function card_edit(Request $request, $id)
    {
        $cardx = Web_card::leftjoin('web_images', 'web_images.web_card_id', 'web_card.id')
                    ->leftjoin('images', 'images.id', 'web_images.image_id')
                    ->where('web_card.id', $id)
                    ->select('web_card.id', 'web_card.active', 'web_card.subheading', 'web_card.heading', 'web_card.color', 'images.url')
                    ->first();

        return view('admin.customisations.edit-card', compact('cardx'));

    }

    public function card_save(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'subheading' => 'required|max:26',
			'heading' => 'required|max:20',
            'color' => 'regex:/^#[0-9a-f]{3}/i',
			"web_image_card" =>"image|mimes:jpeg,jpg,png|max:5000000",
        ]);
        
        // If validation fails it will return the translated message key
        if ($validator->fails()) {
            $errors = $validator->failed();
            $validation_key = key(array_slice($errors , 0, 1, true));
            $validation_type_key = strtolower(key(array_slice($errors[$validation_key], 0, 1, true)));
            $errorMessage = __("errors.{$validation_key}_{$validation_type_key}");
            return redirect()->back()->with(['details_error' => $errorMessage]);
        }

        $ct = new Web_card;
        $ct->subheading = $request->input('subheading');
        $ct->heading = $request->input('heading');
        $ct->color = $request->input('color');
        $ct->active = \DefConst::ACTIVE;
        $ct->save();

        if ($request->hasFile( \DefConst::IMGTYPE_WEB_CARD)){
			$img_helper = new ImageHelper;
			$file = $request->file(\DefConst::IMGTYPE_WEB_CARD);
			$process = $img_helper->processCardImage($ct->id, $file, $file->getClientOriginalName(), \DefConst::IMGTYPE_WEB_CARD);
        }
        
        return redirect()->route('admin.customisation.card')->with(['details_success' => __('errors.data_updated_successfully')]);
    }


    public function card_update(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'cid' => 'required',
            'subheading' => 'required|max:26',
			'heading' => 'required|max:20',
            'color' => 'regex:/^#[0-9a-f]{3}/i',
			"web_image_card" =>"image|mimes:jpeg,jpg,png|max:5000000",
        ]);
        
        // If validation fails it will return the translated message key
        if ($validator->fails()) {
            $errors = $validator->failed();
            $validation_key = key(array_slice($errors , 0, 1, true));
            $validation_type_key = strtolower(key(array_slice($errors[$validation_key], 0, 1, true)));
            $errorMessage = __("errors.{$validation_key}_{$validation_type_key}");
            return redirect()->back()->with(['details_error' => $errorMessage]);
        }

        $ct = Web_card::where('id', $request->input('cid'))->first();
        $ct->subheading = $request->input('subheading');
        $ct->heading = $request->input('heading');
        $ct->color = $request->input('color');
        $ct->save();

        if ($request->hasFile( \DefConst::IMGTYPE_WEB_CARD)){
			$img_helper = new ImageHelper;
			$file = $request->file(\DefConst::IMGTYPE_WEB_CARD);
			$process = $img_helper->processCardImage($ct->id, $file, $file->getClientOriginalName(), \DefConst::IMGTYPE_WEB_CARD);
        }
        
        return redirect()->route('admin.customisation.card')->with(['details_success' => __('errors.data_updated_successfully')]);
    }

    public function card_delete(Request $request)
    {
        Web_card::where('id', $request->input('cid'))->delete();
        return redirect()->route('admin.customisation.card')->with(['details_success' => __('errors.data_updated_successfully')]);
    }
}