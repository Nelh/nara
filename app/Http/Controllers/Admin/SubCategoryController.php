<?php

namespace App\Http\Controllers\Admin;  

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\SubCategory;
use App\Models\List_item_category;


class SubCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $subcategories = SubCategory::leftjoin('list_item_categories', 'list_item_categories.id', 'sub_categories.list_item_category_id')
                        ->select('sub_categories.id','sub_categories.name as name','list_item_categories.name as category_name', 'list_item_categories.type')                
                        ->get();

        return view('admin.subcategories.index', [
            'subcategories' => $subcategories
        ]);
    }

    public function add() 
    {
        $categories = List_item_category::get();
        if(empty($categories)){
            return redirect()->route('admin.category')->with(['details_error' => __('errors.you_first_need_to_create_category')]);
        }
        return view('admin.subcategories.add', compact('categories'));
    }

    public function save(Request $request) 
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'category_id' => 'required',
		]);
		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }

        $cls = new SubCategory;
        $cls->name = $request->input('name');
        $cls->list_item_category_id = $request->input('category_id');
        $cls->save();

        return redirect()->route('admin.subcategory')->with(['details_success' => __('errors.data_saved_successfully')]);

    }

    public function edit(Request $request, $id) 
    {
        $categories = List_item_category::get();
        $subcategory = SubCategory::where('id', $id)->first();

        return view('admin.subcategories.edit', [
            'subcategory' => $subcategory,
            'categories' => $categories,
        ]);
    }

    public function update(Request $request) 
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'category_id' => 'required',
		]);
		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }

        $cls = SubCategory::where('id', $request->id)->first();
        $cls->name = $request->input('name');
        $cls->list_item_category_id = $request->input('category_id');
        $cls->save();

        return redirect()->route('admin.subcategory')->with(['details_success' => __('errors.data_updated_successfully')]);
    }


}