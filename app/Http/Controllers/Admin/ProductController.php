<?php

namespace App\Http\Controllers\Admin;  

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Classification;
use App\Models\Items;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $products = Items::leftjoin('list_item_brands', 'list_item_brands.id', 'items.list_item_brand_id')
                            ->leftjoin('list_item_categories', 'list_item_categories.id', 'items.list_item_brand_id')
                            ->select('items.id', 'items.name as product_name', 'items.recordno', 'list_item_categories.name as category_name','items.active','list_item_brands.name as brand_name')
                            ->get();


        return view('admin.products.index', [
            'products' => $products
        ]);
    }

}