<?php

namespace App\Http\Controllers\Admin;  

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Classification;
use App\Site;
use App\Models\Site_application;



class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except(['site_application_save']);
    }

    //////////////////////////////////////////////
    // All site list
    public function index()
    {
        $sites = DB::table('sites')
                    ->leftjoin('classifications', 'classifications.id', 'sites.classification_id')
                    ->select('sites.id','sites.active', 'sites.name as site_name', 'sites.slug_name', 'sites.email', 'classifications.name as classification_name')
                    ->get();

        return view('admin.site.index', [
            'sites' => $sites
        ]);
    }

    /////////////////////////////////////////////
    // add Site page
    public function add(Request $request)
    {
        $classifications = Classification::get();
        $cities = DB::table('cities')->get();

        return view('admin.site.add', [
            'classifications' => $classifications,
            'cities' => $cities,
        ]);
    }

    //////////////////////////////////////////////
    // Edit Site page
    public function edit(Request $request, $id)
    {
        $classifications = Classification::get();
        $cities = DB::table('cities')->get();
                        
        $site = DB::table('sites')
                    ->where('id', $id)
                    ->first();

        return view('admin.site.edit', [
            'site' => $site,
            'classifications' => $classifications,
            'cities' => $cities,
        ]);
    }

    ////////////////////////////////////////////////
    // Admin Save a New Site
    public function save(Request $request)
    {

        $validator = Validator::make($request->all(),[
			'name' => 'required|unique:sites',
			'slug_name' => 'required|unique:sites',
			'password' => 'required|min:10',
            'email' => 'required|email|unique:sites',
            'city' => 'required',
		]);

		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }
        
        $site = new Site;
        $site->name = $request->input("name");
		$site->slug_name = $request->input("slug_name");
        $site->password = Hash::make($request->input("password"));
        $site->classification_id = $request->input('classification_id');
        $site->email = $request->input("email");
		$site->type = $request->input("type");
		$site->addr1 = $request->input("addr1");
		$site->addr2 = $request->input("addr2");
		$site->city = $request->input("city");
		$site->location = $request->input("city");
		$site->zip = $request->input("zip");
		$site->phone1 = $request->input("phone1");
		$site->phone2 = $request->input("phone2");
        $site->description = $request->input("description");
        $site->website = $request->input("website");
        $site->save();

        return redirect()->route('admin.sites')->with(['details_success' => __('errors.data_saved_successfully')]);
    }

    //////////////////////////////////////////////////////////
    // Admin update a Site
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name' => 'required',
			'slug_name' => 'required',
			'email' => 'required|email',
            'city' => 'required',
		]);

		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }
        
        $site = Site::where('id', $request->id)->first();
        $site->active = $request->input("active") ? \DefConst::ACTIVE : \DefConst::NOTACTIVE;
        $site->name = $request->input("name");
		$site->slug_name = $request->input("slug_name");
        $site->classification_id = $request->input('classification_id');
		$site->email = $request->input("email");
		$site->type = $request->input("type");
		$site->addr1 = $request->input("addr1");
		$site->addr2 = $request->input("addr2");
		$site->city = $request->input("city");
		$site->location = $request->input("city");
		$site->zip = $request->input("zip");
		$site->phone1 = $request->input("phone1");
		$site->phone2 = $request->input("phone2");
        $site->description = $request->input("description");
        $site->website = $request->input("website");
        $site->save();

        return redirect()->route('admin.sites.edit', ['id' => $request->id])->with(['details_success' => __('errors.data_updated_successfully')]);
    }

    /////////////////////////////////////////////////////
    // Approval site List
    public function site_application_index()
    {
        $applications = Site_application::where('status', \DefConst::PENDING)->get();

        return view('admin.site.application.index', [
            'applications' => $applications
        ]);
    }

    //////////////////////////////////////////////////////
    // Submittingsite for approval
    public function site_application_save(Request $request)
    {
        $validator = Validator::make($request->all(),[
			'company_name' => 'required|unique:site_applications',
			'email' => 'required|email|unique:sites',
			'classification_id' => 'required',
			'addr' => 'required',
			'zip' => 'required|numeric',
			'city' => 'required',
			'country' => 'required',
			'phone' => 'required|numeric',
			'description' => 'required',
		]);

		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }

        $apply = new Site_application;
        $apply->company_name = $request->input('company_name');
        $apply->email = $request->input('email');
        $apply->classification_id = $request->input('classification_id');
        $apply->addr = $request->input('addr');
        $apply->zip = $request->input('zip');
        $apply->city = $request->input('city');
        $apply->country = $request->input('country');
        $apply->phone = $request->input('phone');
        $apply->website = $request->input('website');
        $apply->description = $request->input('description');
        $apply->save();

        $apply->recordno = \DefConst::APPLICATIONCODE . $apply->id;
        $apply->save();

        return redirect()->back()->with(['success' => __('labels.application_submitted')]);
    }


    ///////////////////////////////////////////
    // Approval Site detail page
    public function site_application_approval(Request $request, $id)
    {
        $site = Site_application::where('id', $id)
                                ->where('status', \DefConst::PENDING)
                                ->first();

        if(!$site) {
            return redirect()->route('admin.sites');
        }

        return view('admin.site.application.approval', [
            'site' => $site
        ]);
    }

    ////////////////////////////////////////
    // Approve a new site
    public function site_application_approved(Request $request)
    {
        $validator = Validator::make($request->all(),[
			'name' => 'required|unique:sites',
			'slug_name' => 'required|unique:sites',
			'password' => 'required|min:10',
			'email' => 'required|email|unique:sites',
		]);

		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }
        
        $site = new Site;
        $site->name = $request->input("name");
		$site->slug_name = $request->input("slug_name");
        $site->password = Hash::make($request->input("password"));
        $site->classification_id = $request->input('classification_id');
        $site->email = $request->input("email");
		$site->addr1 = $request->input("addr1");
		$site->city = $request->input("city");
		$site->zip = $request->input("zip");
		$site->phone1 = $request->input("phone1");
        $site->website = $request->input("website");
        $site->description = $request->input("description");
        $site->save();

        $a_site = Site_application::where('id', $request->input('a_id'))->first();
        $a_site->status = \DefConst::APPROVED;
        $a_site->save();

        return redirect()->route('admin.sites')->with(['details_success' => __('errors.site_application_rejected')]);
    }

    /////////////////////////////////////
    // reject a new site
    public function site_application_rejected(Request $request)
    {
        $a_site = Site_application::where('id', $request->input('a_id'))->first();
        $a_site->status = \DefConst::REJECTED;
        $a_site->save();

        return redirect()->route('admin.sites')->with(['details_success' => __('errors.site_application_rejected')]);
    }
}