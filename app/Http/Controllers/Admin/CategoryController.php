<?php

namespace App\Http\Controllers\Admin;  

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\List_item_category;


class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $categories = List_item_category::get();

        return view('admin.categories.index', [
            'categories' => $categories
        ]);
    }

    public function add() 
    {
        return view('admin.categories.add');
    }

    public function save(Request $request) 
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
		]);
		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }

        $cls = new List_item_category;
        $cls->name = $request->input('name');
        $cls->type = $request->input('type');
        $cls->save();

        return redirect()->route('admin.category')->with(['details_success' => __('errors.data_saved_successfully')]);

    }

    public function edit(Request $request, $id) 
    {
        $category = List_item_category::where('id', $id)->first();

        return view('admin.categories.edit', [
            'category' => $category
        ]);
    }

    public function update(Request $request) 
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
		]);
		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }

        $cls = List_item_category::where('id', $request->id)->first();
        $cls->name = $request->input('name');
        $cls->type = $request->input('type');
        $cls->save();

        return redirect()->route('admin.category')->with(['details_success' => __('errors.data_updated_successfully')]);
    }


}