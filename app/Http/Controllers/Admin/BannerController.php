<?php

namespace App\Http\Controllers\Admin;  

use App\Http\Controllers\Controller;  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Models\Site_extra;


class BannerController extends Controller
{

    private $page_title;
    
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->page_title = __('labels.config');
    }

    public function index()
    {
        $sitex = Site_extra::leftjoin('sites', 'sites.id', 'site_extra.site_id')
                ->where('site_extra.status', \DefConst::PENDING)
                ->select('site_extra.id','site_extra.site_id','site_extra.status', 'site_extra.banner', 'sites.name as site_name')
                ->paginate(12);

        return view('admin.banners.index', [
            'page_title' => $this->page_title,
            'sitex' => $sitex,
        ]);
    }

    public function approval(Request $request, $id)
    {
        $sitex = Site_extra::join('sites', 'sites.id', 'site_extra.site_id')
                ->leftjoin('site_images', 'site_images.site_id', 'site_extra.site_id')
                ->leftjoin('images', 'images.id', 'site_images.image_id')
                ->where('site_extra.id', $id)
                ->where('site_extra.status', \DefConst::PENDING)
                ->select('site_extra.id','site_extra.site_id','site_extra.status', 'site_extra.banner', 'site_extra.banner_color', 'site_extra.note', 'sites.name as site_name')
                ->selectRaw('(select url
                            from images
                            inner join site_images on site_images.image_id = images.id
                            where site_images.site_id = site_extra.site_id
                            and images.image_type = ?) as site_banner', [\DefConst::IMGTYPE_SITE_BANNER])
                ->first();

        return view('admin.banners.approval', [
            'page_title' => $this->page_title,
            'sitex' => $sitex,
        ]);
    }

    public function approved(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'banner_id' => 'required',
        ]);

        // If validation fails it will return the translated message key
        if ($validator->fails()) {
            // $errors = $validator->errors();
            $errors = $validator->failed();
            // Return the key for the failed validation
            $validation_key = key(array_slice($errors , 0, 1, true));
            // Return the validation rule for the failed validation required, max etc
            $validation_type_key = strtolower(key(array_slice($errors[$validation_key], 0, 1, true)));

            $errorMessage = __("errors.{$validation_key}_{$validation_type_key}");

            return redirect()->route('admin.banner')->with(['details_error' => $errorMessage]);
        }

        $sitex = Site_extra::where('id', $request->input('banner_id'))->first();
        $sitex->status = \DefConst::APPROVED;
        $sitex->save();

        return redirect()->route('admin.banner');
    }

    public function rejected(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'banner_id' => 'required',
        ]);

        // If validation fails it will return the translated message key
        if ($validator->fails()) {
            // $errors = $validator->errors();
            $errors = $validator->failed();
            // Return the key for the failed validation
            $validation_key = key(array_slice($errors , 0, 1, true));
            // Return the validation rule for the failed validation required, max etc
            $validation_type_key = strtolower(key(array_slice($errors[$validation_key], 0, 1, true)));

            $errorMessage = __("errors.{$validation_key}_{$validation_type_key}");

            return redirect()->route('admin.banner')->with(['details_error' => $errorMessage]);
        }

        $sitex = Site_extra::where('id', $request->input('banner_id'))->first();
        $sitex->status = \DefConst::REJECTED;
        $sitex->save();

        return redirect()->route('admin.banner');
    }

}