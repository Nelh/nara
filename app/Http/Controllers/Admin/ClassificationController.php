<?php

namespace App\Http\Controllers\Admin;  

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Classification;


class ClassificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $classifications = Classification::get();

        return view('admin.classifications.index', [
            'classifications' => $classifications
        ]);
    }

    public function add() 
    {
        return view('admin.classifications.add');
    }

    public function save(Request $request) 
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
		]);
		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }

        $cls = new Classification;
        $cls->name = $request->input('name');
        $cls->type = $request->input('type');
        $cls->save();

        return redirect()->route('admin.classifications')->with(['details_success' => __('errors.data_saved_successfully')]);

    }

    public function edit(Request $request, $id) 
    {
        $classification = Classification::where('id', $id)->first();

        return view('admin.classifications.edit', [
            'classification' => $classification
        ]);
    }

    public function update(Request $request) 
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
		]);
		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
        }

        $cls = Classification::where('id', $request->id)->first();
        $cls->name = $request->input('name');
        $cls->type = $request->input('type');
        $cls->save();

        return redirect()->route('admin.classifications')->with(['details_success' => __('errors.data_updated_successfully')]);
    }


}