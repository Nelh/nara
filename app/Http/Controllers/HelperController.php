<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SubCategory;
use Illuminate\Support\Facades\DB;

class HelperController extends Controller
{

    public function getsubcategories(Request $request)
    {
        $cat_id = $request->cat_id ?? $product->list_item_category_id;
        $subcategories = SubCategory::when($cat_id, function($q, $cat_id){
            return $q->where('list_item_category_id', $cat_id);
        })->get();
        return response()->json($subcategories);
    }

    // public function getstatus(){
    //     $datas = DB::table("orders_status")->get();
    //     return response()->json($datas);
    // }
}
