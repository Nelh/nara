<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
    	// validation
    	$this->validate($request,[
			'email' => 'required|email',
			'password' => 'required'
		]);

        if (method_exists($this, 'hasTooManyLoginAttempts') && $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        // Match hashed password
        $user = User::where('email',$request->email)->first();
		if (!$user || !Hash::check($request->password, $user->password)) {
	        $this->incrementLoginAttempts($request);
	        return $this->sendFailedLoginResponse($request);
        }
        
        // Login using id
		if (!Auth::guard('web')->loginUsingId($user->id, $request->remember)){
	        return $this->sendFailedLoginResponse($request);
        }
        
        // Redirect to intended route
		return redirect()->intended(route('user.home'));

    }

    protected function redirectTo()
	{
		if (session()->has('shop.location')){
			return route('shop.checkout');
		}
		return route('user.home');
    }
    
    public function logout()
	{
		session()->forget(['shop', 'web']);
		Auth::guard('web')->logout();
		return redirect('/');
	}
}
