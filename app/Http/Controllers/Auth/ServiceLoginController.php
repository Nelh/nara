<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

use App\Service;

class ServiceLoginController extends Controller
{
	use AuthenticatesUsers;

	public function __construct()
	{
		$this->middleware('guest:service')->except('serviceLogout');
	}

	public function showLoginForm()
	{
		return view('auth.service-login');
	}

	public function login(Request $request)
	{
		// validation
    	$this->validate($request,[
			'email' => 'required|email',
			'password' => 'required|min:6'
        ]);

        // Attempt to log the user in
        if (Auth::guard('service')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('service.dashboard'));
        } 
        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('email', 'remember'));
        
    }


	public function serviceLogout()
	{
		session()->forget('service');
		Auth::guard('service')->logout();
		return redirect()->route('service.login');
	}

}
