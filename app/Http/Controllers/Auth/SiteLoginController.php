<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

use App\Site;

class SiteLoginController extends Controller
{
	use AuthenticatesUsers;

	public function __construct()
	{
		$this->middleware('guest:site')->except('siteLogout');
	}

	public function showLoginForm()
	{
		return view('auth.site-login');
	}

	public function login(Request $request)
	{
		// validation
    	$this->validate($request,[
			'email' => 'required|email',
			'password' => 'required|min:6'
        ]);

        // Attempt to log the user in
        if (Auth::guard('site')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('site.dashboard'));
        } 
        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('email', 'remember'));
        
    }


	public function siteLogout()
	{
		session()->forget('site');
		Auth::guard('site')->logout();
		return redirect()->route('site.login');
	}

}
