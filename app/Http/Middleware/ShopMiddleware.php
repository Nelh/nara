<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\DB;

use Closure;

class ShopMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $categories = DB::table('list_item_categories')
                        ->leftjoin('sub_categories', 'sub_categories.list_item_category_id', 'list_item_categories.id')
                        ->select('list_item_categories.id as category_id','list_item_categories.name as category_name','sub_categories.id as sub_category_id','sub_categories.name as sub_category_name')                      
                        ->orderBy('list_item_categories.name')
                        ->get();

        $cats = [];
        $cur_cat_id = -1;
        $cur_cat_name = null;
        foreach ($categories as $category) {
            if ($cur_cat_id != $category->category_id) {
                $cur_cat_id = $category->category_id;
                $cats[$cur_cat_id]["category_name"] = $category->category_name;
                $cats[$cur_cat_id]["categories"] = [];
            }
            if($category->sub_category_id != null){
                $cats[$cur_cat_id]["categories"][] = [
                    'sub_category_id' => $category->sub_category_id,
                    'sub_category_name' => $category->sub_category_name,
                ];
            }
        }
        if(!session()->has('shop.cats') || !session()->has('shop.location')) 
        {
            session([
                'shop.cats' => $cats,
                'shop.location' => $request->location,
            ]);
        }

        return $next($request);
    }
}
