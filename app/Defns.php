<?php

use Illuminate\Support\Facades\DB;
use Carbon\CarbonImmutable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Akaunting\Money\Money;
use Akaunting\Money\Currency;
use Exception\Exception;

class DeFns {

    //////////////////////////////////////////////////////
    /// Curl Api for Get Request
    public static function curlgetApi($url, $headers)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $response = json_decode(curl_exec($curl));
        curl_close ($curl);
        return $response;
    }

    //////////////////////////////////////////////////////
    /// Curl Api for Post Request
    public static function curlpostApi($url, $headers, $cmd_opt = null)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => json_encode($cmd_opt),
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => $headers
        ]);

        $response = json_decode(curl_exec($curl));
        curl_close ($curl);
        return $response;
    }
}