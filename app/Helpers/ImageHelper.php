<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Aws\CloudFront\CloudFrontClient;
use App\Models\Image;


class ImageHelper
{
	private $img_properties;
	private $img_resource;
	private $img_original_name;


	////////////////////////////////////////////////////////////////////
	// This will process the item image
	public function processItemImage(int $item_id, string $filepath, string $original_filename, string $image_type) : bool
	{
		// This first process will save the full item image
		if (!$this->loadImage($filepath)){
			return false;
		}

		if (!$this->resizeWidth(1024)){
			imagedestroy($this->img_resource);
			return false;
		}

		if (!$this->cropSquare()){
			imagedestroy($this->img_resource);
			return false;
		}

		$this->deleteExistingS3Image($item_id, $image_type);

		$img_url = $this->saveToS3($item_id, $image_type);
		if (!$img_url){
			imagedestroy($this->img_resource);
			return false;
		}

		$file_basename = $item_id . "_" . $image_type;
		$image = $this->saveToDatabase($original_filename, $file_basename,  $img_url,  $image_type);
		if ($image->wasRecentlyCreated){
			DB::table("item_images")->insert([
				"image_id"=>$image->id,
				"item_id"=>$item_id,
			]);
		}

		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	// process a site banner image
	public function processSiteBannerImage(int $site_id, string $filepath, string $original_filename, string $image_type) : bool
	{
		if (!$this->loadImage($filepath)){
			return false;
		}

		if (!$this->resizeWidth(645)){
			imagedestroy($this->img_resource);
			return false;
		}

		if (!$this->cropHeight(361)){
			imagedestroy($this->img_resource);
			return false;
		}

		$this->deleteExistingS3Image($site_id, $image_type);

		$img_url = $this->saveToS3($site_id, $image_type);
		if (!$img_url){
			imagedestroy($this->img_resource);
			return false;
		}

		$file_basename = $site_id . "_" . $image_type;
		$image = $this->saveToDatabase($original_filename, $file_basename,  $img_url,  $image_type);
		if ($image->wasRecentlyCreated){
			DB::table("site_images")->insert([
				'site_id' => $site_id,
				'image_id' => $image->id,
			]);
		}

		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	// process web card image
	public function processCardImage(int $web_id, string $filepath, string $original_filename, string $image_type) : bool
	{
		if (!$this->loadImage($filepath)){
			return false;
		}

		if (!$this->resizeHeight(282)){
			imagedestroy($this->img_resource);
			return false;
		}

		if (!$this->cropWidth(235)){
			imagedestroy($this->img_resource);
			return false;
		}

		$this->deleteExistingS3Image($web_id, $image_type);

		$img_url = $this->saveToS3($web_id, $image_type);
		if (!$img_url){
			imagedestroy($this->img_resource);
			return false;
		}

		$file_basename = $web_id . "_" . $image_type;
		$image = $this->saveToDatabase($original_filename, $file_basename,  $img_url,  $image_type);
		if ($image->wasRecentlyCreated){
			DB::table("web_images")->insert([
				'web_card_id' => $web_id,
				'image_id' => $image->id,
			]);
		}

		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	// process a site logo image
	public function processSiteLogoImage(int $site_id, string $filepath, string $original_filename, string $image_type) : bool
	{
		if (!$this->loadImage($filepath)){
			return false;
		}

		if (!$this->resizeWidth(300)){
			imagedestroy($this->img_resource);
			return false;
		}

		if (!$this->cropHeight(85)){
			imagedestroy($this->img_resource);
			return false;
		}

		$this->deleteExistingS3Image($site_id, $image_type);

		$img_url = $this->saveToS3($site_id, $image_type);
		if (!$img_url){
			imagedestroy($this->img_resource);
			return false;
		}

		$file_basename = $site_id . "_" . $image_type;
		$image = $this->saveToDatabase($original_filename, $file_basename,  $img_url,  $image_type);
		if ($image->wasRecentlyCreated){
			DB::table("site_images")->insert([
				'site_id' => $site_id,
				'image_id' => $image->id,
			]);
		}

		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	// rotate image
	public function rotateImage(int $id, string $image_type, float $angle) : bool
	{
		if (!$this->loadImageFromS3($id, $image_type)) {
			imagedestroy($this->img_resource);
			return false;
		}
		if (!$this->rotate($angle)){
			imagedestroy($this->img_resource);
			return false;
		}

		$this->deleteExistingS3Image($id, $image_type);
		
		$img_url = $this->saveToS3($id, $image_type);
		if (!$img_url){
			imagedestroy($this->img_resource);
			return false;
		}

		$file_basename = $id . "_" . $image_type;
		$image = $this->saveToDatabase($this->img_original_name, $file_basename,  $img_url,  $image_type);
		if ($image->wasRecentlyCreated){
			if ($image_type == \DefConst::IMGTYPE_USER){
				DB::table("users_images")->insert([
					'image_id' => $image->id,
					'user_id' => $id,
				]);
			}
		}

		return true;
	}

	///////////////////////////////////////////////////////////////
	// this will load the image and get the image metadata
	private function loadImage($filename)
	{
		$this->img_properties = getimagesize($filename);
		if (!$this->img_properties || ($this->img_properties[2] != IMAGETYPE_JPEG && $this->img_properties[2] != IMAGETYPE_PNG)){
			return false;
		}

		if ($this->img_properties[2] == IMAGETYPE_JPEG){
			$this->img_resource =  imagecreatefromjpeg($filename);
		} else {
			$this->img_resource = imagecreatefrompng($filename);
		}
		return true;
	}


	/////////////////////////////////////////////////////////
	// save image to s3 bucket
	private function saveToS3(int $id, string $img_type)
	{
		ob_start();
		if ($this->img_properties[2] == IMAGETYPE_JPEG){
			imagejpeg($this->img_resource);
			$extension = ".jpg";
		} else {
			imagepng($this->img_resource);
			$extension = ".png";
		}
		$get_content = ob_get_contents();
		ob_end_clean();

		$filename = $id . "_" . $img_type . "_" . time() . $extension;

		if (!Storage::disk('s3')->put($filename, $get_content)){
			imagedestroy($this->img_resource);
			return false;
		}

		return Storage::disk('s3')->url($filename);
	}

	/////////////////////////////////////////////////////////
	// fetch the image from s3 bucket
	private function loadImageFromS3(int $id, string $img_type) : bool
	{
		$filename = $id . "_" . $img_type;
		$image_data = DB::table("images")
					->where("name", $filename)
					->select("url", "original_name")
					->first();

		$tmp = explode("/", $image_data->url);
		$img_name = array_pop($tmp);
		// Check if image existe on S3
		$exists = Storage::disk('s3')->exists($img_name);
		if(!$exists) {
			return false;
		}
		$s3_image = Storage::disk('s3')->get($img_name);
		$this->img_resource = imagecreatefromstring($s3_image);
		$this->img_properties = getimagesizefromstring($s3_image);
		$this->img_original_name = $image_data->original_name;

		return true;
	}

	/////////////////////////////////////////////////////////////////////////////
	// delete an existing image from S3 and the database
	private function deleteExistingS3Image(int $id, string $img_type) : bool
	{
		$filename = $id . "_" . $img_type;
		$s3_url = DB::table("images")
					->where("name", $filename)
					->pluck("url")
					->first();

		if (!empty($s3_url)){
			$tmp = explode("/", $s3_url);
			$img_name = array_pop($tmp);
			Storage::disk('s3')->delete($img_name);
		}

		return true;
	}


	/////////////////////////////////////////////////////////////////////////////
	// insert or update image record in the DB
	private function saveToDatabase($original_filename, $file_basename,  $uri, $img_type)
	{
		$saved_img = Image::updateOrCreate(
			[
				'name' => $file_basename
			],[
				'original_name' => $original_filename,
				'url' => 'https://' . $uri,
				'xsize'=>$this->img_properties[0],
				'ysize'=>$this->img_properties[1],
				'image_type'=>$img_type
			]
		);
		return $saved_img;
	}


	///////////////////////////////////////////////////////////////
	// crop an image square
	private function cropSquare()
	{
		$size = min($this->img_properties[0], $this->img_properties[1]); 

		// if wider than tall we trim left and right
		if ($this->img_properties[0] > $this->img_properties[1]){
			$x = ($this->img_properties[0] - $this->img_properties[1]) / 2;
			$cropped_img = imagecrop($this->img_resource, ['x'=> $x, 'y'=> 0, 'width'=> $size, 'height'=> $size]);
			imagedestroy($this->img_resource);
			$this->img_resource = $cropped_img;
			$this->img_properties[0] = $size;
		} else if ($this->img_properties[0] < $this->img_properties[1]){
			$y = ($this->img_properties[1] - $this->img_properties[0]) / 2;
			$cropped_img = imagecrop($this->img_resource, ['x'=> 0, 'y'=> $y, 'width'=> $size, 'height'=> $size]);
			imagedestroy($this->img_resource);
			$this->img_resource = $cropped_img;
			$this->img_properties[1] = $size;
		}
		return true;
	}


	///////////////////////////////////////////////////////////////
	// crop image to reqd width
	private function cropWidth(int $width)
	{
		if (!$width){
			return false;
		}

		if ($this->img_properties[0] > $width){
			$x = ($this->img_properties[0] - $width) / 2;
			$cropped_img = imagecrop($this->img_resource, ['x'=>$x, 'y'=>0, 'width'=>$width, 'height'=>$this->img_properties[1]]);
			imagedestroy($this->img_resource);
			$this->img_resource = $cropped_img;
			$this->img_properties[0] = $width;
		}
		return true;
	}


	///////////////////////////////////////////////////////////////
	// crop image to reqd height
	private function cropHeight(int $height)
	{
		if ($this->img_properties[1] > $height){
			$y_offset = ($this->img_properties[1] - $height) / 2;
			$cropped_img = imagecrop($this->img_resource, ['x'=>0, 'y'=>$y_offset, 'width'=>$this->img_properties[0], 'height'=>$height]);
			imagedestroy($this->img_resource);
			$this->img_resource = $cropped_img;
			$this->img_properties[1] = $height;
		}
		return true;
	}


	///////////////////////////////////////////////////////////
	// this will resize the height
	private function resizeHeight(int $height)
	{
		if ($this->img_properties[1] > $height){
			$ratio = $height / $this->img_properties[1];
			$width = round($this->img_properties[0] * $ratio);
			$resized_img = imagecreatetruecolor($width,$height);
			imagecopyresampled($resized_img, $this->img_resource,0,0,0,0,$width,$height, $this->img_properties[0], $this->img_properties[1]);
			imagedestroy($this->img_resource);
			$this->img_resource = $resized_img;
			$this->img_properties[0] = $width;
			$this->img_properties[1] = $height;
		}
		return true;
	}


	////////////////////////////////////////////////////////
	// this resize the image by the supplied width
	private function resizeWidth(int $width)
	{
		if ($this->img_properties[0] > $width){
			$ratio = $width / $this->img_properties[0];
			$height = round($this->img_properties[1] * $ratio);
			$resized_img = imagecreatetruecolor($width,$height);
			imagecopyresampled($resized_img, $this->img_resource,0,0,0,0,$width,$height, $this->img_properties[0], $this->img_properties[1]);
			imagedestroy($this->img_resource);
			$this->img_resource = $resized_img;
			$this->img_properties[0] = $width;
			$this->img_properties[1] = $height;
		}
		return true;
	}

	////////////////////////////////////////////////////////
	// this function rotate the image depending on the angle
	private function rotate(float $angle)
	{
		if (!$angle){
			return false;
		}
		$rotate = imagerotate($this->img_resource, $angle, 0);
		imagedestroy($this->img_resource);
		$this->img_resource = $rotate;
		return true;
	}
}

