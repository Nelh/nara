<?php

namespace App\Helpers;
use Exception\Exception;
use Illuminate\Support\Str;

class PaymentHelper 
{
    private $UUID;
    private $headers;
    private $url;
    private $cmd_opt;
    private $referenceId;

    public function __construct()
    {
        $this->UUID = (string) Str::uuid();
        $this->referenceId = (string) Str::uuid();
    }
    //////////////////////////////////////////////////////////
    public function processMtnMomo(string $external_id = null, string $amount = null, string $accountnumber = null, string $payerMessage = null, string $payernote = null)
    {
        if(!session()->has('access_token')){
            $this->createtoken();
        }

        // check if Number is valid
        $this->checkValidNumber($accountnumber);

        // Request to Pay
        $this->cmd_opt = [
            "amount" => $amount,
            "currency" => env('APP_CURRENCY',\DefConst::CURRENCY),
            "externalId" => $external_id,
            "payer" => [
                "partyIdType" => "MSISDN",
                "partyId" => $accountnumber
            ],
            "payerMessage" => $payerMessage,
            "payeeNote" => $payernote
        ];

        $this->headers = [
            'Authorization: Bearer ' . session()->get('access_token'),
            'X-Reference-Id: '. $this->referenceId,
            'X-Target-Environment: '. env('PAYMENT_TARGET_ENVIRONNEMENT'),
            'Ocp-Apim-Subscription-Key :' . env('PAYMENT_API_KEY'),
        ];
        $this->url = "https://sandbox.momodeveloper.mtn.com/collection/v1_0/requesttopay";

        try {
            $createpayment = \DeFns::curlpostApi($this->url, $this->headers, $this->cmd_opt);
             try {
                // get Requested payment result
                $this->headers = [
                    'Authorization: Bearer ' . session()->get('access_token'),
                    'X-Target-Environment: '. env('PAYMENT_TARGET_ENVIRONNEMENT'),
                    'Ocp-Apim-Subscription-Key :' . env('PAYMENT_API_KEY'),
                ];
                $this->url = "https://sandbox.momodeveloper.mtn.com/collection/v1_0/requesttopay/" . $this->referenceId;

                $response = \DeFns::curlgetApi($this->url, $this->headers);
                if($response) {
                    switch ($response->status){
                        case 'FAILED':
                            die($response);
                            break;
                        case 'REJECTED':
                            die($response);
                            break;
                        case 'TIMEOUT':
                            die($response);
                            break;
                        default:
                            return $response;
                            break;
                    }
                }

            } catch (\Exception $e){
                die($e->getMessage());
            }
        } catch (\Exception $e){
            die($e->getMessage());
        }

    }

    ///////////////////////////////////////////////////////////////////////
    // check valid Number
    private function checkValidNumber($accountnumber)
    {
        $this->headers = [
            'Authorization: Bearer ' . session()->get('access_token'),
            'X-Target-Environment: '. env('PAYMENT_TARGET_ENVIRONNEMENT'),
            'Content-Type : application/json',
            'Ocp-Apim-Subscription-Key :' . env('PAYMENT_API_KEY'),
        ];
        $this->url = "https://sandbox.momodeveloper.mtn.com/collection/v1_0/accountholder/MSISDN/".$accountnumber."/active";
        try {
            return \DeFns::curlgetApi($this->url, $this->headers);
        } catch (\Exception $e){
            die($e->getMessage());
        }
    }

    /////////////////////////////////////////////////////////////////////
    // check Fonds
    private function checkAccountFonds()
    {
        $this->headers = [
            'Authorization: Bearer ' . session()->get('access_token'),
            'X-Target-Environment: '. env('PAYMENT_TARGET_ENVIRONNEMENT'),
            'Ocp-Apim-Subscription-Key :' . env('PAYMENT_API_KEY'),
        ];
        $this->url = "https://sandbox.momodeveloper.mtn.com/collection/v1_0/account/balance";
        try {
            return \DeFns::curlgetApi($this->url, $this->headers);
        } catch (\Exception $e){
            die($e->getMessage());
        }
    }

    //////////////////////////////////////////////////////////////////
    // Create Token
    public function createtoken()
    {
        $apikey = $this->createUserApi();
        $str = base64_encode($this->UUID .':'. $apikey);
        $this->headers = [
            'Authorization : Basic ' . $str,
            'Ocp-Apim-Subscription-Key :' . env('PAYMENT_API_KEY'),
        ];
        $this->url = "https://sandbox.momodeveloper.mtn.com/collection/token/";
        try {
            $response = \DeFns::curlpostApi($this->url, $this->headers);
            session()->put('access_token', $response->access_token);
            return $response->access_token;

        } catch (\Exception $e){
            die($e->getMessage());
        }
    }

    //////////////////////////////////////////////////////////////////
    // Create User
    public function createUserApi()
    {
        $this->cmd_opt = ["providerCallbackHost" => "Nelh"];
        $this->url = "https://sandbox.momodeveloper.mtn.com/v1_0/apiuser";
        $this->headers = [
            'X-Reference-Id :' . $this->UUID,
            'Ocp-Apim-Subscription-Key :' . env('PAYMENT_API_KEY'),
        ];
        
        try {
            \DeFns::curlpostApi($this->url, $this->headers, $this->cmd_opt);
            try {
                $this->url = "https://sandbox.momodeveloper.mtn.com/v1_0/apiuser/".$this->UUID;
                $this->headers = [
                    'Ocp-Apim-Subscription-Key :' . env('PAYMENT_API_KEY'),
                ];
                \DeFns::curlgetApi($this->url, $this->headers);
                try {
                    $this->url = "https://sandbox.momodeveloper.mtn.com/v1_0/apiuser/".$this->UUID."/apikey";
                    $response = \DeFns::curlpostApi($this->url, $this->headers);
                    return $response->apiKey;

                } catch (\Exception $e){
                    die($e->getMessage());
                }
            } catch (\Exception $e){
			    die($e->getMessage());
            }
            
        } catch (\Exception $e){
			die($e->getMessage());
		}

    }

}