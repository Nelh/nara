<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ServicePasswordResetNotification;

class Service extends Authenticatable
{
    use Notifiable;

    protected $guard = 'service';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'address', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

        // overridden function for sending pw reset notification 
        public function sendPasswordResetNotification($token)
        {
            $this->notify(new ServicePasswordResetNotification($token));
        }
}
